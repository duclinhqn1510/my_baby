package bkdev.linh.mybaby.api;


public class Params {

    public static final String FAMILY_ID = "familyId";
    public static final String BABY_ID = "babyId";
    public static final String LIMIT = "limit";
    public static final String OFFSET = "offset";
    public static final String DATE = "date";
    public static final String ACTIVITY_ID = "activityId";
    public static final String ID = "id";
}
