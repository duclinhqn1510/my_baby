package bkdev.linh.mybaby.api;

import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.BabyInput;
import bkdev.linh.mybaby.models.input.BottleInput;
import bkdev.linh.mybaby.models.input.BreastInput;
import bkdev.linh.mybaby.models.input.CreateHeightInput;
import bkdev.linh.mybaby.models.input.CreateMomentInput;
import bkdev.linh.mybaby.models.input.CreateWeightInput;
import bkdev.linh.mybaby.models.input.DeleteActivityInput;
import bkdev.linh.mybaby.models.input.DeviceTokenInput;
import bkdev.linh.mybaby.models.input.DiaperInput;
import bkdev.linh.mybaby.models.input.JoinFamilyInput;
import bkdev.linh.mybaby.models.input.LoginInput;
import bkdev.linh.mybaby.models.input.RegisterFamilyInput;
import bkdev.linh.mybaby.models.input.SignUpInput;
import bkdev.linh.mybaby.models.input.SleepInput;
import bkdev.linh.mybaby.models.input.SolidInput;
import bkdev.linh.mybaby.models.input.UpdateUserInput;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import bkdev.linh.mybaby.models.response.AdviceResponse;
import bkdev.linh.mybaby.models.response.BabiesResponse;
import bkdev.linh.mybaby.models.response.BabyResponse;
import bkdev.linh.mybaby.models.response.BottleResponse;
import bkdev.linh.mybaby.models.response.BreastResponse;
import bkdev.linh.mybaby.models.response.CreateBottleResponse;
import bkdev.linh.mybaby.models.response.CreateHeightResponse;
import bkdev.linh.mybaby.models.response.CreateMomentResponse;
import bkdev.linh.mybaby.models.response.CreateWeightResponse;
import bkdev.linh.mybaby.models.response.DeleteResponse;
import bkdev.linh.mybaby.models.response.DeviceTokenResponse;
import bkdev.linh.mybaby.models.response.DiaperResponse;
import bkdev.linh.mybaby.models.response.FamilyResponse;
import bkdev.linh.mybaby.models.response.GetActivitiesResponse;
import bkdev.linh.mybaby.models.response.GetActivityDetailsResponse;
import bkdev.linh.mybaby.models.response.GetBottlesResponse;
import bkdev.linh.mybaby.models.response.GetHeightsResponse;
import bkdev.linh.mybaby.models.response.GetMomentsResponse;
import bkdev.linh.mybaby.models.response.GetWeightsResponse;
import bkdev.linh.mybaby.models.response.UserAccountResponse;
import bkdev.linh.mybaby.models.response.NewsResponse;
import bkdev.linh.mybaby.models.response.SleepResponse;
import bkdev.linh.mybaby.models.response.SolidResponse;
import bkdev.linh.mybaby.models.response.UploadImageResponse;
import bkdev.linh.mybaby.models.response.UsersResponse;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Linh NDD
 * on 1/17/18.
 */

public interface ApiService {

    @POST("/api/user/login")
    Observable<UserAccountResponse> login(@Body LoginInput loginInput);

    @POST("/api/user/create")
    Observable<UserAccountResponse> signUp(@Body SignUpInput signUpInput);

    @PUT("/api/user/{id}")
    Observable<UserAccountResponse> updateUser(@Path("id") int id, @Body UpdateUserInput updateUserInput);

    @POST("/api/user/joinFamily")
    Observable<UserAccountResponse> joinFamily(@Body JoinFamilyInput joinFamilyInput);

    @GET("/api/family/members")
    Observable<UsersResponse> getFamilyMembers(@Query(Params.ID) int familyId);

    @GET("/api/baby/index")
    Observable<BabiesResponse> getBabies(@Query(Params.FAMILY_ID) int familyId);

    @POST("/api/baby/create")
    Observable<BabyResponse> addBaby(@Body BabyInput babyInput);

    @GET("/api/family/indexById")
    Observable<FamilyResponse> getFamily(@Query(Params.ID) int id);

    @POST("/api/family/create")
    Observable<FamilyResponse> registerFamily(@Body RegisterFamilyInput registerFamilyInput);

    @POST("/api/bottle/create")
    Observable<CreateBottleResponse> createBottle(@Body BottleInput bottleInput);

    @GET("/api/bottle/index")
    Observable<GetBottlesResponse> getBottles(@Query(Params.BABY_ID) int babyId);

    @PUT("/api/bottle/{id}")
    Observable<BottleResponse> updateBottle(@Path("id") int id, @Body BottleInput bottleInput);

    @GET("/api/bottle/indexById")
    Observable<BottleResponse> getBottle(@Query(Params.ID) int id);

    @GET("/api/bottle/advice")
    Observable<AdviceResponse> getBottleAdvice(@Query(Params.BABY_ID) int babyId);

    @GET("/api/weight/index")
    Observable<GetWeightsResponse> getWeights(@Query(Params.BABY_ID) int babyId);

    @GET("/api/height/index")
    Observable<GetHeightsResponse> getHeights(@Query(Params.BABY_ID) int babyId);

    @POST("/api/weight/create")
    Observable<CreateWeightResponse> createWeight(@Body CreateWeightInput createWeightInput);

    @POST("/api/height/create")
    Observable<CreateHeightResponse> createHeight(@Body CreateHeightInput createHeightInput);

    @GET("/api/activity/getActivityAWeekAgo")
    Observable<GetActivitiesResponse> getActivitiesAWeekAgo(@Query(Params.BABY_ID) int babyId);

    @GET("/api/activity/index")
    Observable<GetActivitiesResponse> getActivities(@Query(Params.BABY_ID) int babyId,
                                                    @Query(Params.LIMIT) int limit,
                                                    @Query(Params.OFFSET) int offset);

    @GET("/api/activity/lastIndex")
    Observable<ActivityResponse> getLastActivity(@Query(Params.BABY_ID) int babyId);

    @PUT("/api/activity/{id}")
    Observable<ActivityResponse> updateActivity(@Path("id") int id, @Body ActivityInput activityInput);

    @GET("/api/activity/indexByDate")
    Observable<GetActivityDetailsResponse> getActivityDetail(@Query(Params.BABY_ID) int babyId,
                                                             @Query(Params.DATE) String date);

    @POST("/api/sleep/create")
    Observable<SleepResponse> createSleep(@Body SleepInput sleepInput);

    @PUT("/api/sleep/{id}")
    Observable<SleepResponse> updateSleep(@Path("id") int id, @Body SleepInput sleepInput);

    @GET("/api/sleep/indexById")
    Observable<SleepResponse> getSleep(@Query(Params.ID) int id);

    @POST("/api/breast/create")
    Observable<BreastResponse> createBreast(@Body BreastInput breastInput);

    @PUT("/api/breast/{id}")
    Observable<BreastResponse> updateBreast(@Path("id") int id, @Body BreastInput bottleInput);

    @GET("/api/breast/indexById")
    Observable<BreastResponse> getBreast(@Query(Params.ID) int id);

    @POST("/api/diaper/create")
    Observable<DiaperResponse> createDiaper(@Body DiaperInput diaperInput);

    @PUT("/api/diaper/{id}")
    Observable<DiaperResponse> updateDiaper(@Path("id") int id, @Body DiaperInput diaperInput);

    @GET("/api/diaper/indexById")
    Observable<DiaperResponse> getDiaper(@Query(Params.ID) int id);

    @POST("/api/solid/create")
    Observable<SolidResponse> createSolid(@Body SolidInput solidInput);

    @PUT("/api/solid/{id}")
    Observable<SolidResponse> updateSolid(@Path("id") int id, @Body SolidInput solidInput);

    @GET("/api/solid/indexById")
    Observable<SolidResponse> getSolid(@Query(Params.ID) int id);

    @HTTP(method = "DELETE", path = "/api/activity/delete", hasBody = true)
    Observable<DeleteResponse> deleteActivity(@Body DeleteActivityInput deleteActivityInput);

    @POST("/api/image/upload")
    @Multipart
    Observable<UploadImageResponse> uploadImage(@Part MultipartBody.Part photo);

    @POST("/api/moment/create")
    Observable<CreateMomentResponse> createMoment(@Body CreateMomentInput createMomentInput);

    @GET("/api/moment/index")
    Observable<GetMomentsResponse> getMoments(@Query(Params.BABY_ID) int babyId);

    @GET("/api/news/index")
    Observable<NewsResponse> getNews();

    @POST("/api/deviceToken/create")
    Observable<DeviceTokenResponse> createDeviceToken(@Body DeviceTokenInput deviceTokenInput);
}
