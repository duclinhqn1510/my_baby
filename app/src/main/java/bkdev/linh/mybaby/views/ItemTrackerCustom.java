package bkdev.linh.mybaby.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bkdev.linh.mybaby.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Linh NDD
 * on 1/24/2018.
 */

public class ItemTrackerCustom extends RelativeLayout {
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.viewLine)
    View mViewLine;
    @BindView(R.id.tvDescription)
    TextView mTvDescription;
    @BindView(R.id.imgLeft)
    ImageView mImgLeft;

    public ItemTrackerCustom(Context context) {
        this(context, null);
    }

    public ItemTrackerCustom(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemTrackerCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        View v = inflate(context, R.layout.custom_track_item, this);
        ButterKnife.bind(this, v);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ItemTrackerCustom);
        boolean hasLine = typedArray.getBoolean(R.styleable.ItemTrackerCustom_itr_has_line, true);
        int srcImgLeft = typedArray.getResourceId(R.styleable.ItemTrackerCustom_itr_src_left, 0);
        String name = typedArray.getString(R.styleable.ItemTrackerCustom_itr_tv_name);
        String description = typedArray.getString(R.styleable.ItemTrackerCustom_itr_tv_description);
        typedArray.recycle();

        mImgLeft.setImageResource(srcImgLeft);
        mViewLine.setVisibility(hasLine ? VISIBLE : INVISIBLE);
        mTvName.setText(name);
        mTvDescription.setText(description);
    }

    public void setHasLine(boolean hasLine) {
        mViewLine.setVisibility(hasLine ? VISIBLE : INVISIBLE);
    }

    public void setIsActived(boolean isActived) {
        this.setBackgroundResource(isActived ? R.drawable.bg_primary_25 : R.drawable.bg_ripple_effect);
    }

    public void setDescription(String description) {
        mTvDescription.setText(description);
    }
}
