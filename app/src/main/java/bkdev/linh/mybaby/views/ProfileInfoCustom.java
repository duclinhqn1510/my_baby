package bkdev.linh.mybaby.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import bkdev.linh.mybaby.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Linh NDD
 * on 1/24/2018.
 */

public class ProfileInfoCustom extends ConstraintLayout {

    @BindView(R.id.tvName)
    TextView mTvContent;
    @BindView(R.id.tvDescription)
    TextView mTvDescription;
    @BindView(R.id.viewLine)
    View mViewLine;

    public ProfileInfoCustom(Context context) {
        this(context, null);
    }

    public ProfileInfoCustom(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProfileInfoCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        View v = inflate(context, R.layout.custom_profile_info, this);
        ButterKnife.bind(this, v);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProfileInfoCustom);
        boolean hasLine = typedArray.getBoolean(R.styleable.ProfileInfoCustom_pi_has_line, true);
        boolean hasIconNext = typedArray.getBoolean(R.styleable.ProfileInfoCustom_pi_has_icon_next, true);
        String content = typedArray.getString(R.styleable.ProfileInfoCustom_pi_tv_content);
        String description = typedArray.getString(R.styleable.ProfileInfoCustom_pi_tv_description);
        typedArray.recycle();

        mViewLine.setVisibility(hasLine ? VISIBLE : INVISIBLE);
        mTvContent.setText(content);
        mTvDescription.setText(description);
    }

    public void setHasLine(boolean hasLine) {
        mViewLine.setVisibility(hasLine ? VISIBLE : INVISIBLE);
    }

    public void setContent(String content) {
        if (!TextUtils.isEmpty(content)) {
            this.setVisibility(VISIBLE);
            mTvContent.setText(content);
        } else {
            this.setVisibility(GONE);
        }
    }
}
