package bkdev.linh.mybaby.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bkdev.linh.mybaby.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Linh NDD
 * on 1/24/2018.
 */

public class PlayViewCustom extends RelativeLayout {
    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.tvTime)
    TextView mTvTime;

    private boolean mIsActived;

    public PlayViewCustom(Context context) {
        this(context, null);
    }

    public PlayViewCustom(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayViewCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        View v = inflate(context, R.layout.custom_play_view, this);
        ButterKnife.bind(this, v);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PlayViewCustom);
        String title = typedArray.getString(R.styleable.PlayViewCustom_pv_title);
        boolean showTime = typedArray.getBoolean(R.styleable.PlayViewCustom_pv_has_show_time, false);
        typedArray.recycle();

        mTvTitle.setText(title);
        mTvTime.setVisibility(showTime ? VISIBLE : GONE);
    }

    public void setTitle(String title) {
        mTvTitle.setText(title);
    }



    public void setSrcLeft(int left) {
        mTvTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(left, 0, 0, 0);
    }

    public void setTime(String time) {
        mTvTime.setText(time);
    }

}
