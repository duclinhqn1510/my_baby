package bkdev.linh.mybaby.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bkdev.linh.mybaby.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;
import lombok.experimental.Accessors;

public class HeaderBarPrimaryCustom extends LinearLayout {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.tvRight)
    TextView mTvRight;
    @BindView(R.id.imgLeft)
    ImageView mImgLeft;

    @Setter
    @Accessors(prefix = "m")
    private HeaderBarListener mHeaderBarListener;

    public HeaderBarPrimaryCustom(Context context) {
        this(context, null);
    }

    public HeaderBarPrimaryCustom(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeaderBarPrimaryCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        View v = inflate(context, R.layout.custom_header_bar_primary, this);
        ButterKnife.bind(this, v);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.HeaderBarPrimaryCustom);
        String title = typedArray.getString(R.styleable.HeaderBarPrimaryCustom_hbp_title);
        String rightText = typedArray.getString(R.styleable.HeaderBarPrimaryCustom_hbp_tv_right);
        int srcImgLeft = typedArray.getResourceId(R.styleable.HeaderBarPrimaryCustom_hbp_src_left, 0);
        int backgroundColor = typedArray.getColor(R.styleable.HeaderBarPrimaryCustom_hbp_background, 0);
        typedArray.recycle();

        mTvTitle.setText(title);
        mImgLeft.setImageResource(srcImgLeft);

        setImgLeft(srcImgLeft);
        setTvRight(rightText);
        if (backgroundColor == 0) {
            v.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        } else {
            v.setBackgroundColor(backgroundColor);
        }
    }

    public void setTitle(String title) {
        mTvTitle.setText(title);
    }

    public void setTitleColor(int color) {
        mTvTitle.setTextColor(color);
    }


    public void setTvRight(String string) {
        if (TextUtils.isEmpty(string)) {
            mTvRight.setVisibility(GONE);
        } else {
            mTvRight.setVisibility(VISIBLE);
            mTvRight.setText(string);
        }
    }

    public void setImgLeft(int srcLeft) {
        if (srcLeft != 0) {
            mImgLeft.setVisibility(VISIBLE);
            mImgLeft.setImageResource(srcLeft);
        } else {
            mImgLeft.setVisibility(GONE);
        }
    }

    public void setTitleDrawableRight(int src) {
        if (src != 0) {
            mTvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, src, 0);
        } else {
            mTvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @OnClick(R.id.imgLeft)
    void onClickImgLeft() {
        if (mHeaderBarListener != null) {
            mHeaderBarListener.onClickImgLeft();
        }
    }

    @OnClick(R.id.tvRight)
    void onClickTvRight() {
        if (mHeaderBarListener != null) {
            mHeaderBarListener.onClickTvRight();
        }
    }

    public interface HeaderBarListener {
        default void onClickImgLeft() {
        }

        default void onClickTvRight() {
        }
    }
}
