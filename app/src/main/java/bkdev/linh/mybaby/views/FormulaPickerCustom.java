package bkdev.linh.mybaby.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import bkdev.linh.mybaby.R;
import butterknife.ButterKnife;

/**
 * Created by Linh NDD
 * on 1/24/2018.
 */

public class FormulaPickerCustom extends RelativeLayout {


    public FormulaPickerCustom(Context context) {
        this(context, null);
    }

    public FormulaPickerCustom(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FormulaPickerCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        View v = inflate(context, R.layout.custom_picker, this);
        ButterKnife.bind(this, v);

    }
}
