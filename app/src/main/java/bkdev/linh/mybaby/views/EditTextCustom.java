package bkdev.linh.mybaby.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.concurrent.TimeUnit;

import bkdev.linh.mybaby.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created by Linh NDD
 * on 1/24/2018.
 */

public class EditTextCustom extends RelativeLayout {

    @BindView(R.id.edtContent)
    TextInputEditText mEdtContent;
    @BindView(R.id.textInputLayout)
    TextInputLayout mTextInputLayout;
    @BindView(R.id.btnClick)
    Button mButtonClick;

    @Setter
    @Accessors(prefix = "m")
    private PickerListener mPickerListener = null;

    public EditTextCustom(Context context) {
        this(context, null);
    }

    public EditTextCustom(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EditTextCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        View v = inflate(context, R.layout.custom_picker, this);
        ButterKnife.bind(this, v);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EditTextCustom);
        String text = typedArray.getString(R.styleable.EditTextCustom_pi_text);
        String hint = typedArray.getString(R.styleable.EditTextCustom_pi_hint);
        boolean enable = typedArray.getBoolean(R.styleable.EditTextCustom_pi_enable, false);
        int inputType = typedArray.getInt(R.styleable.EditTextCustom_pi_inputType, 1);
        typedArray.recycle();

        mEdtContent.setEnabled(enable);
        mEdtContent.setText(text);
        mTextInputLayout.setHint(hint);
        mEdtContent.setInputType(inputType);
        mButtonClick.setVisibility(enable ? GONE : VISIBLE);

        RxView.clicks(v.findViewById(R.id.btnClick))
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    if (mPickerListener != null) {
                        mPickerListener.onClick();
                    }
                });
    }

    public void setText(String text) {
        mEdtContent.setText(text);
    }

    public String getText() {
        return mEdtContent.getText().toString().trim();
    }

    public void setHint(String hint) {
        mEdtContent.setHint(hint);
    }

    public interface PickerListener {
        void onClick();
    }
}
