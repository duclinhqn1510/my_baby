package bkdev.linh.mybaby.shareds;

/**
 * Created by tamnguyen
 * on 10/14/17.
 */

public final class PrefsKey {
    public static final String IS_LOGIN = "IS_LOGIN";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String USER = "USER";
    public static final String BABY_ID = "BABY_ID";
    public static final String BABY = "BABY";
    public static final String IS_SLEEP_TIMER_STARTED = "IS_SLEEP_TIMER_STARTED";
    public static final String IS_BREAST_TIMER_STARTED = "IS_BREAST_TIMER_STARTED";
    public static final String SLEEP_BEGIN_TIME = "SLEEP_BEGIN_TIME";
    public static final String BREAST_LEFT_TIME = "BREAST_LEFT_TIME";
    public static final String BREAST_RIGHT_TIME = "BREAST_RIGHT_TIME";
    public static final String FAMILY = "FAMILY";
    public static final String BEGIN_SLEEP_CALENDAR = "BEGIN_SLEEP_CALENDAR";
    public static final String IS_REGISTER_DEVICE = "IS_REGISTER_DEVICE";
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
    public static final String IS_RECEIVE_NOTIFICATION = "IS_RECEIVE_NOTIFICATION";
    public static final String BREAST_SIDE = "BREAST_SIDE";
}
