package bkdev.linh.mybaby.chat;

import android.os.Bundle;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;

/**
 * Created by Linh NDD
 * on 5/8/2018.
 */

@MakeActivityStarter
public class ChatActivity extends BaseActivity{
    @Override
    public int getContentView() {
        return R.layout.activity_chat;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {

    }
}
