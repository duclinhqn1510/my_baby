package bkdev.linh.mybaby.type;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@StringDef({DiaperReasonType.POO, DiaperReasonType.PEE})
@Retention(RetentionPolicy.SOURCE)
public @interface DiaperReasonType {
    String POO = "POO";
    String PEE = "PEE";
    String CLEAN = "CLEAN";
    String BOTH = "BOTH";

}
