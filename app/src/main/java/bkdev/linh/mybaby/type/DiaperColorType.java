package bkdev.linh.mybaby.type;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@StringDef({DiaperColorType.BLACK, DiaperColorType.BROWN, DiaperColorType.YELLOW})
@Retention(RetentionPolicy.SOURCE)
public @interface DiaperColorType {
    String BLACK = "#000000";
    String BROWN = "#8D6E63";
    String YELLOW = "#FFF59D";
}
