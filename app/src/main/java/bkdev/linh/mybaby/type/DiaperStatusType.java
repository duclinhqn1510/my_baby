package bkdev.linh.mybaby.type;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@StringDef({DiaperStatusType.VERY_RUNNY, DiaperStatusType.RUNNY, DiaperStatusType.MUSHY, DiaperStatusType.MUCUSY, DiaperStatusType.SOLID, DiaperStatusType.LITTLE_BALLS})
@Retention(RetentionPolicy.SOURCE)
public @interface DiaperStatusType {
    String VERY_RUNNY = "VERY_RUNNY";
    String RUNNY = "RUNNY";
    String MUSHY = "MUSHY";
    String MUCUSY = "MUCUSY";
    String SOLID = "SOLID";
    String LITTLE_BALLS = "LITTLE_BALLS";
}
