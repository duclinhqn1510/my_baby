package bkdev.linh.mybaby.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@IntDef({BreastSideType.LEFT, BreastSideType.RIGHT})
@Retention(RetentionPolicy.SOURCE)
public @interface BreastSideType {
    int LEFT = 0;
    int RIGHT = 1;

}
