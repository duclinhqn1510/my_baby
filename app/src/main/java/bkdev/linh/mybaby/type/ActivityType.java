package bkdev.linh.mybaby.type;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@StringDef({ActivityType.SOLID, ActivityType.SLEEP, ActivityType.DIAPER, ActivityType.BOTTLE, ActivityType.BREAST, ActivityType.HEIGHT})
@Retention(RetentionPolicy.SOURCE)
public @interface ActivityType {
    String BOTTLE = "BOTTLE";
    String BREAST = "BREAST";
    String SOLID = "SOLID";
    String SLEEP = "SLEEP";
    String DIAPER = "DIAPER";
    String WEIGHT = "WEIGHT";
    String HEIGHT = "HEIGHT";


}
