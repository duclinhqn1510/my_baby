package bkdev.linh.mybaby.type;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@IntDef({GenderType.FEMALE, GenderType.MALE})
@Retention(RetentionPolicy.SOURCE)
public @interface GenderType {
    int FEMALE = 0;
    int MALE = 1;

}
