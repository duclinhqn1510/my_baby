package bkdev.linh.mybaby.type;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tamnguyen
 * on 3/2/18.
 */

@StringDef({BabyReactionType.LIKE, BabyReactionType.HATE})
@Retention(RetentionPolicy.SOURCE)
public @interface BabyReactionType {
    String LIKE = "LIKE";
    String HATE = "HATE";
}
