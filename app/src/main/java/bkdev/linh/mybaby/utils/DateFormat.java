package bkdev.linh.mybaby.utils;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.SOURCE)
@StringDef({
        DateFormat.yyyy_MM_dd_T_HH_mm_ss,
        DateFormat.yyMMdd_HHmmss,
        DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z,
        DateFormat.MMM_dd_yyyy_hh_mm_aa,
        DateFormat.dd_MM_yyyy_hh_mm_aa,
        DateFormat.hh_mm_aa,
        DateFormat.MMM_dd_yyyy,
        DateFormat.yyyy_MM_dd,
        DateFormat.dd_MM_yyyy,
        DateFormat.dd_MMM_yyyy,
        DateFormat.dd_MMMM_yyyy,
        DateFormat.yyyy_MM_dd_HH_mm_ss,
        DateFormat.yyyy_MM_dd_hh_mm_aa,
        DateFormat.dd_MMM_yyyy_hh_mm_aa,
        DateFormat.HH_mm,
        DateFormat.dd_MM_yyyy_HH_mm
})
public @interface DateFormat {

    String yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss";
    String yyMMdd_HHmmss = "yyyyMMdd_HHmmss";
    String yyyy_MM_dd_T_HH_mm_ss_SSS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String MMM_dd_yyyy_hh_mm_aa = "MMM dd, yyyy hh:mm aa";
    String dd_MM_yyyy_hh_mm_aa = "dd/MM/yyyy hh:mm aa";
    String yyyy_MM_dd_hh_mm_aa = "yyyy-MM-dd hh:mm aa";
    String hh_mm_aa = "hh:mm aa";
    String HH_mm = "HH:mm";
    String MMM_dd_yyyy = "MMM dd, yyyy";
    String yyyy_MM_dd = "yyyy-MM-dd";
    String dd_MM_yyyy = "dd/MM/yyyy";
    String dd_MMM_yyyy = "dd MMM yyyy";
    String dd_MMMM_yyyy = "dd MMMM yyyy";
    String dd_MM_yyyy_HH_mm = "dd/MM/yyyy HH:mm";
    String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    String dd_MMM_yyyy_hh_mm_aa = "dd MMM yyyy hh:mm aa";

}
