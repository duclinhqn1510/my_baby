package bkdev.linh.mybaby.utils;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class DateHelper {

    private DateHelper() {

    }

    public static Date getToday() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Date dateTimeToDate(Date dateTime, @DateFormat String format) {
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getDefault());
        String dateString = dateToString(dateTime, format);
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String dateToString(Date date, @DateFormat String format) {
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getDefault());
        return dateFormat.format(date);
    }

    public static String dateToUTCString(Date date, @DateFormat String format) {
        SimpleDateFormat formatter = validInitialize(format, TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    public static Date dateToUTC(Date date) {
        SimpleDateFormat formatter = validInitialize(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, TimeZone.getTimeZone("UTC"));
        return stringToDate(formatter.format(date), DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z);
    }

    public static Date timestampToDate(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return calendar.getTime();
    }

    public static boolean isValidDate(String dateString, @DateFormat String format) {
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getDefault());
        try {
            Date date = dateFormat.parse(dateString);
            return date != null;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Date stringToDate(String dateString, @DateFormat String format) {
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getDefault());
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date stringToUTCDate(String dateString, @DateFormat String format) {
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getTimeZone("UTC"));
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date addTimeToDate(Date date, int years, int moths, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, years);
        calendar.add(Calendar.MONTH, moths);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    public static Date addHourMinuteSecond(Date date, int hour, int minute, int second) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hour);
        calendar.add(Calendar.MINUTE, minute);
        calendar.add(Calendar.SECOND, second);
        return calendar.getTime();
    }

    public static String prettyTimeString(Date date) {
        Date today = getToday();

        long seconds = today.getTime() - date.getTime();
        int hours = (int) (seconds / 3_600_000);
        if (hours < 24) {
            return dateToString(date, DateFormat.hh_mm_aa);
        }
        int days = hours / 24;
        if (days < 7) {
            return days > 1 ? days + " days ago" : days + " day ago";
        }
        return dateToString(date, DateFormat.MMM_dd_yyyy);
    }

    public static boolean checkDiffTimeLargeThan(Date from, Date to, int minutes) {
        if (from == null || to == null) {
            return false;
        }
        long diffTimeInMinutes = (to.getTime() - from.getTime()) / 1000 / 60;
        return diffTimeInMinutes > minutes;
    }

    public static int getAge(String birthday, @DateFormat String format) {
        if (birthday == null) {
            return 0;
        }
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getDefault());
        Calendar now = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        Date convertDate = new Date();
        try {
            convertDate = dateFormat.parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dob.setTime(convertDate);
        if (dob.after(now)) {
            return -1;
        }
        int yearNow = now.get(Calendar.YEAR);
        int yearDOB = dob.get(Calendar.YEAR);
        int monthNow = now.get(Calendar.MONTH) + 1;
        int monthDOB = dob.get(Calendar.MONTH) + 1;
        int dayNow = now.get(Calendar.DAY_OF_MONTH);
        int dayDOB = dob.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearDOB;
        if (monthDOB > monthNow) {
            age--;
        } else if (monthDOB == monthNow) {
            if (dayDOB > dayNow) {
                age--;
            }
        }
        return age;
    }

    public static int getAge(Date date) {
        if (date == null) {
            return 0;
        }
        Calendar now = Calendar.getInstance();
        Calendar dob = Calendar.getInstance();
        dob.setTime(date);
        if (dob.after(now)) {
            return -1;
        }
        int yearNow = now.get(Calendar.YEAR);
        int yearDOB = dob.get(Calendar.YEAR);
        int monthNow = now.get(Calendar.MONTH) + 1;
        int monthDOB = dob.get(Calendar.MONTH) + 1;
        int dayNow = now.get(Calendar.DAY_OF_MONTH);
        int dayDOB = dob.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearDOB;
        if (monthDOB > monthNow) {
            age--;
        } else if (monthDOB == monthNow) {
            if (dayDOB > dayNow) {
                age--;
            }
        }
        return age;
    }

    public static String convertDate(String dateString, @DateFormat String sourceFormat, @DateFormat String targetFormat) {
        SimpleDateFormat dateFormat = validInitialize(sourceFormat, TimeZone.getDefault());
        try {
            Date convertedDate = dateFormat.parse(dateString);
            dateFormat = validInitialize(targetFormat, TimeZone.getDefault());
            return dateFormat.format(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isPastDate(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date today = calendar.getTime();

        return date.before(today);
    }

    private static SimpleDateFormat validInitialize(@DateFormat String format, @NonNull TimeZone timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        dateFormat.setTimeZone(timeZone);
        return dateFormat;
    }

    public static int compareDate(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        int y1 = calendar1.get(Calendar.YEAR);
        int y2 = calendar2.get(Calendar.YEAR);
        int d1 = calendar1.get(Calendar.DAY_OF_YEAR);
        int d2 = calendar2.get(Calendar.DAY_OF_YEAR);
        if (y1 != y2) {
            return y1 > y2 ? 1 : -1;
        } else if (d1 == d2) {
            return 0;
        }
        return d1 > d2 ? 1 : -1;
    }

    public static Date removeTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
