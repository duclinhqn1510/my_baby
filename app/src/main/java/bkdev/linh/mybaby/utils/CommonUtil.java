package bkdev.linh.mybaby.utils;

import android.content.Context;
import android.text.TextUtils;

import java.util.HashMap;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.type.DiaperColorType;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class CommonUtil {
    public static final String JOIN_ROOM = "join_room";
    public static final String ACTIVITY = "activity";
    public static final String KEY_EXTRA_TYPE_ACTIVITY = "KEY_EXTRA_TYPE_ACTIVITY";
    public static final String KEY_EXTRA_CONTENT_ACTIVITY = "KEY_EXTRA_CONTENT_ACTIVITY";
    public static final String KEY_EXTRA__ACTIVITY = "KEY_EXTRA__ACTIVITY";

    public static final String KEY_BUNDLE_ACTIVITY = "KEY_BUNDLE_ACTIVITY";
    public static final String KEY_BUNDLE_MOMENT = "KEY_BUNDLE_MOMENT";

    public static final String BABY_ID = "babyId";
    public static final String FAMILY_ID = "familyId";
    public static final String AUTHOR_ID = "authorId";
    public static final String CONTENT = "content";
    public static final String TYPE = "type";
    public static final String DATE = "date";
    public static final String TYPE_ID = "typeId";

    public static final String TIME_INTENT = "TIME_INTENT";
    public static final String EXTRA_KEY_TIME = "EXTRA_KEY_TIME";
    public static final String EXTRA_SERVICE_STARTED = "EXTRA_SERVICE_STARTED";
    public static final String EXTRA_ACTIVITY_TYPE = "EXTRA_ACTIVITY_TYPE";
    public static final String EXTRA_KEY_LEFT_TIME = "EXTRA_KEY_LEFT_TIME";
    public static final String EXTRA_KEY_RIGHT_TIME = "EXTRA_KEY_RIGHT_TIME";

    public static final String IS_FROM_NOTIFICATION = "IS_FROM_NOTIFICATION";

    public static final HashMap<String, String> ColorMap = new HashMap<String, String>() {
        {
            put(DiaperColorType.BLACK, "black");
            put(DiaperColorType.YELLOW, "yellow");
            put(DiaperColorType.BROWN, "brown");
        }

    };
    public static final int LIMIT_LOAD = 20;
    public static final String EXTRA_KEY_BREAST_SIDE = "EXTRA_KEY_BREAST_SIDE";
    public static final String BREAST_TIMER = "BREAST_TIMER";

    public static int parseInt(String number) {
        int result;
        if (!TextUtils.isEmpty(number)) {
            try {
                result = Integer.parseInt(number);
            } catch (NumberFormatException e) {
                result = 0;
            }
        } else {
            result = 0;
        }
        return result;
    }

    public static String[] getMonths(int monthTotal) {
        String[] months = new String[monthTotal + 1];
        for (int i = 0; i < months.length; i++) {
            months[i] = String.valueOf(i);
        }
        return months;
    }

    public static String durationFormat(Context context, long duration) {
        return context.getString(R.string.duration_format,duration ,
                duration > 1 ? context.getString(R.string.minutes) :  context.getString(R.string.minute));
    }
}
