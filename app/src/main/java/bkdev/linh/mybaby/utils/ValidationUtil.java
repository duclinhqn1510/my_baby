package bkdev.linh.mybaby.utils;

import android.text.TextUtils;

import java.util.regex.Pattern;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;

/**
 * Created by tamnguyen
 * on 10/20/17.
 */

public final class ValidationUtil {
    /**
     * Regex allowing email addresses permitted by RFC 5322 http://howtodoinjava.com/regex/java-regex-validate-email-address
     */
    private static final String REGEX_EMAIL = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    private static final String REGEX_USER_NAME = "^[a-z0-9_-]{3,15}$";

    private ValidationUtil() {
    }

    /**
     * Check email
     */
    private static boolean isEmail(String target) {
        return target != null && Pattern.compile(REGEX_EMAIL).matcher(target).matches();
    }

    /**
     * Check username
     */
    private static boolean isUsername(String target) {
        return target != null && Pattern.compile(REGEX_USER_NAME).matcher(target).matches();
    }

    /**
     * Check String only digit
     */
    private static boolean isDigit(CharSequence target) {
        return TextUtils.isDigitsOnly(target);
    }

    /**
     * This method is used to check password format
     */
    private static boolean isPassword(String password) {
        String passwordPattern = "[A-Za-z0-9@-_.#~]+";
        Pattern pattern = Pattern.compile(passwordPattern);
        return pattern.matcher(password).matches();
    }

    /**
     * This method is used to check name format
     */
    private static boolean isName(String name) {
        if (name.matches("[0-9]+")) {
            return false;
        }
        String unicodePattern = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(unicodePattern);
        return pattern.matcher(name).matches();
    }

    /**
     * Check length of input with min and max
     */
    private static boolean isValidLength(String input, int min, int max) {
        int currentSize = input == null ? 0 : input.length();
        return !(currentSize < min || currentSize > max);
    }

    public static boolean validateName(BaseActivity activity, String name) {
        if (activity == null) {
            return false;
        }
        if (TextUtils.isEmpty(name)) {
            activity.showToast(activity.getString(R.string.valid_empty_error_format, activity.getString(R.string.name)));
            return false;
        }
        if (!isName(name)) {
            activity.showToast(activity.getString(R.string.valid_not_correct_error_format, activity.getString(R.string.name)));
            return false;
        }
        return true;
    }

    public static boolean validateUsername(BaseActivity activity, String username) {
        if (activity == null) {
            return false;
        }
        if (TextUtils.isEmpty(username)) {
            activity.showToast(activity.getString(R.string.valid_empty_error_format, activity.getString(R.string.username)));
            return false;
        }
        if (!isUsername(username)) {
            activity.showToast(activity.getString(R.string.valid_not_correct_error_format, activity.getString(R.string.username)));
            return false;
        }
        return true;
    }

    public static boolean validatePassword(BaseActivity activity, String password) {
        if (activity == null) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            activity.showToast(activity.getString(R.string.valid_empty_error_format, activity.getString(R.string.password)));
            return false;
        }
        if (!isPassword(password) || !isValidLength(password, activity.getResources().getInteger(R.integer.valid_length_min_password),
                activity.getResources().getInteger(R.integer.valid_length_max_password))) {
            activity.showToast(activity.getString(R.string.valid_not_correct_error_format, activity.getString(R.string.password)));
            return false;
        }
        return true;
    }


}
