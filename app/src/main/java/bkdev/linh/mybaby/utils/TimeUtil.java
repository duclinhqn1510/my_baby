package bkdev.linh.mybaby.utils;

import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public final class TimeUtil {
    private TimeUtil() {
    }

    public static String getDateStartEnd() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), TimeUtil.FormatType.TYPE_2);
    }

    public static String getDateCurrent() {
        Calendar calendar = Calendar.getInstance();
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), TimeUtil.FormatType.TYPE_2);
    }

    public static String getTimeString(Calendar calendar) {
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), TimeUtil.FormatType.TYPE_2);
    }

    public static String getTimeString(Calendar calendar, String formatType) {
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), formatType);
    }

    public static String getTimeStartBefore2Hour(String timeStart) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_4, timeStart));
        calendar.add(Calendar.HOUR, -2);
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), FormatType.TYPE_4);
    }

    private static SimpleDateFormat validInitialize(@DateFormat String format, @NonNull TimeZone timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
        dateFormat.setTimeZone(timeZone);
        return dateFormat;
    }

    public static Date stringToUTCDate(String dateString, @DateFormat String format) {
        SimpleDateFormat dateFormat = validInitialize(format, TimeZone.getTimeZone("UTC"));
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String dateToUTCString(Date date, @DateFormat String format) {
        SimpleDateFormat formatter = validInitialize(format, TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    public static String dateToLocalString(Date date, @DateFormat String format) {
        SimpleDateFormat formatter = validInitialize(format, TimeZone.getDefault());
        return formatter.format(date);
    }

    public static String stringToUTCString(String dateString, @DateFormat String format) {
        return dateToUTCString(stringToUTCDate(dateString, DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z), format);
    }

    public static String stringUTCToLocalString(String dateString, @DateFormat String format) {
        return dateToLocalString(stringToUTCDate(dateString, DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z), format);
    }

    public static String getDateYesterday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), TimeUtil.FormatType.TYPE_2);
    }

    public static String getDateTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), TimeUtil.FormatType.TYPE_2);
    }

    public static String getTimeCurrent(@FormatType String formatType) {
        Calendar calendar = Calendar.getInstance();
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), formatType);
    }

    public static boolean compareTime(String timeRent, String timeReturn) {
        long milliRent = TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_4, timeRent);
        long milliReturn = TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_4, timeReturn);
        return milliRent - milliReturn > 0;
    }

    public static boolean isBetween2Date(String dateFrom, String dateTo, String dateBetween) {
        long milliFrom = TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_2, dateFrom);
        long milliTo = TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_2, dateTo);
        long milliBetween = TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_2, dateBetween);
        return milliBetween >= milliFrom && milliBetween <= milliTo;
    }

    public static long convertStringDateTime2Milliseconds(String formatType, String dateTime) {
        if (dateTime == null || TextUtils.isEmpty(dateTime)) {
            return 0;
        }

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatType, Locale.getDefault());
        simpleDateFormat.setTimeZone(cal.getTimeZone());

        try {
            return simpleDateFormat.parse(dateTime).getTime();
        } catch (ParseException e) {
            Log.d(TimeUtil.class.getName(), e.getMessage());
        }

        return 0;
    }

    public static long convertStringUTCDateTime2Milliseconds(String formatType, String dateTime) {
        if (dateTime == null || TextUtils.isEmpty(dateTime)) {
            return 0;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatType, Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            return simpleDateFormat.parse(dateTime).getTime();
        } catch (ParseException e) {
            Log.d(TimeUtil.class.getName(), e.getMessage());
        }

        return 0;
    }

    public static long getTimeDifferent(String dateTimeFrom, String dateTimeEnd) {
        long from = convertStringDateTime2Milliseconds(FormatType.TYPE_4, dateTimeFrom);
        long end = convertStringDateTime2Milliseconds(FormatType.TYPE_4, dateTimeEnd);
        return end <= from ? 0 : (end - from) / (1000 * 60 * 60);
    }

    public static float getDayDifferent(String dateTimeFrom, String dateTimeEnd) {
        long from = convertStringDateTime2Milliseconds(FormatType.TYPE_4, dateTimeFrom);
        long end = convertStringDateTime2Milliseconds(FormatType.TYPE_4, dateTimeEnd);
        return end <= from ? 0 : (float) (end - from) / (1000 * 60 * 60 * 24);
    }

    public static long getMinuteDiff(Calendar begin, Calendar end) {
        if (begin == null || end == null) return 0;
        long diff = (end.getTimeInMillis() - begin.getTimeInMillis()) / (1000 * 60);

        return diff < 0 ? 0 : diff;
    }

    public static String getDatetime(long timestamp, String formatType) {
        Date date = new Date();
        date.setTime(timestamp);
        SimpleDateFormat format = new SimpleDateFormat(formatType, Locale.getDefault());
        String dateTime = format.format(date);

        if (formatType.equals(FormatType.TYPE_1) && dateTime.split(":").length == 3) {
            /*
             For Sam Sung S3 Galaxy or X peria A SO-04E(4.2.2) the format of timeZone is incorrect.
             The value of shotDate for these devices like 2015-10-08T11:21:38+0700, but we want
             like 2015-10-08T11:21:38+07:00. So the below code will convert it.
             */
            dateTime = dateTime.substring(0, dateTime.length() - 2) + ":" +
                    dateTime.substring(dateTime.length() - 2, dateTime.length());
        }
        return dateTime;
    }

    public static String getDateTimeUTC(Calendar calendar, @FormatType String formatType) {
        SimpleDateFormat format = new SimpleDateFormat(formatType, Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return TimeUtil.getDatetime(calendar.getTimeInMillis(), formatType);
    }

    @StringDef({FormatType.TYPE_1, FormatType.TYPE_2, FormatType.TYPE_3, FormatType.TYPE_4, FormatType.TYPE_5, FormatType.TYPE_6})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FormatType {
        String TYPE_1 = "yyyy-MM-dd'T'HH:mm:ssZZZZZ";
        String TYPE_2 = "dd/MM/yyyy";
        String TYPE_3 = "dd/MM";
        String TYPE_4 = "dd/MM/yyyy HH:mm";
        String TYPE_5 = "HH:mm – dd/MM/yyyy";
        String TYPE_6 = "HH:mm";
    }
}
