package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.List;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import bkdev.linh.mybaby.models.Baby;
import butterknife.BindView;
import lombok.Setter;
import lombok.experimental.Accessors;

;

/**
 * Created by Linh NDD
 * on 3/30/2018.
 */

@MakeActivityStarter
public class ChooseBabyDialog extends BaseDialog {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @Arg
    String mTitle;


    private DialogListener mDialogListener;
    private BabyAdapter mBabyAdapter;


    @Setter
    @Accessors(prefix = "m")
    private List<Baby> mBabies;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTvTitle.setText(mTitle);
        mBabyAdapter = new BabyAdapter(getBaseActivity(), mBabies);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        mRecyclerView.setAdapter(mBabyAdapter);

        mBabyAdapter.setOnItemClickListener((v, position) -> {
            if(mDialogListener != null) {
                mDialogListener.onChooseBaby(mBabies.get(position));
            }
        });
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_choose_baby;
    }

    public ChooseBabyDialog setDialogListener(DialogListener dialogListener) {
        mDialogListener = dialogListener;
        return this;
    }

}
