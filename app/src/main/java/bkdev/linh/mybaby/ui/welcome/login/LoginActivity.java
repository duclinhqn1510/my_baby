package bkdev.linh.mybaby.ui.welcome.login;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.LoginInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.main.MainActivityStarter;
import bkdev.linh.mybaby.ui.welcome.add_baby.AddBabyActivityStarter;
import bkdev.linh.mybaby.ui.welcome.family.RegisterFamilyActivityStarter;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class LoginActivity extends BaseActivity<LoginView, LoginPresenter> implements HeaderBarPrimaryCustom.HeaderBarListener, LoginView {

    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.edtUsername)
    TextInputEditText mEdtUsername;
    @BindView(R.id.edtPassword)
    TextInputEditText mEdtPassword;

    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_login;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(this);
    }

    /**
     * click back
     */
    @Override
    public void onClickImgLeft() {
        onBackPressed();
    }

    /**
     * click done
     */
    @Override
    public void onClickTvRight() {
        getPresenter().login(LoginInput.builder()
                .username(mEdtUsername.getText().toString().trim())
                .password(mEdtPassword.getText().toString().trim())
                .build());
    }

    @Override
    public void loginSuccess(User user) {
        if (user.getFamilyId() != 0) {
            showToast(R.string.message_login_success);
            if (Prefs.getInstance().get(PrefsKey.BABY, Baby.class) == null) {
                AddBabyActivityStarter.start(this);
            } else {
                MainActivityStarter.start(this);
            }
        } else {
            showToast(R.string.message_please_register_family);
            RegisterFamilyActivityStarter.start(this, user.getId(), true);
        }
        Prefs.getInstance().put(PrefsKey.IS_LOGIN, true);
        Prefs.getInstance().put(PrefsKey.ACCESS_TOKEN, user.getToken());
        Prefs.getInstance().put(PrefsKey.USER, user);
        ActivityCompat.finishAffinity(this);
    }
}
