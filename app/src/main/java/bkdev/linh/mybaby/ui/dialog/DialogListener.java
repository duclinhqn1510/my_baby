package bkdev.linh.mybaby.ui.dialog;


import java.util.Date;

import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.input.BabyInput;

/**
 * Created by tamnguyen
 * on 10/21/17.
 */

public interface DialogListener {

    default void onSelectedItem(int genderType, String text) {
    }

    default void onBackProgress() {
    }

    default void onDismiss() {
    }

    default void onPositive() {
    }

    default void onPositive(int number) {
    }

    default void onEdit() {
    }

    default void onDelete() {
    }

    default void onCamera() {
    }

    default void onGallery() {
    }

    default void onPositive(Date date, float number) {
    }

    default void onChooseBaby(Baby baby) {
    }

    default void onAddBaby(BabyInput babyInput) {
    }
}
