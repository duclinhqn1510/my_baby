package bkdev.linh.mybaby.ui.news;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.News;
import bkdev.linh.mybaby.utils.decor.LinearItemDecoration;
import bkdev.linh.mybaby.utils.recycler.EndlessRecyclerView;
import bkdev.linh.mybaby.views.SwipeRefreshLayoutCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 3/25/2018.
 */

@MakeActivityStarter
public class NewsFragment extends BaseFragment<NewsView, NewsPresenter> implements NewsView {

    @BindView(R.id.imgHeader)
    ImageView mImgHeader;
    @BindView(R.id.recyclerView)
    EndlessRecyclerView mRecyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayoutCustom mSwipeRefreshLayout;

    private List<News> mNewsList;
    private NewsAdapter mNewsAdapter;

    @Override
    protected NewsPresenter createPresenter() {
        return new NewsPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_news;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mNewsList = new ArrayList<>();
        mNewsAdapter = new NewsAdapter(getBaseActivity(), mNewsList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new LinearItemDecoration(8));
        mRecyclerView.setAdapter(mNewsAdapter);
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        getPresenter().getNews(false);

        mNewsAdapter.setOnItemClickListener((v, position) -> {
            NewDetailActivityStarter.start(getBaseActivity(), mNewsList.get(position));
        });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getPresenter().getNews(true);
        });
    }

    @Override
    public void getNewsListSuccess(List<News> newsList) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        mNewsList.clear();
        mNewsList.addAll(newsList);
        mNewsAdapter.notifyDataSetChanged();
    }
}
