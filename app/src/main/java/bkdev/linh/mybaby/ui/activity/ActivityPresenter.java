package bkdev.linh.mybaby.ui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.ActivityDetail;
import bkdev.linh.mybaby.models.input.DeleteActivityInput;
import bkdev.linh.mybaby.models.response.AdviceResponse;
import bkdev.linh.mybaby.models.response.DeleteResponse;
import bkdev.linh.mybaby.models.response.GetActivitiesResponse;
import bkdev.linh.mybaby.models.response.GetActivityDetailsResponse;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public class ActivityPresenter extends BasePresenter<ActivityView> {


    public void getActivities(int babyId, int limit, int offset, boolean isRefresh) {
        if (!isRefresh) {
            getView().showLoading();
        }
        getCompositeDisposable().add(getActivitiesObservable(babyId, limit, offset)
                .subscribeWith(new CallbackWrapper<List<Activity>>() {
                    @Override
                    public void next(List<Activity> activities) {
                        getView().getActivitiesSuccess(activities);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }

    public void getActivities(int babyId, String date) {
        getView().showLoading();

        getCompositeDisposable().add(getActivityDetailsObservable(babyId, date)
                .subscribeWith(new CallbackWrapper<List<ActivityDetail>>() {
                    @Override
                    public void next(List<ActivityDetail> activityDetails) {
                        getView().getActivityDetailsSuccess(activityDetails);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }

    public void getBottleAdvice(int babyId) {
        getView().showLoading();

        getCompositeDisposable().add(getBottleAdviceObservable(babyId)
                .subscribeWith(new CallbackWrapper<String>() {
                    @Override
                    public void next(String advice) {
                        getView().getBottleAdviceSuccess(advice);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }



    public void deleteActivity(DeleteActivityInput deleteActivityInput) {
        getView().showLoading();

        getCompositeDisposable().add(deleteActivitiesObservable(deleteActivityInput)
                .subscribeWith(new CallbackWrapper<Boolean>() {
                    @Override
                    public void next(Boolean b) {
                        getView().deleteActivitySuccess(b);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }

    public void filterActivities(String dateFrom, String dateTo, List<Activity> activities) {
        getView().showLoading();
        getCompositeDisposable().add(filterActivityByDayObservable(dateFrom, dateTo, activities)
                .subscribeWith(new CallbackWrapper<List<Activity>>() {
                    @Override
                    public void next(List<Activity> activities) {
                        getView().filterActivitiesSuccess(activities);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }

    private Observable<List<Activity>> getActivitiesObservable(int babyId, int limit, int offset) {
        return Observable.defer(() -> ApiClient.call().getActivities(babyId, limit, offset)
                .filter(getActivitiesResponse -> getActivitiesResponse != null && getActivitiesResponse.getActivities() != null)
                .map(GetActivitiesResponse::getActivities)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Boolean> deleteActivitiesObservable(DeleteActivityInput deleteActivityInput) {
        return Observable.defer(() -> ApiClient.call().deleteActivity(deleteActivityInput)
                .filter(deleteResponse -> deleteResponse != null)
                .map(DeleteResponse::getResult)
                .map(integer -> integer > 0)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<List<ActivityDetail>> getActivityDetailsObservable(int babyId, String date) {
        return Observable.defer(() -> ApiClient.call().getActivityDetail(babyId, date)
                .filter(getActivityDetailsResponse -> getActivityDetailsResponse != null && getActivityDetailsResponse.getActivityDetails() != null)
                .map(GetActivityDetailsResponse::getActivityDetails)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<String> getBottleAdviceObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getBottleAdvice(babyId)
                .filter(adviceResponse -> adviceResponse != null && adviceResponse.getAdvice() != null)
                .map(AdviceResponse::getAdvice)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<List<Activity>> filterActivityByDayObservable(String dateFrom, String dateTo, List<Activity> activities) {
        return Observable.fromIterable(activities)
                .filter(activity ->
                        TimeUtil.isBetween2Date(dateFrom, dateTo, TimeUtil.stringUTCToLocalString(activity.getDate(), DateFormat.dd_MM_yyyy)))
                .toList()
                .toObservable()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui());
    }


}
