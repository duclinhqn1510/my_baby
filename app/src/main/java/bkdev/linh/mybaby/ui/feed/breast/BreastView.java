package bkdev.linh.mybaby.ui.feed.breast;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Breast;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface BreastView extends MvpView {
    void createBreastSuccess(Breast breast);

    void getBreastSuccess(Breast breast);

    void updateBreastSuccess(Breast breast);

    void updateActivitySuccess(Activity activity);
}
