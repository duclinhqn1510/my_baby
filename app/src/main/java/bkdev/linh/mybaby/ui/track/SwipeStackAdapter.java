package bkdev.linh.mybaby.ui.track;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.R;

/**
 * Created by Linh NDD
 * on 4/17/2018.
 */

public class SwipeStackAdapter extends BaseAdapter {

    private List<String> mData;
    private Context mContext;

    public SwipeStackAdapter(@NonNull Context context, List<String> data) {
        this.mData = data;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_card, parent, false);
        TextView textViewCard = convertView.findViewById(R.id.tvContent);
        textViewCard.setText(mData.get(position));

        return convertView;
    }
}
