package bkdev.linh.mybaby.ui.more.baby;

import android.os.Bundle;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.GenderType;
import bkdev.linh.mybaby.ui.more.family.FamilyProfilePresenter;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import bkdev.linh.mybaby.views.ProfileInfoCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/20/2018.
 */

@MakeActivityStarter
public class BabyInformationActivity extends BaseActivity {

    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.infoName)
    ProfileInfoCustom mInfoName;
    @BindView(R.id.infoBirthday)
    ProfileInfoCustom mInfoBirthday;
    @BindView(R.id.infoGender)
    ProfileInfoCustom mInfoGender;


    @Override
    protected FamilyProfilePresenter createPresenter() {
        return new FamilyProfilePresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_baby_information;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        mInfoGender.setContent(baby.getGender() == GenderType.MALE ? getString(R.string.boy) : getString(R.string.girl));
        mInfoName.setContent(baby.getName());
        mInfoBirthday.setContent(TimeUtil.stringToUTCString(baby.getBirthday(), DateFormat.yyyy_MM_dd));
    }

}
