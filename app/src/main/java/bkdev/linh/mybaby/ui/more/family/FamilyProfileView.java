package bkdev.linh.mybaby.ui.more.family;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.User;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public interface FamilyProfileView extends MvpView {
    void getBabiesSuccess(List<Baby> babies);

    void getUsersSuccess(List<User> users);
}
