package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import bkdev.linh.mybaby.type.GenderType;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 3/30/2018.
 */

@MakeActivityStarter
public class GenderDialog extends BaseDialog {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.rgGenders)
    RadioGroup mRgGenders;
    @BindView(R.id.rbMale)
    RadioButton mRbMale;
    @BindView(R.id.rbFemale)
    RadioButton mRbFemale;

    @Arg
    int mSelectIndex;

    private DialogListener mDialogListener;
    private String mSelectedText;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTvTitle.setText(getBaseActivity().getString(R.string.gender));
        mRbMale.setChecked(mSelectIndex == GenderType.MALE);
        mRbFemale.setChecked(mSelectIndex == GenderType.FEMALE);

        mRgGenders.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id) {
                case R.id.rbMale:
                    mSelectIndex = GenderType.MALE;
                    mSelectedText = getBaseActivity().getString(R.string.male);
                    break;
                case R.id.rbFemale:
                    mSelectIndex = GenderType.FEMALE;
                    mSelectedText = getBaseActivity().getString(R.string.female);
                    break;
            }
            if (mDialogListener != null) {
                mDialogListener.onSelectedItem(mSelectIndex, mSelectedText);
            }
            dismiss();
        });
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_gender;
    }

    public GenderDialog setDialogListener(DialogListener mDialogListener) {
        this.mDialogListener = mDialogListener;
        return this;
    }
}
