package bkdev.linh.mybaby.ui.more;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.service.firebase.MyFirebaseNotificationUtil;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.more.baby.BabyInformationActivityStarter;
import bkdev.linh.mybaby.ui.more.family.FamilyProfileActivityStarter;
import bkdev.linh.mybaby.ui.welcome.SplashActivityStarter;
import bkdev.linh.mybaby.ui.welcome.add_baby.AddBabyActivityStarter;
import bkdev.linh.mybaby.views.HeaderBarCustom;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class MoreFragment extends BaseFragment {
    @BindView(R.id.headerBar)
    HeaderBarCustom mHeaderBar;

    @Override
    public int getContentView() {
        return R.layout.fragment_more;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mHeaderBar.setTitle(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getName());
        mHeaderBar.setHeaderBarListener(new HeaderBarCustom.HeaderBarListener() {
            @Override
            public void onClickTvTitle() {
                if (getMainActivity() != null) {
                    getMainActivity().getBabies();
                }
            }
        });
    }

    @OnClick(R.id.tvLogOut)
    void onClickLogOut() {
        /*Observable.just(mAppDatabase)
                .doOnNext(appDatabase -> {
                    appDatabase.weightDao().deleteAll();
                    appDatabase.bottleDao().deleteAll();
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(appDatabase -> {
                    Log.d("TAGGGG", "delete success");
                });*/
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Prefs.getInstance().clear();
        MyFirebaseNotificationUtil.clearNotifications(App.getInstance().getApplicationContext());
        SplashActivityStarter.start(getBaseActivity());
        ActivityCompat.finishAffinity(getBaseActivity());
        getBaseActivity().overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out);
    }

    @OnClick(R.id.tvChat)
    void onClickChat() {
        getBaseActivity().showToast(R.string.commin_soon);
    }

    @OnClick(R.id.tvFamilyProfile)
    void onClickFamilyProfile() {
        FamilyProfileActivityStarter.start(getBaseActivity());
    }

    @OnClick(R.id.tvBabyInfo)
    void onClickBabyInfo() {
        BabyInformationActivityStarter.start(getBaseActivity());
    }

    @OnClick(R.id.tvAddNewBaby)
    void onClickAddBaby() {
        AddBabyActivityStarter.start(getBaseActivity());
    }
}
