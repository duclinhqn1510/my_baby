package bkdev.linh.mybaby.ui.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import java.util.Calendar;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import butterknife.BindView;
import butterknife.OnClick;


@MakeActivityStarter
public class GrowthDialog extends BaseDialog {

    @BindView(R.id.edtDate)
    EditTextCustom mEdtDate;
    @BindView(R.id.edtNumber)
    EditTextCustom mEdtNumber;
    @BindView(R.id.tvUnit)
    TextView mTvUnit;

    @Arg
    String growthType;
    @Arg
    String unit;

    private DialogListener mDialogListener;
    private Calendar mDateSelected;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mEdtNumber.setHint(growthType);
        mTvUnit.setText(unit);
        mDateSelected = Calendar.getInstance();
        mEdtDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mDateSelected.set(year, monthOfYear, dayOfMonth);
                    mEdtDate.setText(TimeUtil.dateToLocalString(mDateSelected.getTime(), DateFormat.yyyy_MM_dd));
                }
            }, mDateSelected.get(Calendar.YEAR), mDateSelected.get(Calendar.MONTH), mDateSelected.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });
    }

    private boolean validateInfo() {
        if (TextUtils.isEmpty(mEdtDate.getText().trim().trim())) {
            getBaseActivity().showToast(R.string.message_date_empty);
            return false;
        }
        if (TextUtils.isEmpty(mEdtNumber.getText().trim().trim())) {
            getBaseActivity().showToast(R.string.message_number_empty);
            return false;
        }
        return true;
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_growth;
    }

    @OnClick(R.id.tvPositive)
    void onPositive() {
        if (validateInfo()) {
            if (mDialogListener != null) {
                mDialogListener.onPositive(mDateSelected.getTime(), Float.parseFloat(mEdtNumber.getText()));
            }
        }

        dismiss();
    }

    @OnClick(R.id.tvNegative)
    void onNegative() {
        dismiss();
    }

    public GrowthDialog setDialogListener(DialogListener dialogListener) {
        mDialogListener = dialogListener;
        return this;
    }


}
