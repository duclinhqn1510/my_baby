package bkdev.linh.mybaby.ui.grow_chart.height;

import java.util.List;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Height;
import bkdev.linh.mybaby.models.input.CreateHeightInput;
import bkdev.linh.mybaby.models.response.CreateHeightResponse;
import bkdev.linh.mybaby.models.response.GetHeightsResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class HeightPresenter extends BasePresenter<HeightView> {
    public void createHeight(CreateHeightInput createHeightInput) {
        getView().showLoading();
        getCompositeDisposable().add(createHeightObservable(createHeightInput)
                .subscribeWith(new CallbackWrapper<Height>() {
                    @Override
                    public void next(Height height) {
                        getView().createHeightSuccess(height);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void getHeights(int babyId) {
        getView().showLoading();
        getCompositeDisposable().add(getHeightsObservable(babyId)
                .subscribeWith(new CallbackWrapper<List<Height>>() {
                    @Override
                    public void next(List<Height> heights) {
                        getView().getHeightsSuccess(heights);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Height> createHeightObservable(CreateHeightInput createHeightInput) {
        return Observable.defer(() -> ApiClient.call().createHeight(createHeightInput)
                .filter(createHeightResponse -> createHeightResponse != null && createHeightResponse.getHeight() != null)
                .map(CreateHeightResponse::getHeight)
                /*.map(weight -> {
                    mAppDatabase.weightDao().insert(weight);
                    return weight;
                })*/
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<List<Height>> getHeightsObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getHeights(babyId)
                .filter(getHeightsResponse -> getHeightsResponse != null && getHeightsResponse.getHeights() != null)
                .map(GetHeightsResponse::getHeights)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

}
