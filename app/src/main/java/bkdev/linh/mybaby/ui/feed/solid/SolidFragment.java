package bkdev.linh.mybaby.ui.feed.solid;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import java.util.Calendar;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Solid;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.SolidInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.type.BabyReactionType;
import bkdev.linh.mybaby.type.GenderType;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class SolidFragment extends BaseFragment<SolidView, SolidPresenter> implements SolidView {
    @BindView(R.id.edtDate)
    EditTextCustom mEdtDate;
    @BindView(R.id.edtTime)
    EditTextCustom mEdtTime;
    @BindView(R.id.edtAmount)
    EditTextCustom mEdtAmount;
    @BindView(R.id.rgReactions)
    RadioGroup mRgReactions;
    @BindView(R.id.rbLike)
    RadioButton mRbLike;
    @BindView(R.id.rbHate)
    RadioButton mRbHate;
    @BindView(R.id.rlContainer)
    RelativeLayout mRlContainer;

    private Calendar mDateOfFeeding;
    private String mBabyReaction;

    @Arg(optional = true)
    int mId;
    @Arg(optional = true)
    int mActivityId;

    @Override
    protected SolidPresenter createPresenter() {
        return new SolidPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_solid;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mDateOfFeeding = Calendar.getInstance();

        mEdtDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mDateOfFeeding.set(year, monthOfYear, dayOfMonth);
                    mEdtDate.setText(TimeUtil.getTimeString(mDateOfFeeding));
                }
            }, mDateOfFeeding.get(Calendar.YEAR), mDateOfFeeding.get(Calendar.MONTH), mDateOfFeeding.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtTime.setPickerListener(() -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(getBaseActivity(), (timePicker, hourOfDay, minute) -> {
                if (timePicker.isShown()) {
                    mDateOfFeeding.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    mDateOfFeeding.set(Calendar.MINUTE, minute);
                    mEdtTime.setText(TimeUtil.getTimeString(mDateOfFeeding, TimeUtil.FormatType.TYPE_6));
                }
            }, mDateOfFeeding.get(Calendar.HOUR_OF_DAY), mDateOfFeeding.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });

        mRgReactions.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id) {
                case R.id.rbHate:
                    mBabyReaction = BabyReactionType.HATE;
                    break;
                case R.id.rbLike:
                    mBabyReaction = BabyReactionType.LIKE;
                    break;
            }
        });

        if (mId != 0 && mActivityId != 0) {
            loadToEdit();
            mRlContainer.setVisibility(View.INVISIBLE);
        }
    }

    private void loadToEdit() {
        if (mId != 0 && mActivityId != 0) {
            getPresenter().getSolid(mId);
        }
    }


    @OnClick(R.id.tvSave)
    void onClickSave() {
        if (mId != 0 && mActivityId != 0) {
            getPresenter().updateSolid(mId, SolidInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .amount(CommonUtil.parseInt(mEdtAmount.getText().trim()))
                    .babyReaction(mBabyReaction)
                    .timeOfFeeding(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .build());
        } else {
            getPresenter().createSolid(SolidInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .amount(CommonUtil.parseInt(mEdtAmount.getText().trim()))
                    .babyReaction(mBabyReaction)
                    .timeOfFeeding(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .build());
        }

    }

    @Override
    public void createSolidSuccess(Solid solid) {
        getBaseActivity().showToast("Solid" + solid.getId());

        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(solid);
        moveBack(activity);
    }

    private bkdev.linh.mybaby.models.Activity getBabyActivity(Solid solid) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        User user = Prefs.getInstance().get(PrefsKey.USER, User.class);
        bkdev.linh.mybaby.models.Activity activity = new bkdev.linh.mybaby.models.Activity();
        activity.setBabyId(solid.getBabyId());
        activity.setAuthorId(user.getId());
        activity.setType(ActivityType.SOLID);
        activity.setFamilyId(baby.getFamilyId());
        activity.setDate(solid.getTimeOfFeeding());
        activity.setTypeId(solid.getId());
        activity.setContent(String.format(getString(R.string.solid_format),
                baby.getName(),
                solid.getAmount(),
                baby.getGender() == GenderType.MALE ? getString(R.string.he) : getString(R.string.she)
                , solid.getBabyReaction()));

        return activity;
    }

    private void moveBack(bkdev.linh.mybaby.models.Activity activity) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY, activity);
        intent.putExtras(bundle);

        getBaseActivity().setResult(Activity.RESULT_OK, intent);
        getBaseActivity().finish();
    }

    @Override
    public void getSolidSuccess(Solid solid) {
        mRlContainer.setVisibility(View.VISIBLE);
        mEdtTime.setText(TimeUtil.stringUTCToLocalString(solid.getTimeOfFeeding(), DateFormat.HH_mm));
        mEdtDate.setText(TimeUtil.stringUTCToLocalString(solid.getTimeOfFeeding(), DateFormat.yyyy_MM_dd));
        mDateOfFeeding.setTimeInMillis(TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, solid.getTimeOfFeeding()));
        mEdtAmount.setText(String.valueOf(solid.getAmount()));
        mRbHate.setChecked(solid.getBabyReaction().equals(BabyReactionType.HATE));
        mRbLike.setChecked(solid.getBabyReaction().equals(BabyReactionType.LIKE));
    }

    @Override
    public void updateSolidSuccess(Solid solid) {
        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(solid);
        ActivityInput activityInput = new ActivityInput(activity);
        getPresenter().updateActivity(mActivityId, activityInput);
    }

    @Override
    public void updateActivitySuccess(bkdev.linh.mybaby.models.Activity activity) {
        moveBack(activity);
    }
}
