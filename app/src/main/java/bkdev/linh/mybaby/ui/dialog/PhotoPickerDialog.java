package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 3/30/2018.
 */

@MakeActivityStarter
public class PhotoPickerDialog extends BaseDialog {
    @BindView(R.id.tvTitle)
    TextView mTvTitle;

    private DialogListener mDialogListener;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTvTitle.setText(getString(R.string.photo));
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_photo_picker;
    }

    @OnClick(R.id.imgTakePhoto)
    void onClickTakePhoto(View view) {
        if(mDialogListener != null) {
            mDialogListener.onCamera();
        }
        dismiss();
    }

    @OnClick(R.id.imgGallery)
    void onClickGallery(View view) {
        if(mDialogListener != null ) {
            mDialogListener.onGallery();
        }
        dismiss();
    }

    public PhotoPickerDialog setDialogListener(DialogListener mDialogListener) {
        this.mDialogListener = mDialogListener;
        return this;
    }
}
