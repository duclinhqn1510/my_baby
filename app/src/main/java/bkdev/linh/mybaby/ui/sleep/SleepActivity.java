package bkdev.linh.mybaby.ui.sleep;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Sleep;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.SleepInput;
import bkdev.linh.mybaby.service.TimeRecordService;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.ui.main.MainActivityStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import bkdev.linh.mybaby.views.PlayViewCustom;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class SleepActivity extends BaseActivity<SleepView, SleepPresenter> implements SleepView {

    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBarPrimary;
    @BindView(R.id.tvTime)
    TextView mTvTime;
    @BindView(R.id.tvInput)
    TextView mTvInput;
    @BindView(R.id.tvSave)
    TextView mTvSave;
    @BindView(R.id.playView)
    PlayViewCustom mPlayView;
    @BindView(R.id.edtBeginDate)
    EditTextCustom mEdtBeginDate;
    @BindView(R.id.edtBeginTime)
    EditTextCustom mEdtBeginTime;
    @BindView(R.id.llBeginTime)
    LinearLayout mLlBeginTime;
    @BindView(R.id.edtEndDate)
    EditTextCustom mEdtEndDate;
    @BindView(R.id.edtEndTime)
    EditTextCustom mEdtEndTime;
    @BindView(R.id.llEndTime)
    LinearLayout mLlEndTime;
    @BindView(R.id.clContainer)
    ConstraintLayout mClContainer;

    @Arg(optional = true)
    int mActivityId;
    @Arg(optional = true)
    int mId;

    private boolean mIsStarted;
    private long mCountTimeInSecond;
    private boolean mIsManual;
    private Calendar mEndDate;
    private Calendar mBeginDate;

    @Override
    protected SleepPresenter createPresenter() {
        return new SleepPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_sleep;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mBeginDate = Calendar.getInstance();
        mEndDate = Calendar.getInstance();

        mIsStarted = getIntent().getBooleanExtra(CommonUtil.EXTRA_SERVICE_STARTED, false);
        mHeaderBarPrimary.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });
        setUpPlayView(mIsStarted);

        RxView.clicks(mPlayView)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    if (!mIsStarted) {
                        Prefs.getInstance().put(PrefsKey.IS_SLEEP_TIMER_STARTED, mIsStarted);
                        startService(new Intent(this, TimeRecordService.class).putExtra(CommonUtil.EXTRA_ACTIVITY_TYPE, ActivityType.SLEEP));
                        Prefs.getInstance().put(PrefsKey.BEGIN_SLEEP_CALENDAR, Calendar.getInstance());
                    } else {
                        stopService(new Intent(this, TimeRecordService.class));
                        mPlayView.setVisibility(View.GONE);
                        mTvSave.setVisibility(View.VISIBLE);
                        mEndDate = Calendar.getInstance();
                    }
                    mIsStarted = !mIsStarted;
                    setUpPlayView(mIsStarted);
                    Prefs.getInstance().put(PrefsKey.IS_SLEEP_TIMER_STARTED, mIsStarted);
                });

        setUpManual();

        registerReceiver(mBroadcastReceiver, new IntentFilter(ActivityType.SLEEP));

        if (mId != 0 && mActivityId != 0) {
            getPresenter().getSleep(mId);
            mClContainer.setVisibility(View.INVISIBLE);
        }
    }

    private void setUpManual() {
        mEdtEndDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mEndDate.set(year, monthOfYear, dayOfMonth);
                    mEdtEndDate.setText(TimeUtil.getTimeString(mEndDate));
                    showDiffTime();
                }
            }, mEndDate.get(Calendar.YEAR), mEndDate.get(Calendar.MONTH), mEndDate.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtEndTime.setPickerListener(() -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minute) -> {
                if (timePicker.isShown()) {
                    mEndDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    mEndDate.set(Calendar.MINUTE, minute);
                    mEdtEndTime.setText(TimeUtil.getTimeString(mEndDate, TimeUtil.FormatType.TYPE_6));
                    showDiffTime();
                }
            }, mEndDate.get(Calendar.HOUR_OF_DAY), mEndDate.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });

        mEdtBeginDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mBeginDate.set(year, monthOfYear, dayOfMonth);
                    mEdtBeginDate.setText(TimeUtil.getTimeString(mBeginDate));
                    showDiffTime();
                }
            }, mBeginDate.get(Calendar.YEAR), mBeginDate.get(Calendar.MONTH), mBeginDate.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtBeginTime.setPickerListener(() -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minute) -> {
                if (timePicker.isShown()) {
                    mBeginDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    mBeginDate.set(Calendar.MINUTE, minute);
                    mEdtBeginTime.setText(TimeUtil.getTimeString(mBeginDate, TimeUtil.FormatType.TYPE_6));
                    showDiffTime();
                }
            }, mBeginDate.get(Calendar.HOUR_OF_DAY), mBeginDate.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });
    }

    private boolean camShowDiffTime() {
        return !TextUtils.isEmpty(mEdtBeginDate.getText()) && !TextUtils.isEmpty(mEdtBeginTime.getText())
                && !TextUtils.isEmpty(mEdtEndDate.getText()) && !TextUtils.isEmpty(mEdtEndTime.getText());
    }

    private void showDiffTime() {
        if (camShowDiffTime()) {
            mTvTime.setText(String.valueOf(TimeUtil.getMinuteDiff(mBeginDate, mEndDate)));
        }
    }

    @OnClick(R.id.tvSave)
    void onClickSave() {

        if (mId != 0 && mActivityId != 0) {
            if (TimeUtil.getMinuteDiff(mBeginDate, mEndDate) < 1) {
                showToast(R.string.message_time_not_correct);
            } else {
                getPresenter().updateSleep(mId, SleepInput.builder()
                        .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                        .beginTime(TimeUtil.getDateTimeUTC(mBeginDate, TimeUtil.FormatType.TYPE_1))
                        .endTime(TimeUtil.getDateTimeUTC(mEndDate, TimeUtil.FormatType.TYPE_1))
                        .duration(TimeUtil.getMinuteDiff(mBeginDate, mEndDate))
                        .build());
            }
        } else {
            if (!mIsManual) {
                mBeginDate = Prefs.getInstance().get(PrefsKey.BEGIN_SLEEP_CALENDAR, Calendar.class);
            }
            if (TimeUtil.getMinuteDiff(mBeginDate, mEndDate) < 1) {
                showToast(R.string.message_time_not_correct);
            } else{
                getPresenter().createSleep(SleepInput.builder()
                        .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                        .beginTime(TimeUtil.getDateTimeUTC(mBeginDate, TimeUtil.FormatType.TYPE_1))
                        .endTime(TimeUtil.getDateTimeUTC(mEndDate, TimeUtil.FormatType.TYPE_1))
                        .duration(TimeUtil.getMinuteDiff(mBeginDate, mEndDate))
                        .build());
            }

        }


    }

    @OnClick(R.id.tvInput)
    void onClickInput() {
        mIsManual = !mIsManual;
        setUpViews();
    }

    private void setUpViews() {
        if (mIsManual) {
            mLlBeginTime.setVisibility(View.VISIBLE);
            mLlEndTime.setVisibility(View.VISIBLE);
            mTvInput.setText(getText(R.string.enter_automatic));
            mPlayView.setVisibility(View.INVISIBLE);
            mTvSave.setVisibility(View.VISIBLE);
        } else {
            mLlBeginTime.setVisibility(View.INVISIBLE);
            mLlEndTime.setVisibility(View.INVISIBLE);
            mTvInput.setText(getText(R.string.enter_manual));
            mPlayView.setVisibility(View.VISIBLE);
            mTvSave.setVisibility(View.INVISIBLE);
        }
    }

    private void setUpPlayView(boolean isStarted) {
        mPlayView.setTitle(isStarted ? getString(R.string.wake_up) : getString(R.string.start));
        mPlayView.setSrcLeft(isStarted ? R.drawable.ic_stop : R.drawable.ic_play);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mCountTimeInSecond = intent.getLongExtra(CommonUtil.EXTRA_KEY_TIME, 0);
            if (mCountTimeInSecond == 0) {
                mBeginDate = Calendar.getInstance();
            }
            mTvTime.setText(String.format(getString(R.string.time_format), mCountTimeInSecond / 60, mCountTimeInSecond % 60));
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void createSleepSuccess(Sleep sleep) {
        showToast("Duration: " + sleep.getDuration());

        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(sleep);

        moveBack(activity);
    }

    private bkdev.linh.mybaby.models.Activity getBabyActivity(Sleep sleep) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        User user = Prefs.getInstance().get(PrefsKey.USER, User.class);
        bkdev.linh.mybaby.models.Activity activity = new bkdev.linh.mybaby.models.Activity();
        activity.setBabyId(sleep.getBabyId());
        activity.setAuthorId(user.getId());
        activity.setContent(String.format(getString(R.string.sleep_activity_format), baby.getName(), sleep.getDuration(),
                sleep.getDuration() > 1 ? getString(R.string.minutes) : getString(R.string.minute)));
        activity.setType(ActivityType.SLEEP);
        activity.setFamilyId(baby.getFamilyId());
        activity.setDate(sleep.getBeginTime());
        activity.setTypeId(sleep.getId());

        return activity;

    }

    @Override
    public void getSleepSuccess(Sleep sleep) {
        mClContainer.setVisibility(View.VISIBLE);
        mIsManual = true;
        setUpViews();
        mTvInput.setVisibility(View.INVISIBLE);
        mEdtBeginTime.setText(TimeUtil.stringUTCToLocalString(sleep.getBeginTime(), DateFormat.HH_mm));
        mEdtBeginDate.setText(TimeUtil.stringUTCToLocalString(sleep.getBeginTime(), DateFormat.yyyy_MM_dd));
        mBeginDate.setTimeInMillis(TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, sleep.getBeginTime()));

        mEdtEndTime.setText(TimeUtil.stringUTCToLocalString(sleep.getEndTime(), DateFormat.HH_mm));
        mEdtEndDate.setText(TimeUtil.stringUTCToLocalString(sleep.getEndTime(), DateFormat.yyyy_MM_dd));
        mEndDate.setTimeInMillis(TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, sleep.getEndTime()));

        mTvTime.setText(CommonUtil.durationFormat(this, sleep.getDuration()));
    }

    @Override
    public void updateSleepSuccess(Sleep sleep) {
        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(sleep);
        ActivityInput activityInput = new ActivityInput(activity);
        getPresenter().updateActivity(mActivityId, activityInput);
    }

    @Override
    public void updateActivitySuccess(bkdev.linh.mybaby.models.Activity activity) {
        moveBack(activity);
    }

    private void moveBack(bkdev.linh.mybaby.models.Activity activity) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY, activity);
        intent.putExtras(bundle);

        if (!isTaskRoot()) {
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            MainActivityStarter.start(this, activity);
        }

    }


}
