package bkdev.linh.mybaby.ui.feed.solid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.type.BabyReactionType;
import butterknife.BindView;
import butterknife.ButterKnife;


public class BabyReactionAdapter extends BaseAdapter {

    private List<String> mTypes;


    public BabyReactionAdapter(@NonNull Context context, List<String> selections) {
        super(context);
        mTypes = selections;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);
        ((SelectionHolder) viewHolder).bindData(getContext(), mTypes.get(i));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SelectionHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_selection, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return mTypes == null ? 0 : mTypes.size();
    }

    static class SelectionHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView mImageView;
        @BindView(R.id.tvContent)
        TextView mTvContent;


        SelectionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(Context context, String selection) {
            switch (selection) {
                case BabyReactionType.HATE:
                case BabyReactionType.LIKE:
                    mImageView.setImageResource(R.drawable.ic_bottle);
                    break;
                case ActivityType.DIAPER:
                    mImageView.setImageResource(R.drawable.ic_diaper_2);
                    break;
                case ActivityType.SLEEP:
                    mImageView.setImageResource(R.drawable.ic_sleep_2);
                    break;
                case ActivityType.SOLID:
                    mImageView.setImageResource(R.drawable.ic_solid);
                    break;
                case ActivityType.WEIGHT:
                    mImageView.setImageResource(R.drawable.ic_scale);
                    break;
            }
        }

    }
}
