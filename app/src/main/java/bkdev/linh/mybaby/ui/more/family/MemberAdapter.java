package bkdev.linh.mybaby.ui.more.family;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.models.User;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MemberAdapter extends BaseAdapter {

    private List<User> mUsers;

    public MemberAdapter(@NonNull Context context, List<User> users) {
        super(context);
        mUsers = users;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);

        ((MemberHolder) viewHolder).bindData(mUsers.get(i));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MemberHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return mUsers == null ? 0 : mUsers.size();
    }

    static class MemberHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUsername)
        TextView mTvUsername;
        @BindView(R.id.tvFullName)
        TextView mTvFullName;

        MemberHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(User user) {
            mTvUsername.setText(user.getUsername());
            mTvFullName.setText(user.getFullName());
        }

    }
}
