package bkdev.linh.mybaby.ui.track;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.AnimationListener;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.OnClick;


@MakeActivityStarter
public class ActivityDetailFragment extends BaseFragment {

    @BindView(R.id.viewSpace)
    View mViewSpace;
    @BindView(R.id.llInfo)
    LinearLayout mLlInfo;
    @BindView(R.id.tvType)
    TextView mTvType;
    @BindAnim(R.anim.slide_bottom_in)
    Animation mSlideBottomIn;
    @BindAnim(R.anim.slide_bottom_out)
    Animation mSlideBottomOut;

    @Arg
    Activity mActivity;
    @BindView(R.id.imageView)
    ImageView mImageView;
    @BindView(R.id.tvContent)
    TextView mTvContent;
    @BindView(R.id.tvDateTime)
    TextView mTvDateTime;

    @Override
    public int getContentView() {
        return R.layout.fragment_activity_detail;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mTvType.setText(mActivity.getType());

        mLlInfo.startAnimation(mSlideBottomIn);

        mSlideBottomIn.setAnimationListener((AnimationListener) animation -> {
            mViewSpace.setVisibility(View.VISIBLE);
        });

        mSlideBottomOut.setAnimationListener((AnimationListener) animation -> {
            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
            }
        });

        mTvContent.setText(mActivity.getContent());
        mTvDateTime.setText(TimeUtil.stringUTCToLocalString(mActivity.getDate(), DateFormat.yyyy_MM_dd_HH_mm_ss));
        switch (mActivity.getType()) {
            case ActivityType.BREAST:
                mImageView.setImageResource(R.drawable.ic_breastfeeding);
                break;
            case ActivityType.BOTTLE:
                mImageView.setImageResource(R.drawable.ic_bottle);
                break;
            case ActivityType.DIAPER:
                mImageView.setImageResource(R.drawable.ic_diaper_2);
                break;
            case ActivityType.SLEEP:
                mImageView.setImageResource(R.drawable.ic_sleep_2);
                break;
            case ActivityType.SOLID:
                mImageView.setImageResource(R.drawable.ic_solid);
                break;
            case ActivityType.WEIGHT:
                mImageView.setImageResource(R.drawable.ic_scale);
                break;
            case ActivityType.HEIGHT:
                mImageView.setImageResource(R.drawable.ic_height);
                break;
        }
    }

    @OnClick
    void onClickViewSpace() {
        mViewSpace.setVisibility(View.GONE);
        mLlInfo.startAnimation(mSlideBottomOut);
    }


    public void onBackPress() {
        mViewSpace.setVisibility(View.GONE);
        mLlInfo.startAnimation(mSlideBottomOut);
    }
}
