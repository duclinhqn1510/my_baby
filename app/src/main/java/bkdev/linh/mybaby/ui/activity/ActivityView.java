package bkdev.linh.mybaby.ui.activity;

import java.util.List;
import java.util.Map;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.ActivityDetail;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public interface ActivityView extends MvpView {
    void getActivitiesSuccess(List<Activity> activities);

    void filterActivitiesSuccess(List<Activity> activities);

    void getActivityDetailsSuccess(List<ActivityDetail> activityDetails);

    void deleteActivitySuccess(Boolean b);

    void getBottleAdviceSuccess(String advice);

}
