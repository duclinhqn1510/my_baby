package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import butterknife.OnClick;

;

/**
 * Created by Linh NDD
 * on 3/30/2018.
 */

@MakeActivityStarter
public class ActionDialog extends BaseDialog {

    private DialogListener mDialogListener;

    @Override
    public void initValue(Bundle savedInstanceState) {
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_action;
    }

    @OnClick(R.id.tvEdit)
    void onClickEdit() {
        if (mDialogListener != null) {
            mDialogListener.onEdit();
        }
        dismiss();
    }

    @OnClick(R.id.tvDelete)
    void onClickDelete() {
        if (mDialogListener != null) {
            mDialogListener.onDelete();
        }
        dismiss();
    }

    public ActionDialog setDialogListener(DialogListener dialogListener) {
        mDialogListener = dialogListener;
        return this;
    }

}
