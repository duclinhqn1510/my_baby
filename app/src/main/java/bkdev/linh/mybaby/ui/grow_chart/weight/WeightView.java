package bkdev.linh.mybaby.ui.grow_chart.weight;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Weight;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface WeightView extends MvpView {
    void createWeightSuccess(Weight weight);

    void getWeightsSuccess(List<Weight> weights);

}
