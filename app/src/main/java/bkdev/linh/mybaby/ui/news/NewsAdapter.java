package bkdev.linh.mybaby.ui.news;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.models.News;
import bkdev.linh.mybaby.utils.GlideUtil;
import butterknife.BindView;
import butterknife.ButterKnife;


public class NewsAdapter extends BaseAdapter<NewsAdapter.NewsHolder> {
    private static final int LARGE_ITEM_TYPE = 0;
    private static final int NORMAL_ITEM_TYPE = 1;

    private List<News> mNewsList;

    public NewsAdapter(@NonNull Context context, List<News> items) {
        super(context);
        mNewsList = items;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);
        NewsHolder newsHolder = (NewsHolder) viewHolder;
        newsHolder.bindData(getContext(), mNewsList.get(i));
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == LARGE_ITEM_TYPE) {
            return new NewsHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_news_large, parent, false));
        } else {
            return new NewsHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_news_normal, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position > 0 && position % 3 == 0 ? LARGE_ITEM_TYPE : NORMAL_ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return mNewsList != null ? mNewsList.size() : 0 ;
    }

    static class NewsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgMain)
        ImageView mImgMain;
        @BindView(R.id.tvTitle)
        TextView mTvTitle;

        NewsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(Context context, News news) {
            GlideUtil.load(context, news.getImageUrl(), mImgMain);
            mTvTitle.setText(news.getTitle());
        }

    }
}
