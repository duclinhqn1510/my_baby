package bkdev.linh.mybaby.ui.track;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.ui.diaper.DiaperActivityStarter;
import bkdev.linh.mybaby.ui.feed.FeedingActivityStarter;
import bkdev.linh.mybaby.ui.grow_chart.GrowChartActivityStarter;
import bkdev.linh.mybaby.ui.sleep.SleepActivity;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.HeaderBarCustom;
import bkdev.linh.mybaby.views.ItemTrackerCustom;
import butterknife.BindView;
import butterknife.OnClick;
import link.fls.swipestack.SwipeStack;

import static bkdev.linh.mybaby.ui.main.MainActivity.DIAPER_REQUEST;
import static bkdev.linh.mybaby.ui.main.MainActivity.FEEDING_REQUEST;
import static bkdev.linh.mybaby.ui.main.MainActivity.SLEEPING_REQUEST;

/**
 * Created by Linh NDD
 * on 3/25/2018.
 */

@MakeActivityStarter
public class TrackFragment extends BaseFragment<TrackView, TrackPresenter> implements SwipeStack.SwipeStackListener, TrackView {

    @BindView(R.id.swipeStack)
    SwipeStack mSwipeStack;
    @BindView(R.id.scatterChart)
    ScatterChart mScatterChart;
    @BindView(R.id.itemTrackerFeed)
    ItemTrackerCustom mItemTrackerFeed;
    @BindView(R.id.itemTrackerSleep)
    ItemTrackerCustom mItemTrackerSleep;
    @BindView(R.id.itemTrackerDiaper)
    ItemTrackerCustom mItemTrackerDiaper;
    @BindView(R.id.headerBar)
    HeaderBarCustom mHeaderBar;

    private Baby mBaby;

    private List<String> mCardContents;
    private SwipeStackAdapter mAdapter;

    @Override
    protected TrackPresenter createPresenter() {
        return new TrackPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_track;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mBaby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        mHeaderBar.setTitle(mBaby.getName());
        mHeaderBar.setHeaderBarListener(new HeaderBarCustom.HeaderBarListener() {
            @Override
            public void onClickTvTitle() {
                if (getMainActivity() != null) {
                    getMainActivity().getBabies();
                }
            }
        });
        mCardContents = new ArrayList<String>() {{
            add(getString(R.string.tutorial_introduce));
            add(getString(R.string.tutorial_introduce));
            add(getString(R.string.tutorial_introduce));
            add(getString(R.string.tutorial_introduce));
        }};

        mAdapter = new SwipeStackAdapter(getBaseActivity(), mCardContents);
        mSwipeStack.setAdapter(mAdapter);
        mSwipeStack.setListener(this);


        setUpViews();
        mScatterChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Log.d("TAGGG", e.toString());
                ActivityDetailFragment activityDetailFragment = ActivityDetailFragmentStarter.newInstance((Activity) e.getData());
                getBaseActivity().addFragment(activityDetailFragment, R.id.flContainer);
            }

            @Override
            public void onNothingSelected() {

            }
        });

    }

    private void getActivities() {
        getPresenter().getActivitiesAWeekAgo(mBaby.getId());

    }

    public void setUpViews() {
        mItemTrackerSleep.setIsActived(Prefs.getInstance().get(PrefsKey.IS_SLEEP_TIMER_STARTED, Boolean.class));
        mItemTrackerFeed.setIsActived(Prefs.getInstance().get(PrefsKey.IS_BREAST_TIMER_STARTED, Boolean.class));
        getActivities();
    }

    protected ScatterData generateScatterData(Map<String, List<Activity>> activityMap) {
        ScatterData d = new ScatterData();
        Calendar dateAWeekAgo = Calendar.getInstance();
        dateAWeekAgo.add(Calendar.DATE, -7);

        for (Map.Entry mapEntry : activityMap.entrySet()) {
            ArrayList<Entry> entries = new ArrayList<>();
            for (int i = 0; i < activityMap.get(mapEntry.getKey().toString()).size(); i++) {
                Activity activity = activityMap.get(mapEntry.getKey().toString()).get(i);
                String to = TimeUtil.stringUTCToLocalString(activity.getDate(), DateFormat.dd_MM_yyyy_HH_mm);
                String from = TimeUtil.getTimeString(dateAWeekAgo, DateFormat.dd_MM_yyyy) + " 00:00";
                float dayDiff = TimeUtil.getDayDifferent(from, to);
                Log.d("TAGGG", from + " " + to + " " + dayDiff + " " + mapEntry.getKey().toString() + " " + (dayDiff - (int) dayDiff) * 24);
                Entry entry = new Entry(dayDiff, (dayDiff - (int) dayDiff) * 24);
                entry.setData(activity);
                entries.add(entry);
            }
            ScatterDataSet set = new ScatterDataSet(entries, mapEntry.getKey().toString());
            set.setScatterShapeSize(14f);
            set.setScatterShape(ScatterChart.ScatterShape.SQUARE);
            set.setDrawValues(false);
            set.setValueTextSize(11f);
            switch (mapEntry.getKey().toString()) {
                case ActivityType.BREAST:
                    set.setColor(Color.RED);
                    break;
                case ActivityType.BOTTLE:
                    set.setColor(Color.YELLOW);
                    break;
                case ActivityType.DIAPER:
                    set.setColor(Color.BLUE);
                    break;
                case ActivityType.SLEEP:
                    set.setColor(Color.MAGENTA);
                    break;
                case ActivityType.SOLID:
                    set.setColor(Color.GREEN);
                    break;
                case ActivityType.WEIGHT:
                    set.setColor(Color.BLACK);
                    break;
                case ActivityType.HEIGHT:
                    set.setColor(Color.GRAY);
                    break;
            }
            d.addDataSet(set);
        }

        return d;
    }

    protected float getRandom(float range, float startsfrom) {
        return (float) (Math.random() * range) + startsfrom;
    }


    @OnClick(R.id.itemTrackerDiaper)
    void onClickDiaper() {
        getBaseActivity().startActivityForResult(DiaperActivityStarter.getIntent(getBaseActivity()), DIAPER_REQUEST);
    }

    @OnClick(R.id.itemTrackerFeed)
    void onClickFeed() {
        getBaseActivity().startActivityForResult(FeedingActivityStarter.getIntent(getBaseActivity()), FEEDING_REQUEST);
    }

    @OnClick(R.id.itemTrackerSleep)
    void onClickSleep() {
        Intent intent = new Intent(getBaseActivity(), SleepActivity.class);
        intent.putExtra(CommonUtil.EXTRA_SERVICE_STARTED, Prefs.getInstance().get(PrefsKey.IS_SLEEP_TIMER_STARTED, Boolean.class));
        getBaseActivity().startActivityForResult(intent, SLEEPING_REQUEST);
    }

    @OnClick(R.id.itemGrowChart)
    void onGrowChart() {
        GrowChartActivityStarter.start(getBaseActivity());
    }

    @Override
    public void onViewSwipedToLeft(int position) {

    }

    @Override
    public void onViewSwipedToRight(int position) {

    }

    @Override
    public void onStackEmpty() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void getActivitiesSuccess(List<Activity> activities) {
        getPresenter().getFilterActivities(activities);
    }

    @Override
    public void getFilterActivitiesSuccess(Map<String, List<Activity>> activityMap) {
        try {
            Log.d("TAGGG", activityMap.size() + "");
            mScatterChart.setData(generateScatterData(activityMap));
            mScatterChart.setPinchZoom(false);
            mScatterChart.setDoubleTapToZoomEnabled(false);
            YAxis yl = mScatterChart.getAxisLeft();
            YAxis yr = mScatterChart.getAxisRight();
            XAxis xAxis = mScatterChart.getXAxis();
            xAxis.setAxisMaximum(8f);
            xAxis.setAxisMinimum(0f);
            yl.setAxisMinimum(0f);
            yl.setAxisMaximum(24f);
            yr.setAxisMaximum(24f);
            yr.setAxisMinimum(0f);
            mScatterChart.invalidate();

        } catch (Exception e) {
            Log.e("TAGGG", "error");
        }
    }


}
