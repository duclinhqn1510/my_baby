package bkdev.linh.mybaby.ui.moment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.moment.add.AddMomentActivityStarter;
import bkdev.linh.mybaby.ui.moment.detail.MomentDetailActivityStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.decor.GridItemDecoration;
import bkdev.linh.mybaby.views.HeaderBarCustom;
import bkdev.linh.mybaby.views.SwipeRefreshLayoutCustom;
import butterknife.BindView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class MomentFragment extends BaseFragment<MomentView, MomentPresenter> implements MomentView {

    private static final int ADD_MOMENT_REQUEST = 121;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.headerBar)
    HeaderBarCustom mHeaderBar;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayoutCustom mSwipeRefreshLayout;

    private List<Moment> mMoments;
    private MomentAdapter mAdapter;

    @Override
    public int getContentView() {
        return R.layout.fragment_moment;
    }

    @Override
    protected MomentPresenter createPresenter() {
        return new MomentPresenter();
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        mHeaderBar.setTitle(baby.getName());
        mHeaderBar.setHeaderBarListener(new HeaderBarCustom.HeaderBarListener() {
            @Override
            public void onClickTvTitle() {
                if (getMainActivity() != null) {
                    getMainActivity().getBabies();
                }
            }
        });
        mMoments = new ArrayList<>();
        mAdapter = new MomentAdapter(getBaseActivity(), mMoments);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getBaseActivity(), 2));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new GridItemDecoration((int) getResources().getDimension(R.dimen.dp_8)));
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getPresenter().getMoments(baby.getId(), true);
        });

        mAdapter.setOnItemClickListener((v, position) -> {
            if (mAdapter.getItemViewType(position) == MomentAdapter.CREATE_ITEM_TYPE) {
                startActivityForResult(AddMomentActivityStarter.getIntent(getBaseActivity()), ADD_MOMENT_REQUEST);
            } else {
                MomentDetailActivityStarter.start(getBaseActivity(), (ArrayList<Moment>) mMoments, position);
            }
        });

        getPresenter().getMoments(baby.getId(), false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_MOMENT_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                Bundle bundle = data.getExtras();
                Moment moment = bundle.getParcelable(CommonUtil.KEY_BUNDLE_MOMENT);
                mMoments.add(moment);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void getMomentsSuccess(List<Moment> moments) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            hideRefresh();
            mMoments.clear();
        }
        mMoments.addAll(moments);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void hideRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
