package bkdev.linh.mybaby.ui.feed.bottle;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Bottle;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface BottleView extends MvpView {
    void createBottleSuccess(Bottle bottle);

    void getBottleSuccess(Bottle bottle);

    void updateBottleSuccess(Bottle bottle);

    void updateActivitySuccess(Activity activity);

}
