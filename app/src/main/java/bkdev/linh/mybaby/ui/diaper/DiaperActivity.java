package bkdev.linh.mybaby.ui.diaper;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import java.util.Calendar;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Diaper;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.DiaperInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.type.DiaperColorType;
import bkdev.linh.mybaby.type.DiaperReasonType;
import bkdev.linh.mybaby.type.DiaperStatusType;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 5/10/2018.
 */

@MakeActivityStarter
public class DiaperActivity extends BaseActivity<DiaperView, DiaperPresenter> implements DiaperView {
    @BindView(R.id.edtDate)
    EditTextCustom mEdtDate;
    @BindView(R.id.edtTime)
    EditTextCustom mEdtTime;
    @BindView(R.id.cbPoo)
    CheckBox mCbPoo;
    @BindView(R.id.cbPee)
    CheckBox mCbPee;
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.llPoo)
    LinearLayout mLLPoo;
    @BindView(R.id.rgColors)
    RadioGroup mRgColors;
    @BindView(R.id.rgStatus)
    RadioGroup mRgStatus;
    @BindView(R.id.rbBlack)
    RadioButton mRbBlack;
    @BindView(R.id.rbYellow)
    RadioButton rmRbYellow;
    @BindView(R.id.rbBrown)
    RadioButton mRbBrown;
    @BindView(R.id.rbRunny)
    RadioButton mRbRunny;
    @BindView(R.id.rbSolid)
    RadioButton mRbSolid;
    @BindView(R.id.rbLittleBalls)
    RadioButton mRbLittleBalls;
    @BindView(R.id.rlContainer)
    RelativeLayout mRlContainer;

    private Calendar mDateOfFeeding;
    private String mColor;
    private String mStatus;
    private String mReason;

    @Arg(optional = true)
    int mActivityId;
    @Arg(optional = true)
    int mId;

    @Override
    protected DiaperPresenter createPresenter() {
        return new DiaperPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_diaper;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });
        mDateOfFeeding = Calendar.getInstance();

        mEdtDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mDateOfFeeding.set(year, monthOfYear, dayOfMonth);
                    mEdtDate.setText(TimeUtil.getTimeString(mDateOfFeeding));
                }
            }, mDateOfFeeding.get(Calendar.YEAR), mDateOfFeeding.get(Calendar.MONTH), mDateOfFeeding.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtTime.setPickerListener(() -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, (timePicker, hourOfDay, minute) -> {
                if (timePicker.isShown()) {
                    mDateOfFeeding.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    mDateOfFeeding.set(Calendar.MINUTE, minute);
                    mEdtTime.setText(TimeUtil.getTimeString(mDateOfFeeding, TimeUtil.FormatType.TYPE_6));
                }
            }, mDateOfFeeding.get(Calendar.HOUR_OF_DAY), mDateOfFeeding.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });

        mCbPoo.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            mLLPoo.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        mRgColors.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id) {
                case R.id.rbYellow:
                    mColor = DiaperColorType.YELLOW;
                    break;
                case R.id.rbBlack:
                    mColor = DiaperColorType.BLACK;
                    break;
                case R.id.rbBrown:
                    mColor = DiaperColorType.BROWN;
                    break;
            }
        });

        mRgStatus.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id) {
                case R.id.rbRunny:
                    mStatus = DiaperStatusType.RUNNY;
                    break;
                case R.id.rbSolid:
                    mStatus = DiaperStatusType.SOLID;
                    break;
                case R.id.rbLittleBalls:
                    mStatus = DiaperStatusType.LITTLE_BALLS;
                    break;
            }
        });

        if (mId != 0 && mActivityId != 0) {
            getPresenter().getDiaper(mId);
            mRlContainer.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void createDiaperSuccess(Diaper diaper) {

        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(diaper);
        moveBack(activity);

    }

    private bkdev.linh.mybaby.models.Activity getBabyActivity(Diaper diaper) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        User user = Prefs.getInstance().get(PrefsKey.USER, User.class);
        bkdev.linh.mybaby.models.Activity activity = new bkdev.linh.mybaby.models.Activity();
        activity.setBabyId(diaper.getBabyId());
        activity.setAuthorId(user.getId());
        activity.setType(ActivityType.DIAPER);
        activity.setFamilyId(baby.getFamilyId());
        activity.setDate(diaper.getTimeOfChanging());
        activity.setTypeId(diaper.getId());
        switch (diaper.getReasonOfChanging()) {
            case DiaperReasonType.PEE:
                activity.setContent(String.format(getString(R.string.diaper_format),
                        baby.getName(), getString(R.string.wet),
                        "").trim());
                break;
            case DiaperReasonType.POO:
                activity.setContent(String.format(getString(R.string.diaper_format),
                        baby.getName(),
                        getString(R.string.dirty),
                        String.format(getString(R.string.dirty_diaper_format),
                                CommonUtil.ColorMap.get(diaper.getColor()),
                                diaper.getStatus())));
                break;
            case DiaperReasonType.BOTH:
                activity.setContent(
                        String.format(getString(R.string.diaper_format),
                                baby.getName(), getString(R.string.wet) + " and " + getString(R.string.dirty),
                                String.format(getString(R.string.dirty_diaper_format),
                                        CommonUtil.ColorMap.get(diaper.getColor()),
                                        diaper.getStatus())));
                break;
            default:
                activity.setContent(String.format(getString(R.string.diaper_format),
                        baby.getName(),
                        getString(R.string.clean),
                        "").trim());
                break;

        }

        return activity;
    }

    private void moveBack(bkdev.linh.mybaby.models.Activity activity) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY, activity);
        intent.putExtras(bundle);

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void getDiaperSuccess(Diaper diaper) {
        mRlContainer.setVisibility(View.VISIBLE);
        mEdtTime.setText(TimeUtil.stringUTCToLocalString(diaper.getTimeOfChanging(), DateFormat.HH_mm));
        mEdtDate.setText(TimeUtil.stringUTCToLocalString(diaper.getTimeOfChanging(), DateFormat.yyyy_MM_dd));
        mDateOfFeeding.setTimeInMillis(TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, diaper.getTimeOfChanging()));
        switch (diaper.getReasonOfChanging()) {
            case DiaperReasonType.PEE:
                mCbPee.setChecked(true);
                break;
            case DiaperReasonType.POO:
                mCbPoo.setChecked(true);
                setUpPoo(diaper);
                break;
            case DiaperReasonType.BOTH:
                mCbPee.setChecked(true);
                mCbPoo.setChecked(true);
                setUpPoo(diaper);
                break;
        }
    }

    private void setUpPoo(Diaper diaper) {
        mLLPoo.setVisibility(View.VISIBLE);
        mRbBlack.setChecked(diaper.getColor().equals(DiaperColorType.BLACK));
        mRbBrown.setChecked(diaper.getColor().equals(DiaperColorType.BROWN));
        rmRbYellow.setChecked(diaper.getColor().equals(DiaperColorType.YELLOW));
        mRbRunny.setChecked(diaper.getStatus().equals(DiaperStatusType.RUNNY));
        mRbLittleBalls.setChecked(diaper.getStatus().equals(DiaperStatusType.LITTLE_BALLS));
        mRbSolid.setChecked(diaper.getStatus().equals(DiaperStatusType.SOLID));
    }

    @Override
    public void updateDiaperSuccess(Diaper diaper) {
        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(diaper);
        ActivityInput activityInput = new ActivityInput(activity);
        getPresenter().updateActivity(mActivityId, activityInput);

    }

    @Override
    public void updateActivitySuccess(bkdev.linh.mybaby.models.Activity activity) {
        moveBack(activity);
    }

    @OnClick(R.id.tvSave)
    void onClickSave() {
        if (mCbPee.isChecked() && mCbPoo.isChecked()) {
            mReason = DiaperReasonType.BOTH;
        } else if (mCbPee.isChecked()) {
            mReason = DiaperReasonType.PEE;
        } else if (mCbPoo.isChecked()) {
            mReason = DiaperReasonType.POO;
        } else {
            mReason = DiaperReasonType.CLEAN;
        }

        if (mId != 0 && mActivityId != 0) {
            getPresenter().updateDiaper(mId, DiaperInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .color(mColor)
                    .status(mStatus)
                    .reasonOfChanging(mReason)
                    .timeOfChanging(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .build());
        } else {
            getPresenter().createDiaper(DiaperInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .color(mColor)
                    .status(mStatus)
                    .reasonOfChanging(mReason)
                    .timeOfChanging(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .build());
        }
    }
}
