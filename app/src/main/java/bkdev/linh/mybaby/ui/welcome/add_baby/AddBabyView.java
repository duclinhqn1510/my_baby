package bkdev.linh.mybaby.ui.welcome.add_baby;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Family;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public interface AddBabyView extends MvpView {
    void getBabiesSuccess(List<Baby> babies);

    void getBottlesSuccess(Boolean isSaved);

    void getWeightsSuccess(Boolean isSaved);

    void getFamilySuccess(Family family);

    void addBabySuccess(Baby baby);
}
