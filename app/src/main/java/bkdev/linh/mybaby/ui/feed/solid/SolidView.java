package bkdev.linh.mybaby.ui.feed.solid;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Solid;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface SolidView extends MvpView {
    void createSolidSuccess(Solid solid);

    void getSolidSuccess(Solid solid);

    void updateSolidSuccess(Solid solid);

    void updateActivitySuccess(Activity activity);
}
