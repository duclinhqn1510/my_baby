package bkdev.linh.mybaby.ui.grow_chart;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.ui.grow_chart.weight.WeightFragmentStarter;
import bkdev.linh.mybaby.ui.grow_chart.height.HeightFragmentStarter;
import bkdev.linh.mybaby.views.HackyViewPager;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */

@MakeActivityStarter
public class GrowChartActivity extends BaseActivity {
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBarPrimary;
    @BindView(R.id.viewPager)
    HackyViewPager mViewPager;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;

    private GrowChartPagerAdapter mGrowChartPagerAdapter;

    @Override
    public int getContentView() {
        return R.layout.activity_grow_chart;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBarPrimary.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });

        mGrowChartPagerAdapter = new GrowChartPagerAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.grow_chat_tab_titles));
        mViewPager.setAdapter(mGrowChartPagerAdapter);
        mViewPager.setEnabled(false);
        mViewPager.setOffscreenPageLimit(mGrowChartPagerAdapter.getCount() - 1);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }

    private class GrowChartPagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<Fragment> mFragments = new SparseArray<>();
        private String[] mTitles;

        GrowChartPagerAdapter(FragmentManager fm, String[] titles) {
            super(fm);
            mTitles = titles;
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            mFragments.put(position, fragment);
            return fragment;
        }

        // Unregister when the item is inactive
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        // Returns the fragment for the position (if instantiated)
        public Fragment getFragment(int position) {
            return mFragments.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = WeightFragmentStarter.newInstance();
                    break;
                case 1:
                    fragment = HeightFragmentStarter.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
