package bkdev.linh.mybaby.ui.feed.bottle;

import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.databases.AppDatabase;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Bottle;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.BottleInput;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import bkdev.linh.mybaby.models.response.BottleResponse;
import bkdev.linh.mybaby.models.response.CreateBottleResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class BottlePresenter extends BasePresenter<BottleView> {

    public void createBottle(BottleInput bottleInput) {
        getView().showLoading();
        getCompositeDisposable().add(createBottleObservable(bottleInput)
                .subscribeWith(new CallbackWrapper<Bottle>() {
                    @Override
                    public void next(Bottle bottle) {
                        getView().createBottleSuccess(bottle);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

//    private void createBottleLocal(BottleInput bottleInput) {
//        getView().showLoading();
//        getCompositeDisposable().add(createBottleLocalObservable(bottleInput)
//                .subscribeWith(new CallbackWrapper<Bottle>() {
//                    @Override
//                    public void next(Bottle bottle) {
//                        getView().createBottleSuccess(bottle);
//                    }
//
//                    @Override
//                    public void complete() {
//                        getView().hideLoading();
//                    }
//
//                    @Override
//                    public void error(int code, String message) {
//                        getView().hideLoading();
//                        getView().onError(code, message);
//                    }
//                }));
//    }
//
//    private Observable<Bottle> createBottleLocalObservable(BottleInput bottleInput) {
//        Bottle bottle = new Bottle();
//        bottle.setVolume(bottleInput.getVolume());
//        bottle.setTimeOfFeeding(bottleInput.getTimeOfFeeding());
//        bottle.setUploaded(false);
//        bottle.setBabyId(bottleInput.getBabyId());
//        return Observable.just(mAppDatabase)
//                .doOnNext(appDatabase -> {
//                    appDatabase.bottleDao().insert(bottle);
//                })
//                .map(appDatabase -> bottle)
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui());
//    }

    public void updateBottle(int id, BottleInput bottleInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateBottleObservable(id, bottleInput)
                .subscribeWith(new CallbackWrapper<Bottle>() {
                    @Override
                    public void next(Bottle bottle) {
                        getView().updateBottleSuccess(bottle);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void updateActivity(int id, ActivityInput activityInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateActivityObservable(id, activityInput)
                .subscribeWith(new CallbackWrapper<Activity>() {
                    @Override
                    public void next(Activity activity) {
                        getView().updateActivitySuccess(activity);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void getBottle(int id) {
        getView().showLoading();
        getCompositeDisposable().add(getBottleObservable(id)
                .subscribeWith(new CallbackWrapper<Bottle>() {
                    @Override
                    public void next(Bottle bottle) {
                        getView().getBottleSuccess(bottle);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Bottle> updateBottleObservable(int id, BottleInput bottleInput) {
        return Observable.defer(() -> ApiClient.call().updateBottle(id, bottleInput)
                .filter(bottleResponse -> bottleResponse != null && bottleResponse.getBottle() != null)
                .map(BottleResponse::getBottle)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Bottle> getBottleObservable(int id) {
        return Observable.defer(() -> ApiClient.call().getBottle(id)
                .filter(bottleResponse -> bottleResponse != null && bottleResponse.getBottle() != null)
                .map(BottleResponse::getBottle)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Activity> updateActivityObservable(int id, ActivityInput activityInput) {
        return Observable.defer(() -> ApiClient.call().updateActivity(id, activityInput)
                .filter(activityResponse -> activityResponse != null && activityResponse.getActivity() != null)
                .map(ActivityResponse::getActivity)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Bottle> createBottleObservable(BottleInput bottleInput) {
        return Observable.defer(() -> ApiClient.call().createBottle(bottleInput)
                .filter(createBottleResponse -> createBottleResponse != null && createBottleResponse.getBottle() != null)
                .map(CreateBottleResponse::getBottle)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }





}
