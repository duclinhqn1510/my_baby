package bkdev.linh.mybaby.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;


/**
 * Created by Linh NDD
 * on 3/23/2018.
 */

public class MaterialDialog {
    private static Dialog sDialog;

    public static void showConfirmDialog(@NonNull final Context context, String message, String positiveText, String negativeText, DialogListener dialogListener) {
        if (sDialog != null && sDialog.isShowing()) {
            return;
        }
        sDialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(positiveText, (dialogInterface, i) -> {
                    if (dialogListener != null) {
                        dialogListener.onPositive();
                    }
                })
                .setNegativeButton(negativeText, (dialogInterface, i) -> {
                    sDialog.dismiss();
                })
                .show();
    }


}
