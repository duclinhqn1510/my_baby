package bkdev.linh.mybaby.ui.feed.breast;

import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.databases.AppDatabase;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Breast;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.BreastInput;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import bkdev.linh.mybaby.models.response.BreastResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class BreastPresenter extends BasePresenter<BreastView> {

    public void createBreast(BreastInput breastInput) {
        getView().showLoading();
        getCompositeDisposable().add(createBreastObservable(breastInput)
                .subscribeWith(new CallbackWrapper<Breast>() {
                    @Override
                    public void next(Breast breast) {
                        getView().createBreastSuccess(breast);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void updateBreast(int id, BreastInput breastInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateBreastObservable(id, breastInput)
                .subscribeWith(new CallbackWrapper<Breast>() {
                    @Override
                    public void next(Breast breast) {
                        getView().updateBreastSuccess(breast);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void updateActivity(int id, ActivityInput activityInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateActivityObservable(id, activityInput)
                .subscribeWith(new CallbackWrapper<Activity>() {
                    @Override
                    public void next(Activity activity) {
                        getView().updateActivitySuccess(activity);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void getBreast(int id) {
        getView().showLoading();
        getCompositeDisposable().add(getBreastObservable(id)
                .subscribeWith(new CallbackWrapper<Breast>() {
                    @Override
                    public void next(Breast breast) {
                        getView().getBreastSuccess(breast);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Breast> updateBreastObservable(int id, BreastInput breastInput) {
        return Observable.defer(() -> ApiClient.call().updateBreast(id, breastInput)
                .filter(breastResponse -> breastResponse != null && breastResponse.getBreast() != null)
                .map(BreastResponse::getBreast)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Breast> getBreastObservable(int id) {
        return Observable.defer(() -> ApiClient.call().getBreast(id)
                .filter(breastResponse -> breastResponse != null && breastResponse.getBreast() != null)
                .map(BreastResponse::getBreast)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Activity> updateActivityObservable(int id, ActivityInput activityInput) {
        return Observable.defer(() -> ApiClient.call().updateActivity(id, activityInput)
                .filter(activityResponse -> activityResponse != null && activityResponse.getActivity() != null)
                .map(ActivityResponse::getActivity)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Breast> createBreastObservable(BreastInput breastInput) {
        return Observable.defer(() -> ApiClient.call().createBreast(breastInput)
                .filter(breastResponse -> breastResponse != null && breastResponse.getBreast() != null)
                .map(BreastResponse::getBreast)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }


}
