package bkdev.linh.mybaby.ui.dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.widget.TextView;

import java.util.Calendar;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import bkdev.linh.mybaby.models.input.BabyInput;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.utils.ValidationUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import butterknife.BindView;
import butterknife.OnClick;


@MakeActivityStarter
public class AddNewBabyDialog extends BaseDialog {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.edtName)
    TextInputEditText mEdtName;
    @BindView(R.id.edtGender)
    EditTextCustom mEdtGender;
    @BindView(R.id.edtBirthday)
    EditTextCustom mEdtBirthday;

    private DialogListener mDialogListener;
    private Calendar mDateSelected;
    private int mSelectedGenderType = -1;

    @Arg
    int mFamilyId;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTvTitle.setText(getBaseActivity().getText(R.string.add_new_baby));
        mDateSelected = Calendar.getInstance();
        mEdtBirthday.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mDateSelected.set(year, monthOfYear, dayOfMonth);
                    mEdtBirthday.setText(TimeUtil.dateToLocalString(mDateSelected.getTime(), DateFormat.yyyy_MM_dd));
                }
            }, mDateSelected.get(Calendar.YEAR), mDateSelected.get(Calendar.MONTH), mDateSelected.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtGender.setPickerListener(() -> {
            getBaseActivity().getSafeTransaction().registerTransition(fragmentManager -> {
                GenderDialogStarter.newInstance(mSelectedGenderType)
                        .setDialogListener(new DialogListener() {
                            @Override
                            public void onSelectedItem(int genderType, String text) {
                                mSelectedGenderType = genderType;
                                mEdtGender.setText(text);
                            }
                        }).show(fragmentManager);
            });
        });
    }

    private boolean validateInfo() {
        if (!ValidationUtil.validateName(getBaseActivity(), mEdtName.getText().toString().trim())) {
            return false;
        }
        if (TextUtils.isEmpty(mEdtBirthday.getText().trim())) {
            getBaseActivity().showToast(R.string.message_date_empty);
            return false;
        }
        if (TextUtils.isEmpty(mEdtGender.getText().trim())) {
            getBaseActivity().showToast(R.string.message_gender_empty);
            return false;
        }
        return true;
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_add_new_baby;
    }

    @OnClick(R.id.tvPositive)
    void onPositive() {
        if (validateInfo()) {
            if (mDialogListener != null) {
                mDialogListener.onAddBaby(BabyInput.builder()
                        .name(mEdtName.getText().toString().trim())
                        .gender(mSelectedGenderType)
                        .birthday(TimeUtil.getDateTimeUTC(mDateSelected, TimeUtil.FormatType.TYPE_1))
                        .familyId(mFamilyId)
                        .build());
            }
            dismiss();
        }
    }

    @OnClick(R.id.tvNegative)
    void onNegative() {
        dismiss();
    }

    public AddNewBabyDialog setDialogListener(DialogListener dialogListener) {
        mDialogListener = dialogListener;
        return this;
    }


}
