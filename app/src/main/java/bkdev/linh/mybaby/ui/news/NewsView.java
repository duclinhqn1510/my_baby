package bkdev.linh.mybaby.ui.news;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.News;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface NewsView extends MvpView {

    void getNewsListSuccess(List<News> newsList);
}
