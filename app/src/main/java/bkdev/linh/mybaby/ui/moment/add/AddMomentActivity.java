package bkdev.linh.mybaby.ui.moment.add;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Image;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.models.input.CreateMomentInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.dialog.DialogListener;
import bkdev.linh.mybaby.ui.dialog.MaterialDialog;
import bkdev.linh.mybaby.ui.dialog.PhotoPickerDialogStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.FileUtil;
import bkdev.linh.mybaby.utils.GlideUtil;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Linh NDD
 * on 4/17/2018.
 */
@MakeActivityStarter
public class AddMomentActivity extends BaseActivity<AddMomentView, AddMomentPresenter> implements HeaderBarPrimaryCustom.HeaderBarListener, AddMomentView {
    private static final int CAMERA_REQUEST = 111;
    private static final int GALLERY_REQUEST = 112;

    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.tvAdd)
    TextView mTvAdd;
    @BindView(R.id.imageView)
    ImageView mImageView;
    @BindView(R.id.edtCaption)
    EditText mEdtCaption;

    private String mCameraPhotoPath;
    private String mGalleryPhotoPath;
    private Uri mAvatarImageUri;
    private int mRequestType;

    @Override
    protected AddMomentPresenter createPresenter() {
        return new AddMomentPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_add_moment;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickTvRight() {
                onUploadImage();
            }

            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });

        RxView.clicks(mTvAdd)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    getSafeTransaction().registerTransition(fragmentManager -> {
                        PhotoPickerDialogStarter.newInstance()
                                .setDialogListener(new DialogListener() {
                                    @Override
                                    public void onCamera() {
                                        onCameraPermission();
                                    }

                                    @Override
                                    public void onGallery() {
                                        onGalleryPermission();
                                    }
                                }).show(fragmentManager);
                    });
                });
    }

    private void onUploadImage() {
        MultipartBody.Part photo = null;
        File file;
        boolean hasError = false;
        if (null != mAvatarImageUri) {
            try {
                switch (mRequestType) {
                    case CAMERA_REQUEST:
                        file = FileUtil.resizeImageFile(mCameraPhotoPath);
                        mAvatarImageUri = Uri.fromFile(file);
                        photo = MultipartBody.Part.createFormData("photo", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                        break;
                    case GALLERY_REQUEST:
                        if (TextUtils.isEmpty(mGalleryPhotoPath)) {
                            mGalleryPhotoPath = FileUtil.getRealPathFromURI(AddMomentActivity.this, mAvatarImageUri);
                        }
                        file = FileUtil.resizeImageFile(mGalleryPhotoPath);
                        mAvatarImageUri = Uri.fromFile(file);
                        photo = MultipartBody.Part.createFormData("photo", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                        break;
                }
            } catch (Exception e) {
                Log.d(AddMomentActivity.class.getSimpleName(), e.getMessage());
                showToast(R.string.error);
                hasError = true;
            }
            if (!hasError) {
                getPresenter().uploadImage(photo);
            }
        } else {
            showToast(R.string.error);
        }
    }

    private void onCameraPermission() {
        final int[] alreadyStartIntent = {0};
        RxPermissions rxPermissions = new RxPermissions(AddMomentActivity.this);
        rxPermissions.requestEach(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(permission -> {
                    if (permission.granted) {
                        alreadyStartIntent[0] = alreadyStartIntent[0] + 1;
                        if (alreadyStartIntent[0] == 3) {
                            takePhoto();
                        }
                    } else {
                        if (!permission.shouldShowRequestPermissionRationale) {
                            showSettingConfirmDialog();
                        }
                    }
                });
    }

    private void onGalleryPermission() {
        final int[] alreadyStartIntent = {0};
        RxPermissions rxPermissions = new RxPermissions(AddMomentActivity.this);
        rxPermissions.requestEach(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(permission -> {
                    if (permission.granted) {
                        alreadyStartIntent[0] = alreadyStartIntent[0] + 1;
                        if (alreadyStartIntent[0] == 2) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, GALLERY_REQUEST);
                        }
                    } else {
                        if (!permission.shouldShowRequestPermissionRationale) {
                            showSettingConfirmDialog();
                        }
                    }
                });
    }

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = FileUtil.createTemplateImageFile(AddMomentActivity.this);
                mCameraPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                showToast(R.string.error);
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        getPackageName(),
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.setClipData(ClipData.newRawUri("", photoURI));
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private void showSettingConfirmDialog() {
        MaterialDialog.showConfirmDialog(AddMomentActivity.this,
                getString(R.string.message_request_read_external),
                getString(R.string.agree),
                getString(R.string.cancel),
                new DialogListener() {
                    @Override
                    public void onPositive() {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        startActivity(intent);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && null != data) {
            if (null != data.getData()) {
                mAvatarImageUri = data.getData();
                mGalleryPhotoPath = "";
                mRequestType = GALLERY_REQUEST;
                GlideUtil.load(this, mAvatarImageUri, mImageView);
                mHeaderBar.setTvRight(getString(R.string.done));
                mTvAdd.setVisibility(View.GONE);
            } else {
                showToast(R.string.error);
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            mAvatarImageUri = Uri.fromFile(new File(mCameraPhotoPath));
            mRequestType = CAMERA_REQUEST;
            GlideUtil.load(this, mAvatarImageUri, mImageView);
            mHeaderBar.setTvRight(getString(R.string.done));
            mTvAdd.setVisibility(View.GONE);
        }
    }

    /**
     * click back
     */
    @Override
    public void onClickImgLeft() {
        onBackPressed();
    }

    /**
     * click done
     */
    @Override
    public void onClickTvRight() {

    }

    @Override
    public void uploadImageSuccess(Image image) {
        mHeaderBar.setTvRight("");
        getPresenter().createMoment(CreateMomentInput.builder()
                .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                .imageUrl(image.getImageUrl())
                .caption(mEdtCaption.getText().toString().trim())
                .build());
    }

    @Override
    public void createMomentSuccess(Moment moment) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonUtil.KEY_BUNDLE_MOMENT, moment);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();

    }
}
