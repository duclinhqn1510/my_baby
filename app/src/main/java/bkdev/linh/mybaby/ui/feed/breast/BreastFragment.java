package bkdev.linh.mybaby.ui.feed.breast;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Breast;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.BreastInput;
import bkdev.linh.mybaby.service.TimeRecordService;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.type.BreastSideType;
import bkdev.linh.mybaby.ui.dialog.DialogListener;
import bkdev.linh.mybaby.ui.dialog.NumberPickerDialogStarter;
import bkdev.linh.mybaby.ui.main.MainActivityStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import bkdev.linh.mybaby.views.PlayViewCustom;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class BreastFragment extends BaseFragment<BreastView, BreastPresenter> implements BreastView {

    @BindView(R.id.tvTime)
    TextView mTvTime;
    @BindView(R.id.tvSave)
    TextView mTvSave;
    @BindView(R.id.playViewLeft)
    PlayViewCustom mPlayViewLeft;
    @BindView(R.id.playViewRight)
    PlayViewCustom mPlayViewRight;
    @BindView(R.id.edtDate)
    EditTextCustom mEdtDate;
    @BindView(R.id.edtLeftDuration)
    EditTextCustom mEdtLeft;
    @BindView(R.id.edtRightDuration)
    EditTextCustom mEdtRight;
    @BindView(R.id.edtTime)
    EditTextCustom mEdtTime;
    @BindView(R.id.llTime)
    LinearLayout mLlTime;
    @BindView(R.id.llDuration)
    LinearLayout mLlDuration;
    @BindView(R.id.llCenter)
    LinearLayout mLlCenter;

    private int mBreastSide;
    private boolean mIsStarted;
    private long mTime;
    private long mLeftTime;
    private long mRightTime;
    private Calendar mDateOfFeeding;

    @Arg(optional = true)
    int mId;
    @Arg(optional = true)
    int mActivityId;

    @Override
    protected BreastPresenter createPresenter() {
        return new BreastPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_breast;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mBreastSide = Prefs.getInstance().get(PrefsKey.BREAST_SIDE, Integer.class);
        mIsStarted = Prefs.getInstance().get(PrefsKey.IS_BREAST_TIMER_STARTED, Boolean.class);
        if (mIsStarted) {
            mTvSave.setVisibility(View.VISIBLE);
            switch (mBreastSide) {
                case BreastSideType.LEFT:
                    setUPLeft(true);
                    setUPRight(false);
                    break;
                case BreastSideType.RIGHT:
                    setUPRight(true);
                    setUPLeft(false);
                    break;
            }
        }

        mDateOfFeeding = Calendar.getInstance();

        mEdtDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mDateOfFeeding.set(year, monthOfYear, dayOfMonth);
                    mEdtDate.setText(TimeUtil.getTimeString(mDateOfFeeding));
                }
            }, mDateOfFeeding.get(Calendar.YEAR), mDateOfFeeding.get(Calendar.MONTH), mDateOfFeeding.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtTime.setPickerListener(() -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(getBaseActivity(), (timePicker, hourOfDay, minute) -> {
                if (timePicker.isShown()) {
                    mDateOfFeeding.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    mDateOfFeeding.set(Calendar.MINUTE, minute);
                    mEdtTime.setText(TimeUtil.getTimeString(mDateOfFeeding, TimeUtil.FormatType.TYPE_6));
                }
            }, mDateOfFeeding.get(Calendar.HOUR_OF_DAY), mDateOfFeeding.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });


        RxView.clicks(mPlayViewLeft)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    mBreastSide = BreastSideType.LEFT;
                    setUPLeft(true);
                    setUPRight(false);
                    getBaseActivity().sendBroadcast(new Intent(CommonUtil.BREAST_TIMER).putExtra(CommonUtil.EXTRA_KEY_BREAST_SIDE, mBreastSide));
                    if (!mIsStarted) {
                        mTvSave.setVisibility(View.VISIBLE);
                        Prefs.getInstance().put(PrefsKey.IS_BREAST_TIMER_STARTED, mIsStarted);
                        getBaseActivity().startService(new Intent(getBaseActivity(), TimeRecordService.class).putExtra(CommonUtil.EXTRA_ACTIVITY_TYPE, ActivityType.BREAST));
                    }
                    mIsStarted = true;
                });
        RxView.clicks(mPlayViewRight)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    mBreastSide = BreastSideType.RIGHT;
                    setUPLeft(false);
                    setUPRight(true);
                    getBaseActivity().sendBroadcast(new Intent(CommonUtil.BREAST_TIMER).putExtra(CommonUtil.EXTRA_KEY_BREAST_SIDE, mBreastSide));
                    if (!mIsStarted) {
                        mTvSave.setVisibility(View.VISIBLE);
                        Prefs.getInstance().put(PrefsKey.IS_BREAST_TIMER_STARTED, mIsStarted);
                        getBaseActivity().startService(new Intent(getBaseActivity(), TimeRecordService.class).putExtra(CommonUtil.EXTRA_ACTIVITY_TYPE, ActivityType.BREAST));
                    }
                    mIsStarted = true;
                });

        mEdtLeft.setPickerListener(() ->
                getBaseActivity().getSafeTransaction().registerTransition(fragmentManager ->
                        NumberPickerDialogStarter.newInstance((int) mLeftTime).setDialogListener(new DialogListener() {
                            @Override
                            public void onPositive(int number) {
                                mLeftTime = number;
                                mEdtLeft.setText(String.valueOf(mLeftTime));
                                mTvTime.setText(CommonUtil.durationFormat(getBaseActivity(), getTotalTime()));
                            }
                        }).show(fragmentManager)));


        mEdtRight.setPickerListener(() ->
                getBaseActivity().getSafeTransaction().registerTransition(fragmentManager ->
                        NumberPickerDialogStarter.newInstance((int) mRightTime).setDialogListener(new DialogListener() {
                            @Override
                            public void onPositive(int number) {
                                mRightTime = number;
                                mEdtRight.setText(String.valueOf(mRightTime));
                                mTvTime.setText(CommonUtil.durationFormat(getBaseActivity(), getTotalTime()));
                            }
                        }).show(fragmentManager)));


        getBaseActivity().registerReceiver(mBroadcastReceiver, new IntentFilter(ActivityType.BREAST));

        if (mId != 0 && mActivityId != 0) {
            Log.d("TAGGG", "edit");
            loadToEdit();
            mLlCenter.setVisibility(View.INVISIBLE);
        }
    }

    private void loadToEdit() {
        setUpView(false);
        if (mId != 0 && mActivityId != 0) {
            getPresenter().getBreast(mId);
        }
    }


    private void setUpView(boolean isHasData) {
        mTvTime.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
        mTvSave.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
        mLlTime.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
        mLlDuration.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mLeftTime = intent.getLongExtra(CommonUtil.EXTRA_KEY_LEFT_TIME, 0);
            mRightTime = intent.getLongExtra(CommonUtil.EXTRA_KEY_RIGHT_TIME, 0);
            mTime = getTotalTime();
            mTvTime.setText(String.format(getString(R.string.time_format), mTime / 60, mTime % 60));
            mPlayViewLeft.setTime(String.format(getString(R.string.time_format), mLeftTime / 60, mLeftTime % 60));
            mPlayViewRight.setTime(String.format(getString(R.string.time_format), mRightTime / 60, mRightTime % 60));
        }
    };

    @OnClick(R.id.tvSave)
    void onClickSave() {
        if (mId != 0 && mActivityId != 0) {
            getPresenter().updateBreast(mId, BreastInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .timeOfFeeding(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .leftDuration(mLeftTime)
                    .rightDuration(mRightTime)
                    .build());
        } else {
            mIsStarted = false;
            Prefs.getInstance().put(PrefsKey.IS_BREAST_TIMER_STARTED, mIsStarted);
            getBaseActivity().stopService(new Intent(getBaseActivity(), TimeRecordService.class));
            if (mTime < 60) {
                getBaseActivity().showToast(R.string.message_time_not_correct);
                return;
            }
            getBaseActivity().stopService(new Intent(getBaseActivity(), TimeRecordService.class));
            getBaseActivity().showToast(mLeftTime + "" + mRightTime);
            Prefs.getInstance().put(PrefsKey.BREAST_LEFT_TIME, 0L);
            Prefs.getInstance().put(PrefsKey.BREAST_RIGHT_TIME, 0L);
            getPresenter().createBreast(BreastInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .timeOfFeeding(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .leftDuration((long) Math.ceil((double) mLeftTime / 60))
                    .rightDuration((long) Math.ceil((double) mRightTime / 60))
                    .build());
        }
    }

    private void setUPLeft(boolean isStared) {
        mPlayViewLeft.setSrcLeft(isStared ? R.drawable.ic_stop : R.drawable.ic_play);
    }

    private void setUPRight(boolean isStared) {
        mPlayViewRight.setSrcLeft(isStared ? R.drawable.ic_stop : R.drawable.ic_play);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getBaseActivity().unregisterReceiver(mBroadcastReceiver);


    }

    @Override
    public void onPause() {
        super.onPause();
        Prefs.getInstance().put(PrefsKey.BREAST_LEFT_TIME, mLeftTime);
        Prefs.getInstance().put(PrefsKey.BREAST_RIGHT_TIME, mRightTime);
        Prefs.getInstance().put(PrefsKey.BREAST_SIDE, mBreastSide);
        Prefs.getInstance().put(PrefsKey.IS_BREAST_TIMER_STARTED, mIsStarted);
    }

    @Override
    public void createBreastSuccess(Breast breast) {
        getBaseActivity().showToast("Breast" + breast.getId());

        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(breast);

        moveBack(activity);
    }

    private void moveBack(bkdev.linh.mybaby.models.Activity activity) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY, activity);
        intent.putExtras(bundle);
        if (!getBaseActivity().isTaskRoot()) {
            getBaseActivity().setResult(Activity.RESULT_OK, intent);
            getBaseActivity().finish();
        } else {
            MainActivityStarter.start(getBaseActivity(), activity);
        }

    }

    private bkdev.linh.mybaby.models.Activity getBabyActivity(Breast breast) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        User user = Prefs.getInstance().get(PrefsKey.USER, User.class);
        bkdev.linh.mybaby.models.Activity activity = new bkdev.linh.mybaby.models.Activity();
        activity.setBabyId(breast.getBabyId());
        activity.setAuthorId(user.getId());
        activity.setContent(String.format(getString(R.string.breast_activity_format),
                baby.getName(), breast.getLeftDuration() + breast.getRightDuration(),
                breast.getLeftDuration(), breast.getRightDuration()));
        activity.setType(ActivityType.BREAST);
        activity.setFamilyId(baby.getFamilyId());
        activity.setDate(breast.getTimeOfFeeding());
        activity.setTypeId(breast.getId());
        return activity;
    }

    @Override
    public void getBreastSuccess(Breast breast) {
        setUpView(true);
        mLeftTime = breast.getLeftDuration();
        mRightTime = breast.getRightDuration();
        mEdtTime.setText(TimeUtil.stringUTCToLocalString(breast.getTimeOfFeeding(), DateFormat.HH_mm));
        mEdtDate.setText(TimeUtil.stringUTCToLocalString(breast.getTimeOfFeeding(), DateFormat.yyyy_MM_dd));
        mDateOfFeeding.setTimeInMillis(TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, breast.getTimeOfFeeding()));
        mEdtLeft.setText(String.valueOf(breast.getLeftDuration()));
        mEdtRight.setText(String.valueOf(breast.getRightDuration()));
        mTvTime.setText(CommonUtil.durationFormat(getBaseActivity(), getTotalTime()));
        mLlDuration.setVisibility(View.VISIBLE);
    }

    private long getTotalTime() {
        return mLeftTime + mRightTime;
    }


    @Override
    public void updateBreastSuccess(Breast breast) {
        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(breast);
        ActivityInput activityInput = new ActivityInput(activity);
        getPresenter().updateActivity(mActivityId, activityInput);
    }

    @Override
    public void updateActivitySuccess(bkdev.linh.mybaby.models.Activity activity) {
        moveBack(activity);
    }
}
