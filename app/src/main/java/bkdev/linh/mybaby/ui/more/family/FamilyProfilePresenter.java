package bkdev.linh.mybaby.ui.more.family;

import java.util.List;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.response.BabiesResponse;
import bkdev.linh.mybaby.models.response.UsersResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/20/2018.
 */

public class FamilyProfilePresenter extends BasePresenter<FamilyProfileView> {

    public void getBabies(int familyId) {
        getView().showLoading();
        getCompositeDisposable().add(getBabiesObservable(familyId)
                .subscribeWith(new CallbackWrapper<List<Baby>>() {
                    @Override
                    public void next(List<Baby> babies) {
                        getView().getBabiesSuccess(babies);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void getFamilyMember(int familyId) {
        getView().showLoading();
        getCompositeDisposable().add(getFamilyMembersObservable(familyId)
                .subscribeWith(new CallbackWrapper<List<User>>() {
                    @Override
                    public void next(List<User> users) {
                        getView().getUsersSuccess(users);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<List<Baby>> getBabiesObservable(int familyId) {
        return Observable.defer(() -> ApiClient.call().getBabies(familyId)
                .filter(babiesResponse -> babiesResponse != null && babiesResponse.getBabies() != null)
                .map(BabiesResponse::getBabies)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<List<User>> getFamilyMembersObservable(int familyId) {
        return Observable.defer(() -> ApiClient.call().getFamilyMembers(familyId)
                .filter(usersResponse -> usersResponse != null && usersResponse.getUsers() != null)
                .map(UsersResponse::getUsers)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

}
