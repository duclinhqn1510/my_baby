package bkdev.linh.mybaby.ui.feed.solid;

import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.databases.AppDatabase;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Solid;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.SolidInput;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import bkdev.linh.mybaby.models.response.SolidResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class SolidPresenter extends BasePresenter<SolidView> {

    public void createSolid(SolidInput solidInput) {
        getView().showLoading();
        getCompositeDisposable().add(createSolidObservable(solidInput)
                .subscribeWith(new CallbackWrapper<Solid>() {
                    @Override
                    public void next(Solid solid) {
                        getView().createSolidSuccess(solid);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void updateSolid(int id, SolidInput solidInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateSolidObservable(id, solidInput)
                .subscribeWith(new CallbackWrapper<Solid>() {
                    @Override
                    public void next(Solid solid) {
                        getView().updateSolidSuccess(solid);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void updateActivity(int id, ActivityInput activityInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateActivityObservable(id, activityInput)
                .subscribeWith(new CallbackWrapper<Activity>() {
                    @Override
                    public void next(Activity activity) {
                        getView().updateActivitySuccess(activity);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void getSolid(int id) {
        getView().showLoading();
        getCompositeDisposable().add(getSolidObservable(id)
                .subscribeWith(new CallbackWrapper<Solid>() {
                    @Override
                    public void next(Solid solid) {
                        getView().getSolidSuccess(solid);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Solid> updateSolidObservable(int id, SolidInput solidInput) {
        return Observable.defer(() -> ApiClient.call().updateSolid(id, solidInput)
                .filter(solidResponse -> solidResponse != null && solidResponse.getSolid() != null)
                .map(SolidResponse::getSolid)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Solid> getSolidObservable(int id) {
        return Observable.defer(() -> ApiClient.call().getSolid(id)
                .filter(solidResponse -> solidResponse != null && solidResponse.getSolid() != null)
                .map(SolidResponse::getSolid)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Activity> updateActivityObservable(int id, ActivityInput activityInput) {
        return Observable.defer(() -> ApiClient.call().updateActivity(id, activityInput)
                .filter(activityResponse -> activityResponse != null && activityResponse.getActivity() != null)
                .map(ActivityResponse::getActivity)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Solid> createSolidObservable(SolidInput solidInput) {
        return Observable.defer(() -> ApiClient.call().createSolid(solidInput)
                .filter(solidResponse -> solidResponse != null && solidResponse.getSolid() != null)
                .map(SolidResponse::getSolid)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }
}
