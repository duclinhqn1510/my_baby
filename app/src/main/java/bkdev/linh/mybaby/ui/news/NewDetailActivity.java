package bkdev.linh.mybaby.ui.news;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.News;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/25/2018.
 */
@MakeActivityStarter
public class NewDetailActivity extends BaseActivity {
    @Arg
    News mNews;

    @BindView(R.id.webView)
    WebView mWebView;
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;

    @Override
    public int getContentView() {
        return R.layout.activity_news;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });
        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showLoading();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideLoading();
            }
        };

        mWebView.setWebViewClient(webViewClient);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDefaultTextEncodingName("utf-8");
        mWebView.loadData(mNews.getContent(), "text/html; charset=utf-8", "utf-8");
    }
}
