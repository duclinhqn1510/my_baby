package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import butterknife.BindView;
import butterknife.OnClick;

;

/**
 * Created by Linh NDD
 * on 3/30/2018.
 */

@MakeActivityStarter
public class NumberPickerDialog extends BaseDialog {

    @BindView(R.id.tvNegative)
    TextView mTvNegative;
    @BindView(R.id.numberPicker)
    NumberPicker mNumberPicker;

    @Arg
    int mNumber;

    private DialogListener mDialogListener;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTvNegative.setVisibility(View.GONE);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setMaxValue(90);
        mNumberPicker.setValue(mNumber);
        mNumberPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            mNumber = newVal;
        });
    }


    @OnClick(R.id.tvPositive)
    void onPositive() {
        if (mDialogListener != null) {
            mDialogListener.onPositive(mNumber);
        }
        dismiss();
    }

    public NumberPickerDialog setDialogListener(DialogListener dialogListener) {
        mDialogListener = dialogListener;
        return this;
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_number_picker;
    }

}
