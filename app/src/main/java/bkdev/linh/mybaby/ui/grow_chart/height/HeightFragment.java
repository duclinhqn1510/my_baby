package bkdev.linh.mybaby.ui.grow_chart.height;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Height;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.events.ActivityEvent;
import bkdev.linh.mybaby.models.input.CreateHeightInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.ui.dialog.DialogListener;
import bkdev.linh.mybaby.ui.dialog.GrowthDialogStarter;
import bkdev.linh.mybaby.ui.dialog.MessageDialogStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/6/2018.
 */
@MakeActivityStarter
public class HeightFragment extends BaseFragment<HeightView, HeightPresenter> implements HeightView {

    @BindView(R.id.lineChart)
    LineChart mLineChart;
    @BindView(R.id.fabAdd)
    FloatingActionButton mFabAdd;


    @Override
    protected HeightPresenter createPresenter() {
        return new HeightPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_height;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mLineChart.setPinchZoom(false);
        mLineChart.setDoubleTapToZoomEnabled(false);

        mFabAdd.setOnClickListener(view1 ->
                getBaseActivity().getSafeTransaction().registerTransition(fragmentManager ->
                        GrowthDialogStarter.newInstance(getString(R.string.height), getString(R.string.cm))
                                .setDialogListener(new DialogListener() {
                                    @Override
                                    public void onPositive(Date date, float number) {
                                        getPresenter().createHeight(CreateHeightInput.builder()
                                                .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                                                .number(number)
                                                .date(TimeUtil.dateToUTCString(date, DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z))
                                                .build());
                                    }
                                }).show(fragmentManager)));

        getPresenter().getHeights(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId());
        //    getPresenter().getLocalWeights();
    }

    private void setUpChart(List<Height> heights) {
        List<Entry> entries = new ArrayList<>();
        for (Height height : heights) {
            entries.add(new Entry(height.getDiffMonth(), height.getNumber()));
        }
        LineDataSet setComp1 = new LineDataSet(entries, getString(R.string.height));
        setComp1.setAxisDependency(YAxis.AxisDependency.LEFT);

        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(setComp1);

        LineData data = new LineData(dataSets);
        mLineChart.setData(data);
        mLineChart.invalidate(); // refresh

        final String[] quarters = CommonUtil.getMonths(12);

        IAxisValueFormatter formatter = (value, axis) -> quarters[(int) value];

        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setGranularity(0.1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);
        mLineChart.invalidate();
    }

    @Override
    public void createHeightSuccess(Height height) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        User user = Prefs.getInstance().get(PrefsKey.USER, User.class);

        getBaseActivity().getSafeTransaction().registerTransition(fragmentManager -> {
            MessageDialogStarter.newInstance(getString(R.string.babay_weight), height.getAdvice())
                    .show(fragmentManager);
        });

        getPresenter().getHeights(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId());

        bkdev.linh.mybaby.models.Activity activity = new bkdev.linh.mybaby.models.Activity();
        activity.setBabyId(height.getBabyId());
        activity.setAuthorId(user.getId());
        activity.setContent(String.format(getString(R.string.height_activity_format), baby.getName(), height.getNumber()));
        activity.setType(ActivityType.HEIGHT);
        activity.setFamilyId(baby.getFamilyId());
        activity.setDate(height.getDate());
        activity.setTypeId(height.getId());
        App.getInstance().bus().send(new ActivityEvent(activity));
    }

    @Override
    public void getHeightsSuccess(List<Height> heights) {
        if (heights.size() > 0) {
            setUpChart(heights);
        }
    }
}
