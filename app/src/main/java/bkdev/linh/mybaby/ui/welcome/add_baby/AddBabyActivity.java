package bkdev.linh.mybaby.ui.welcome.add_baby;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Family;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.BabyInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.dialog.AddNewBabyDialogStarter;
import bkdev.linh.mybaby.ui.dialog.BabyAdapter;
import bkdev.linh.mybaby.ui.dialog.DialogListener;
import bkdev.linh.mybaby.ui.main.MainActivityStarter;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class AddBabyActivity extends BaseActivity<AddBabyView, AddBabyPresenter> implements HeaderBarPrimaryCustom.HeaderBarListener, AddBabyView {

    private static final int MAX_PROGRESS = 2;

    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.fabAdd)
    FloatingActionButton mFabAdd;
    @BindView(R.id.text)
    TextView mTvNotify;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.edtName)
    TextInputEditText edtName;
    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;
    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;
    @BindView(R.id.llAddBaby)
    LinearLayout llAddBaby;

    private BabyAdapter mBabyAdapter;
    private List<Baby> mBabies;
    private ProgressDialog mProgressDialog;

    private int mLoadCount;
    private int mBabyId;
    private int mFamilyId;

    @Override
    protected AddBabyPresenter createPresenter() {
        return new AddBabyPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_new_baby;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(this);
        mBabies = new ArrayList<>();

        mBabyAdapter = new BabyAdapter(this, mBabies);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mBabyAdapter);

        mBabyAdapter.setOnItemClickListener((v, position) -> {
            Prefs.getInstance().put(PrefsKey.BABY, mBabies.get(position));
            mBabyId = mBabies.get(position).getId();
            loadFirstData();

        });
        mFamilyId = Prefs.getInstance().get(PrefsKey.USER, User.class).getFamilyId();
        getPresenter().getBabies(mFamilyId);

        mFabAdd.setOnClickListener(view ->
                getSafeTransaction().registerTransition(fragmentManager -> {
                    AddNewBabyDialogStarter.newInstance(mFamilyId)
                            .setDialogListener(new DialogListener() {
                                @Override
                                public void onAddBaby(BabyInput babyInput) {
                                    getPresenter().addBaby(babyInput);
                                }
                            }).show(fragmentManager);
                }));
    }

    /**
     * click back
     */
    @Override
    public void onClickImgLeft() {
        onBackPressed();
    }

    /**
     * click done
     */
    @Override
    public void onClickTvRight() {
        MainActivityStarter.start(this);
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void getBabiesSuccess(List<Baby> babies) {
        mBabies.clear();
        mBabies.addAll(babies);
        mBabyAdapter.notifyDataSetChanged();

        if (mBabies.size() > 0) {
            mTvNotify.setText(getString(R.string.had_baby_notify_format, mBabies.size(),
                    mBabies.size() > 1 ? getString(R.string.babies) : getString(R.string.baby)));
        } else {
            mTvNotify.setText(getString(R.string.no_baby_notify));
        }
    }

    @Override
    public void getBottlesSuccess(Boolean isSaved) {
        if (isSaved) {
            mLoadCount++;
        }
        if (mLoadCount == MAX_PROGRESS) {
            mProgressDialog.dismiss();

        }
    }

    @Override
    public void getWeightsSuccess(Boolean isSaved) {
        if (isSaved) {
            mLoadCount++;
        }
        if (mLoadCount == MAX_PROGRESS) {
            mProgressDialog.dismiss();
            showToast(R.string.message_load_data_success);
            MainActivityStarter.start(this);
            ActivityCompat.finishAffinity(this);
        }
    }

    @Override
    public void getFamilySuccess(Family family) {
        Prefs.getInstance().put(PrefsKey.FAMILY, family);
        Prefs.getInstance().put(PrefsKey.IS_RECEIVE_NOTIFICATION, true);
        showToast(R.string.message_load_data_success);
        MainActivityStarter.start(this);
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void addBabySuccess(Baby baby) {
        showToast(R.string.message_add_baby_success);
        getPresenter().getBabies(mFamilyId);
    }

    private void loadFirstData() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

//        getPresenter().getBottles(mBabyId);
//        getPresenter().getWeights(mBabyId);
        int familyId = Prefs.getInstance().get(PrefsKey.USER, User.class).getFamilyId();
        getPresenter().getFamily(familyId);
    }
}
