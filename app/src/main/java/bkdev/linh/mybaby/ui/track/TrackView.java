package bkdev.linh.mybaby.ui.track;

import java.util.List;
import java.util.Map;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface TrackView extends MvpView {

    void getActivitiesSuccess(List<Activity> activities);

    void getFilterActivitiesSuccess(Map<String, List<Activity>> activityMap);

}
