package bkdev.linh.mybaby.ui.moment;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Image;
import bkdev.linh.mybaby.models.Moment;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface MomentView extends MvpView {

    void getMomentsSuccess(List<Moment> moments);
}
