package bkdev.linh.mybaby.ui.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.ActivityDetail;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.input.DeleteActivityInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.ui.dialog.ActionDialogStarter;
import bkdev.linh.mybaby.ui.dialog.DialogListener;
import bkdev.linh.mybaby.ui.dialog.MessageDialogStarter;
import bkdev.linh.mybaby.ui.diaper.DiaperActivityStarter;
import bkdev.linh.mybaby.ui.feed.FeedingActivity;
import bkdev.linh.mybaby.ui.feed.FeedingActivityStarter;
import bkdev.linh.mybaby.ui.main.RecentActivityAdapter;
import bkdev.linh.mybaby.ui.sleep.SleepActivityStarter;
import bkdev.linh.mybaby.ui.track.ActivityDetailFragment;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.utils.decor.StickyRecyclerHeadersDecoration;
import bkdev.linh.mybaby.utils.decor.StickyRecyclerHeadersTouchListener;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import bkdev.linh.mybaby.views.SwipeRefreshLayoutCustom;
import butterknife.BindView;


@MakeActivityStarter
public class ActivityActivity extends BaseActivity<ActivityView, ActivityPresenter> implements ActivityView {
    private static final int LIMIT_LOAD = 10000;
    private static final int EDIT_REQUEST = 111;

    @BindView(R.id.recyclerViewActivity)
    RecyclerView mRecyclerViewActivity;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayoutCustom mSwipeRefreshLayout;
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.tvDateFrom)
    TextView mTvDateFrom;
    @BindView(R.id.tvDateTo)
    TextView mTvDateTo;

    private Calendar mDateFrom;
    private Calendar mDateTo;
    private Baby mBaby;
    private long mSelectedHeaderId;
    private int mOffset;
    private int mLongClickPosition;


    @Override
    protected ActivityPresenter createPresenter() {
        return new ActivityPresenter();
    }

    private RecentActivityAdapter mRecentActivityAdapter;
    private List<Activity> mActivities;
    private List<Activity> mFilteredActivities;
    private StickyRecyclerHeadersDecoration mHeadersDecoration;

    @Override
    public int getContentView() {
        return R.layout.activity_acitivity;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {

        initViews();
        setActions();
        getPresenter().getActivities(mBaby.getId(), LIMIT_LOAD, mOffset, false);
    }

    private void initViews() {
        mBaby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        mTvDateFrom.setText(TimeUtil.stringUTCToLocalString(mBaby.getBirthday(), DateFormat.dd_MM_yyyy));
        mTvDateTo.setText(TimeUtil.getDateCurrent());

        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });
        mActivities = new ArrayList<>();
        mFilteredActivities = new ArrayList<>();

        mRecentActivityAdapter = new RecentActivityAdapter(this, mActivities);
        mRecyclerViewActivity.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewActivity.setAdapter(mRecentActivityAdapter);

        mHeadersDecoration = new StickyRecyclerHeadersDecoration(mRecentActivityAdapter);
        mRecyclerViewActivity.addItemDecoration(mHeadersDecoration);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mTvDateFrom.setText(TimeUtil.stringUTCToLocalString(mBaby.getBirthday(), DateFormat.dd_MM_yyyy));
            mTvDateTo.setText(TimeUtil.getDateCurrent());
            getPresenter().getActivities(mBaby.getId(), LIMIT_LOAD, mOffset, true);
        });

        StickyRecyclerHeadersTouchListener touchListener =
                new StickyRecyclerHeadersTouchListener(mRecyclerViewActivity, mHeadersDecoration);

        touchListener.setOnHeaderClickListener(
                (header, position, headerId) -> {
                    Log.d("TAGGG", "header:" + position + " " + headerId);
                    mSelectedHeaderId = headerId;
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.yyyy_MM_dd, Locale.US);
                    try {
                        cal.setTime(sdf.parse(TimeUtil.getDatetime(mSelectedHeaderId, DateFormat.yyyy_MM_dd)));// all done
                        getPresenter().getActivities(mBaby.getId(), TimeUtil.getDateTimeUTC(cal, TimeUtil.FormatType.TYPE_1));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                });
        mRecyclerViewActivity.addOnItemTouchListener(touchListener);
    }

    private void setActions() {
        mDateFrom = Calendar.getInstance();
        mDateFrom.setTimeInMillis(TimeUtil.convertStringDateTime2Milliseconds(TimeUtil.FormatType.TYPE_2, TimeUtil.stringUTCToLocalString(mBaby.getBirthday(), DateFormat.dd_MM_yyyy)));
        RxView.clicks(mTvDateFrom)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, monthOfYear, dayOfMonth) -> {
                        if (datePicker.isShown()) {
                            mDateFrom.set(year, monthOfYear, dayOfMonth);
                            mTvDateFrom.setText(TimeUtil.getTimeString(mDateFrom));
                            getPresenter().filterActivities(mTvDateFrom.getText().toString(), mTvDateTo.getText().toString(), mActivities);

                        }
                    }, mDateFrom.get(Calendar.YEAR), mDateFrom.get(Calendar.MONTH), mDateFrom.get(Calendar.DATE));
                    datePickerDialog.getDatePicker().setMinDate(mDateFrom.getTimeInMillis() - 10000);
                    datePickerDialog.show();
                });

        mDateTo = Calendar.getInstance();
        RxView.clicks(mTvDateTo)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, monthOfYear, dayOfMonth) -> {
                        if (datePicker.isShown()) {
                            mDateTo.set(year, monthOfYear, dayOfMonth);
                            mTvDateTo.setText(TimeUtil.getTimeString(mDateTo));
                            getPresenter().filterActivities(mTvDateFrom.getText().toString(), mTvDateTo.getText().toString(), mActivities);

                        }
                    }, mDateTo.get(Calendar.YEAR), mDateTo.get(Calendar.MONTH), mDateTo.get(Calendar.DATE));
                    datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                    datePickerDialog.show();
                });

        mRecentActivityAdapter.setActivityListener(position ->
                getSafeTransaction().registerTransition(fragmentManager ->
                        ActionDialogStarter.newInstance().setDialogListener(new DialogListener() {
                            @Override
                            public void onEdit() {
                                mLongClickPosition = position;
                                onClickEdit(position);
                            }

                            @Override
                            public void onDelete() {
                                mLongClickPosition = position;
                                onClickDelete(position);
                            }
                        }).show(fragmentManager)));
    }

    private void onClickDelete(int position) {
        getPresenter().deleteActivity(DeleteActivityInput.builder()
                .activityId(mActivities.get(position).getId())
                .build());
    }

    private void onClickEdit(int position) {
        Activity activity = mActivities.get(position);
        switch (activity.getType()) {
            case ActivityType.BOTTLE:
                startActivityForResult(FeedingActivityStarter.getIntent(this, FeedingActivity.TAB_BOTTLE,
                        activity.getId(), activity.getTypeId()), EDIT_REQUEST);
                break;
            case ActivityType.BREAST:
                startActivityForResult(FeedingActivityStarter.getIntent(this, FeedingActivity.TAB_BREAST,
                        activity.getId(), activity.getTypeId()), EDIT_REQUEST);
                break;
            case ActivityType.SOLID:
                startActivityForResult(FeedingActivityStarter.getIntent(this, FeedingActivity.TAB_SOLID,
                        activity.getId(), activity.getTypeId()), EDIT_REQUEST);
                break;
            case ActivityType.DIAPER:
                startActivityForResult(DiaperActivityStarter.getIntent(this,
                        activity.getId(), activity.getTypeId()), EDIT_REQUEST);
                break;
            case ActivityType.SLEEP:
                startActivityForResult(SleepActivityStarter.getIntent(this,
                        activity.getId(), activity.getTypeId()), EDIT_REQUEST);
                break;
            case ActivityType.WEIGHT:
                break;
            case ActivityType.HEIGHT:
                break;
        }
    }

    @Override
    public void getActivitiesSuccess(List<Activity> activities) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            hideRefresh();
            mActivities.clear();
        }
        mActivities.clear();
        mActivities.addAll(activities);
        mRecentActivityAdapter.setActivities(mActivities);
        mRecentActivityAdapter.notifyDataSetChanged();
    }

    @Override
    public void hideRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void filterActivitiesSuccess(List<Activity> activities) {
        mFilteredActivities.clear();
        mFilteredActivities.addAll(activities);
        mRecentActivityAdapter.setActivities(mFilteredActivities);
        mRecentActivityAdapter.notifyDataSetChanged();
    }

    @Override
    public void getActivityDetailsSuccess(List<ActivityDetail> activityDetails) {
        String title = getString(R.string.date_format, TimeUtil.getDatetime(mSelectedHeaderId, TimeUtil.FormatType.TYPE_2));
        SelectDayFragment fragment = SelectDayFragmentStarter.newInstance(title);
        fragment.setActivityDetails(activityDetails);
        fragment.setActivityListener(() ->
                getPresenter().getBottleAdvice(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId()));
        addFragment(fragment, R.id.flContainer);
    }

    public interface ActivityListener {
        void onInfo();
    }

    @Override
    public void deleteActivitySuccess(Boolean isOK) {
        if (isOK) {
            showToast(R.string.delete_success);
            mActivities.remove(mLongClickPosition);
            mRecentActivityAdapter.notifyItemRemoved(mLongClickPosition);
        } else {
            showToast(R.string.error);
        }
    }

    @Override
    public void getBottleAdviceSuccess(String advice) {
        getSafeTransaction().registerTransition(fragmentManager -> {
            MessageDialogStarter.newInstance(getString(R.string.bottle_advise), advice)
                    .show(fragmentManager);
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                showToast(R.string.update_success);
                Activity activity = data.getExtras().getParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY);
                mActivities.set(mLongClickPosition, activity);
                mRecentActivityAdapter.notifyDataSetChanged();
                mHeadersDecoration.invalidateHeaders();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragmentCurrent = getSupportFragmentManager().findFragmentById(R.id.flContainer);
        if (fragmentCurrent != null && fragmentCurrent instanceof SelectDayFragment) {
            ((SelectDayFragment) fragmentCurrent).onBackPress();
        } else if(fragmentCurrent != null && fragmentCurrent instanceof ActivityDetailFragment){
            ((ActivityDetailFragment) fragmentCurrent).onBackPress();
        }else {
            super.onBackPressed();
        }
    }
}
