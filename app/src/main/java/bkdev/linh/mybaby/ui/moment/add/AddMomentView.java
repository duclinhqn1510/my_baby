package bkdev.linh.mybaby.ui.moment.add;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Image;
import bkdev.linh.mybaby.models.Moment;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface AddMomentView extends MvpView {
    void uploadImageSuccess(Image image);

    void createMomentSuccess(Moment moment);

}
