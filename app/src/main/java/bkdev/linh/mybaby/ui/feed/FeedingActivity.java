package bkdev.linh.mybaby.ui.feed;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.ui.feed.bottle.BottleFragmentStarter;
import bkdev.linh.mybaby.ui.feed.breast.BreastFragmentStarter;
import bkdev.linh.mybaby.ui.feed.solid.SolidFragmentStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.views.HackyViewPager;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class FeedingActivity extends BaseActivity {
    public static final int TAB_BOTTLE = 0;
    public static final int TAB_BREAST = 1;
    public static final int TAB_SOLID = 2;

    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBarPrimary;
    @BindView(R.id.viewPager)
    HackyViewPager mViewPager;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;

    @Arg(optional = true)
    int mTabPosition = -1;
    @Arg(optional = true)
    int mActivityId;
    @Arg(optional = true)
    int mTypeId;

    private FeedPagerAdapter mFeedPagerAdapter;

    @Override
    public int getContentView() {
        return R.layout.activity_feeding;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBarPrimary.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });

        mFeedPagerAdapter = new FeedPagerAdapter(getSupportFragmentManager(), getResources().getStringArray(R.array.feed_tab_titles));
        mViewPager.setAdapter(mFeedPagerAdapter);
        mViewPager.setEnabled(true);
        mViewPager.setOffscreenPageLimit(mFeedPagerAdapter.getCount() - 1);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        if (mTabPosition != -1) {
            mViewPager.setCurrentItem(mTabPosition);
        }

    }

    private class FeedPagerAdapter extends FragmentStatePagerAdapter {
        private SparseArray<Fragment> mFragments = new SparseArray<>();
        private String[] mTitles;

        FeedPagerAdapter(FragmentManager fm, String[] titles) {
            super(fm);
            mTitles = titles;
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            mFragments.put(position, fragment);
            return fragment;
        }

        // Unregister when the item is inactive
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        // Returns the fragment for the position (if instantiated)
        public Fragment getFragment(int position) {
            return mFragments.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case TAB_BOTTLE:
                    if (mTabPosition == TAB_BOTTLE) {
                        fragment = BottleFragmentStarter.newInstance(getIntent().getBooleanExtra(CommonUtil.EXTRA_SERVICE_STARTED, false),
                                mTypeId, mActivityId);
                    } else {
                        fragment = BottleFragmentStarter.newInstance(getIntent().getBooleanExtra(CommonUtil.EXTRA_SERVICE_STARTED, false));
                    }
                    break;
                case TAB_BREAST:
                    if (mTabPosition == TAB_BREAST) {
                        fragment = BreastFragmentStarter.newInstance(mTypeId, mActivityId);
                    } else {
                        fragment = BreastFragmentStarter.newInstance();
                    }
                    break;
                case TAB_SOLID:
                    if (mTabPosition == TAB_SOLID) {
                        fragment = SolidFragmentStarter.newInstance(mTypeId, mActivityId);
                    } else {
                        fragment = SolidFragmentStarter.newInstance();
                    }
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }
    }
}
