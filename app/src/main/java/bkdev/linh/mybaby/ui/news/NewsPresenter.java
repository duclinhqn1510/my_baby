package bkdev.linh.mybaby.ui.news;

import java.util.List;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.News;
import bkdev.linh.mybaby.models.response.NewsResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/18/2018.
 */

public class NewsPresenter extends BasePresenter<NewsView> {
    public void getNews(boolean isRefresh) {
        if (!isRefresh) {
            getView().showLoading();
        }
        getCompositeDisposable().add(getNewsObservable()
                .subscribeWith(new CallbackWrapper<List<News>>() {
                    @Override
                    public void next(List<News> newsList) {
                        getView().getNewsListSuccess(newsList);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().hideRefresh();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<List<News>> getNewsObservable() {
        return Observable.defer(() -> ApiClient.call().getNews()
                .filter(newsResponse -> newsResponse != null && newsResponse.getNewsList() != null)
                .map(NewsResponse::getNewsList)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }
}
