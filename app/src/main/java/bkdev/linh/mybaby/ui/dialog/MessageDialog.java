package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;
import android.widget.TextView;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;
import butterknife.BindView;

;

/**
 * Created by Linh NDD
 * on 3/30/2018.
 */

@MakeActivityStarter
public class MessageDialog extends BaseDialog {

    @BindView(R.id.tvMessage)
    TextView mTvMessage;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;

    @Arg
    String mTitle;
    @Arg
    String mMessage;

    private DialogListener mDialogListener;

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTvTitle.setText(mTitle);
        mTvMessage.setText(mMessage);
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_message;
    }

}
