package bkdev.linh.mybaby.ui.grow_chart.height;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Height;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface HeightView extends MvpView {
    void createHeightSuccess(Height height);

    void getHeightsSuccess(List<Height> heights);

}
