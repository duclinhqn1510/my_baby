package bkdev.linh.mybaby.ui.welcome.family;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Family;
import bkdev.linh.mybaby.models.User;

/**
 * Created by Linh NDD
 * on 5/26/2018.
 */

public interface RegisterFamilyView extends MvpView {

    void registerFamilySuccess(Family family);

    void updateUserSuccess(User user);

    void joinFamilySuccess(User user);
}
