package bkdev.linh.mybaby.ui.welcome.family;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Family;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.JoinFamilyInput;
import bkdev.linh.mybaby.models.input.RegisterFamilyInput;
import bkdev.linh.mybaby.models.input.UpdateUserInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.welcome.add_baby.AddBabyActivityStarter;
import bkdev.linh.mybaby.ui.welcome.login.LoginActivityStarter;
import bkdev.linh.mybaby.utils.ValidationUtil;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/26/2018.
 */

@MakeActivityStarter
public class RegisterFamilyActivity extends BaseActivity<RegisterFamilyView, RegisterFamilyPresenter> implements RegisterFamilyView {
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.edtName)
    TextInputEditText mEdtName;
    @BindView(R.id.edtCode)
    TextInputEditText mEdtCode;
    @BindView(R.id.tilName)
    TextInputLayout mTilName;
    @BindView(R.id.tilCode)
    TextInputLayout mTilCode;
    @BindView(R.id.text)
    TextView mTvText;
    @BindView(R.id.rgFamily)
    RadioGroup mRgFamily;

    @Arg
    int mUserId;
    @Arg(optional = true)
    boolean mIsFromLogin;

    private boolean mIsRegister;

    @Override
    protected RegisterFamilyPresenter createPresenter() {
        return new RegisterFamilyPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_register_family;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickTvRight() {
                if (mIsRegister) {
                    register();
                } else {
                    joinFamily();
                }
            }
        });
        mTilName.setVisibility(mIsFromLogin ? View.GONE : View.VISIBLE);
        mTilCode.setVisibility(mIsFromLogin ? View.GONE : View.VISIBLE);
        mTvText.setVisibility(mIsFromLogin ? View.VISIBLE : View.GONE);
        mRgFamily.setVisibility(mIsFromLogin ? View.VISIBLE : View.GONE);

        mRgFamily.setOnCheckedChangeListener((radioGroup, id) -> {
            switch (id) {
                case R.id.rbYes:
                    mTilCode.setVisibility(View.VISIBLE);
                    mTilName.setVisibility(View.GONE);
                    mIsRegister = false;
                    break;
                case R.id.rbNo:
                    mTilCode.setVisibility(View.VISIBLE);
                    mTilName.setVisibility(View.VISIBLE);
                    mIsRegister = true;
                    break;
            }
        });
    }

    private void joinFamily() {
        if (!ValidationUtil.validatePassword(this, mEdtCode.getText().toString().trim())) {
            return;
        }
        getPresenter().joinFamily(JoinFamilyInput.builder()
                .familyCode(mEdtCode.getText().toString().trim())
                .userId(mUserId)
                .build());
    }

    private void register() {
        if (!ValidationUtil.validateName(this, mEdtName.getText().toString().trim())) {
            return;
        }
        if (!ValidationUtil.validatePassword(this, mEdtCode.getText().toString().trim())) {
            return;
        }
        getPresenter().registerFamily(RegisterFamilyInput.builder()
                .name(mEdtName.getText().toString().trim())
                .code(mEdtCode.getText().toString().trim())
                .authorId(mUserId)
                .build());
    }

    @Override
    public void registerFamilySuccess(Family family) {
        getPresenter().updateUser(mUserId, UpdateUserInput.builder()
                .familyId(family.getId())
                .build());
    }

    @Override
    public void updateUserSuccess(User user) {
        showToast(R.string.message_register_family_success);
        if (mIsFromLogin) {
            goToBabyActivity(user);
        } else {
            Log.d("TAGGG", user.getFamilyId() + "");
            LoginActivityStarter.startWithFlags(this, Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            finish();
        }

    }

    @Override
    public void joinFamilySuccess(User user) {
        goToBabyActivity(user);
    }

    private void goToBabyActivity(User user) {
        User userLocal = Prefs.getInstance().get(PrefsKey.USER, User.class);
        userLocal.setFamilyId(user.getFamilyId());
        Prefs.getInstance().put(PrefsKey.USER, userLocal);
        AddBabyActivityStarter.start(this);
        finish();
    }
}
