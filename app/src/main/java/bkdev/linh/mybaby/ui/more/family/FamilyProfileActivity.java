package bkdev.linh.mybaby.ui.more.family;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Family;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.dialog.BabyAdapter;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import bkdev.linh.mybaby.views.ProfileInfoCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/20/2018.
 */

@MakeActivityStarter
public class FamilyProfileActivity extends BaseActivity<FamilyProfileView, FamilyProfilePresenter> implements FamilyProfileView {
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.infoName)
    ProfileInfoCustom mInfoName;
    @BindView(R.id.infoCode)
    ProfileInfoCustom mInfoCode;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tvBabyText)
    TextView mTvBabyText;
    @BindView(R.id.recyclerViewMembers)
    RecyclerView mRecyclerViewMembers;
    @BindView(R.id.tvMemberText)
    TextView mTvMemberText;

    private List<Baby> mBabies;
    private List<User> mUsers;
    private BabyAdapter mBabyAdapter;
    private MemberAdapter mMemberAdapter;

    @Override
    protected FamilyProfilePresenter createPresenter() {
        return new FamilyProfilePresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_family_profile;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarPrimaryCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }
        });
        Family family = Prefs.getInstance().get(PrefsKey.FAMILY, Family.class);
        mInfoCode.setContent(family.getCode());
        mInfoName.setContent(family.getName());

        mBabies = new ArrayList<>();
        mBabyAdapter = new BabyAdapter(this, mBabies);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mBabyAdapter);

        mUsers = new ArrayList<>();
        mMemberAdapter = new MemberAdapter(this, mUsers);
        mRecyclerViewMembers.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewMembers.setAdapter(mMemberAdapter);

        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        ViewCompat.setNestedScrollingEnabled(mRecyclerViewMembers, false);

        getPresenter().getBabies(family.getId());
        getPresenter().getFamilyMember(family.getId());
    }

    @Override
    public void getBabiesSuccess(List<Baby> babies) {
        mBabies.clear();
        mBabies.addAll(babies);
        mBabyAdapter.notifyDataSetChanged();
        mTvBabyText.setText(getString(R.string.family_had_baby_notify_format, mBabies.size(),
                mBabies.size() > 1 ? getString(R.string.babies) : getString(R.string.baby)));
    }

    @Override
    public void getUsersSuccess(List<User> users) {
        mUsers.clear();
        mUsers.addAll(users);
        mMemberAdapter.notifyDataSetChanged();
        mTvMemberText.setText(getString(R.string.family_had_baby_notify_format, mUsers.size(),
                mUsers.size() > 1 ? getString(R.string.members) : getString(R.string.member)));
    }

}
