package bkdev.linh.mybaby.ui.track;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.response.GetActivitiesResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class TrackPresenter extends BasePresenter<TrackView> {
    public void getActivitiesAWeekAgo(int babyId) {
        getView().showLoading();

        getCompositeDisposable().add(getActivitiesAWeekAgoObservable(babyId)
                .subscribeWith(new CallbackWrapper<List<Activity>>() {
                    @Override
                    public void next(List<Activity> activities) {
                        getView().getActivitiesSuccess(activities);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }

    public void getFilterActivities(List<Activity> activities) {
        getView().showLoading();

        getCompositeDisposable().add(filterActivityByDayObservable(activities)
                .subscribeWith(new CallbackWrapper<Map<String, List<Activity>>>() {
                    @Override
                    public void next(Map<String, List<Activity>> activityMap) {
                        getView().getFilterActivitiesSuccess(activityMap);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                        getView().hideLoading();
                        getView().hideRefresh();
                    }
                }));
    }

    private Observable<List<Activity>> getActivitiesAWeekAgoObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getActivitiesAWeekAgo(babyId)
                .filter(getActivitiesResponse -> getActivitiesResponse != null && getActivitiesResponse.getActivities() != null)
                .map(GetActivitiesResponse::getActivities)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Map<String, List<Activity>>> filterActivityByDayObservable(List<Activity> activities) {
        Map<String, List<Activity>> activityMap = new HashMap<>();
        return Observable.fromIterable(activities)
                .doOnNext(activity -> {
                    if (!activityMap.containsKey(activity.getType())) {
                        List<Activity> activityList = new ArrayList<>();
                        activityList.add(activity);
                        activityMap.put(activity.getType(), activityList);
                    } else {
                        List<Activity> oldActivityList = activityMap.get(activity.getType());
                        oldActivityList.add(activity);
                        activityMap.put(activity.getType(), oldActivityList);
                    }
                })
                .toList()
                .toObservable()
                .map(activities1 -> activityMap)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui());
    }
}
