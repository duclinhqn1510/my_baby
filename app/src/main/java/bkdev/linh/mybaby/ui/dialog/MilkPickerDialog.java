package bkdev.linh.mybaby.ui.dialog;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseDialog;


@MakeActivityStarter
public class MilkPickerDialog extends BaseDialog {

    private DialogListener mDialogListener;

    @Override
    public void initValue(Bundle savedInstanceState) {

    }

    @Override
    public int getContentView() {
        return R.layout.dialog_milk_picker;
    }

    class MilkAdapter extends RecyclerView.Adapter<MilkAdapter.MilkHolder> {
        private List<String> mNumbers = new ArrayList<>();

        public MilkAdapter(List<String> reasons) {
            this.mNumbers = reasons;
        }

        @Override
        public MilkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_milk, parent, false);
            return new MilkHolder(v);
        }

        @Override
        public void onBindViewHolder(MilkHolder holder, int position) {
            holder.bindData(mNumbers.get(position % mNumbers.size()));
        }

        @Override
        public int getItemCount() {
            return Integer.MAX_VALUE;
        }


        public class MilkHolder extends RecyclerView.ViewHolder {
            private TextView tvNumber;

            public MilkHolder(View itemView) {
                super(itemView);
                tvNumber = itemView.findViewById(R.id.tvNumber);
            }

            public void bindData(String number) {
                tvNumber.setText(number);
            }
        }

    }
}
