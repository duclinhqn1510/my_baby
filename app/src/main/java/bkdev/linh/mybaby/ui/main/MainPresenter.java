package bkdev.linh.mybaby.ui.main;

import java.util.List;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.DeviceToken;
import bkdev.linh.mybaby.models.input.DeviceTokenInput;
import bkdev.linh.mybaby.models.response.BabiesResponse;
import bkdev.linh.mybaby.models.response.DeviceTokenResponse;
import bkdev.linh.mybaby.models.response.GetActivitiesResponse;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public class MainPresenter extends BasePresenter<MainView> {
    private static final int RECENT_ACTIVITY_LIMIT = 3;

    public void getBabies(int familyId) {
        getView().showLoading();
        getCompositeDisposable().add(getBabiesObservable(familyId)
                .subscribeWith(new CallbackWrapper<List<Baby>>() {
                    @Override
                    public void next(List<Baby> babies) {
                        getView().getBabiesSuccess(babies);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void createDeviceToken(DeviceTokenInput deviceTokenInput) {
        getCompositeDisposable().add(createDeviceTokenObservable(deviceTokenInput)
                .subscribeWith(new CallbackWrapper<DeviceToken>() {
                    @Override
                    public void next(DeviceToken deviceToken) {
                        getView().createDeviceTokenSuccess(deviceToken);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<List<Baby>> getBabiesObservable(int familyId) {
        return Observable.defer(() -> ApiClient.call().getBabies(familyId)
                .filter(babiesResponse -> babiesResponse != null && babiesResponse.getBabies() != null)
                .map(BabiesResponse::getBabies)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    public void getActivities(int babyId) {
        getCompositeDisposable().add(getActivitiesObservable(babyId)
                .subscribeWith(new CallbackWrapper<List<Activity>>() {
                    @Override
                    public void next(List<Activity> activities) {
                        getView().getActivitiesSuccess(activities);
                    }

                    @Override
                    public void complete() {
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }


    private Observable<List<Activity>> getActivitiesObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getActivities(babyId, RECENT_ACTIVITY_LIMIT, 0)
                .filter(getActivitiesResponse -> getActivitiesResponse != null && getActivitiesResponse.getActivities() != null)
                .map(GetActivitiesResponse::getActivities)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    public void getLastActivity(int babyId) {
        getCompositeDisposable().add(getLastActivityObservable(babyId)
                .subscribeWith(new CallbackWrapper<Activity>() {
                    @Override
                    public void next(Activity activity) {
                        getView().getLastActivitySuccess(activity);
                    }

                    @Override
                    public void complete() {
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }


    private Observable<Activity> getLastActivityObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getLastActivity(babyId)
                .filter(activityResponse -> activityResponse != null && activityResponse.getActivity() != null)
                .map(ActivityResponse::getActivity)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<DeviceToken> createDeviceTokenObservable(DeviceTokenInput deviceTokenInput) {
        return Observable.defer(() -> ApiClient.call().createDeviceToken(deviceTokenInput)
                .filter(deviceTokenResponse -> deviceTokenResponse != null && deviceTokenResponse.getDeviceToken() != null)
                .map(DeviceTokenResponse::getDeviceToken)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }
}
