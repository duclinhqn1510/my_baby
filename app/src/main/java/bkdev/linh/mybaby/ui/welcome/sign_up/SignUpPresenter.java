package bkdev.linh.mybaby.ui.welcome.sign_up;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.SignUpInput;
import bkdev.linh.mybaby.models.response.UserAccountResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/26/2018.
 */

public class SignUpPresenter extends BasePresenter<SignUpView> {

    public void signUp(SignUpInput signUpInput) {
        getView().showLoading();
        getCompositeDisposable().add(signUpObservable(signUpInput)
                .subscribeWith(new CallbackWrapper<User>() {
                    @Override
                    public void next(User user) {
                        getView().signUpSuccess(user);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<User> signUpObservable(SignUpInput signUpInput) {
        return Observable.defer(() -> ApiClient.call().signUp(signUpInput)
                .filter(userAccountResponse -> userAccountResponse != null && userAccountResponse.getUser() != null)
                .map(UserAccountResponse::getUser)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }
}
