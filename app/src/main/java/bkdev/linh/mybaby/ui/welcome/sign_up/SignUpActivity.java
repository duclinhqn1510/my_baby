package bkdev.linh.mybaby.ui.welcome.sign_up;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.CheckBox;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.SignUpInput;
import bkdev.linh.mybaby.ui.welcome.family.RegisterFamilyActivityStarter;
import bkdev.linh.mybaby.ui.welcome.login.LoginActivityStarter;
import bkdev.linh.mybaby.utils.ValidationUtil;
import bkdev.linh.mybaby.views.HeaderBarPrimaryCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class SignUpActivity extends BaseActivity<SignUpView, SignUpPresenter> implements HeaderBarPrimaryCustom.HeaderBarListener, SignUpView {
    @BindView(R.id.headerBar)
    HeaderBarPrimaryCustom mHeaderBar;
    @BindView(R.id.edtUsername)
    TextInputEditText mEdtUsername;
    @BindView(R.id.edtFullName)
    TextInputEditText mEdtFullName;
    @BindView(R.id.edtPassword)
    TextInputEditText mEdtPassword;
    @BindView(R.id.edtFamilyCode)
    TextInputEditText mEdtFamilyCode;
    @BindView(R.id.tilFamilyCode)
    TextInputLayout mTilFamilyCode;
    @BindView(R.id.cbFamily)
    CheckBox mCbFamily;

    @Override
    protected SignUpPresenter createPresenter() {
        return new SignUpPresenter();
    }

    @Override
    public int getContentView() {
        return R.layout.activity_sign_up;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(this);

        mCbFamily.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            mTilFamilyCode.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });
    }

    @Override
    public void onClickImgLeft() {
        onBackPressed();
    }

    @Override
    public void onClickTvRight() {
        if (!ValidationUtil.validateUsername(this, mEdtUsername.getText().toString().trim())) {
            return;
        }
        if (!ValidationUtil.validatePassword(this, mEdtPassword.getText().toString().trim())) {
            return;
        }
        if (!ValidationUtil.validateName(this, mEdtFullName.getText().toString().trim())) {
            return;
        }
        if (mTilFamilyCode.getVisibility() == View.VISIBLE && !ValidationUtil.validatePassword(this, mEdtFamilyCode.getText().toString().trim())) {
            return;
        }

        String familyCode = mCbFamily.isChecked() ? mEdtFamilyCode.getText().toString().trim() : "";

        getPresenter().signUp(SignUpInput.builder()
                .username(mEdtUsername.getText().toString().trim())
                .fullName(mEdtFullName.getText().toString().trim())
                .familyCode(familyCode)
                .password(mEdtPassword.getText().toString().trim())
                .build());
    }

    @Override
    public void signUpSuccess(User user) {
        if (user.getFamilyId() == 0) {
            RegisterFamilyActivityStarter.start(this, user.getId());

        } else {
            showToast(R.string.sign_up_success);
            LoginActivityStarter.start(this);
        }
        finish();
    }
}
