package bkdev.linh.mybaby.ui.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.models.ActivityDetail;
import bkdev.linh.mybaby.type.ActivityType;
import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Setter;
import lombok.experimental.Accessors;


public class ActivityDetailAdapter extends BaseAdapter {

    private List<ActivityDetail> mActivityDetails;

    @Setter
    @Accessors(prefix = "m")
    private ActivityDetailListener mActivityDetailListener = null;

    public ActivityDetailAdapter(@NonNull Context context, List<ActivityDetail> activityDetails) {
        super(context);
        mActivityDetails = activityDetails;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);
        ((ActivityHolder) viewHolder).bindData(getContext(), mActivityDetails.get(i));

        RxView.clicks(((ActivityHolder) viewHolder).mImgInfo)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    if (mActivityDetailListener != null && mActivityDetails.get(i).getType().equals(ActivityType.BOTTLE)) {
                        mActivityDetailListener.onClickInfo();
                    }
                });
    }

    public interface ActivityDetailListener {
        void onClickInfo();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ActivityHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_activity_count, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return mActivityDetails == null ? 0 : mActivityDetails.size();
    }

    static class ActivityHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView mImageView;
        @BindView(R.id.tvContent)
        TextView mTvContent;
        @BindView(R.id.tvNumber)
        TextView mTvNumber;
        @BindView(R.id.imgInfo)
        ImageView mImgInfo;

        ActivityHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(Context context, ActivityDetail activityDetail) {
            mTvContent.setText(context.getString(R.string.activiy_count_format, activityDetail.getCount()
                    , activityDetail.getCount() > 1 ? context.getString(R.string.times) : context.getString(R.string.time)));
            switch (activityDetail.getType()) {
                case ActivityType.BOTTLE:
                    mImageView.setImageResource(R.drawable.ic_bottle);
                    mTvNumber.setText(context.getString(R.string.total_format, activityDetail.getTotal(),
                            context.getString(R.string.ml)));
                    mImgInfo.setVisibility(View.VISIBLE);
                    break;
                case ActivityType.BREAST:
                    mImageView.setImageResource(R.drawable.ic_breastfeeding);
                    mTvNumber.setText(context.getString(R.string.total_format, activityDetail.getTotal(),
                            activityDetail.getTotal() > 1 ? context.getString(R.string.minutes) :
                                    context.getString(R.string.minute)));
                    break;
                case ActivityType.DIAPER:
                    mImageView.setImageResource(R.drawable.ic_diaper_2);
                    mTvNumber.setVisibility(View.GONE);
                    break;
                case ActivityType.SLEEP:
                    mImageView.setImageResource(R.drawable.ic_sleep_2);
                    mTvNumber.setText(context.getString(R.string.total_format, activityDetail.getTotal(),
                            activityDetail.getTotal() > 1 ? context.getString(R.string.minutes) :
                                    context.getString(R.string.minute)));
                    mTvNumber.setVisibility(View.GONE);
                    break;
                case ActivityType.SOLID:
                    mImageView.setImageResource(R.drawable.ic_solid);
                    mTvNumber.setText(context.getString(R.string.total_format, activityDetail.getTotal(),
                            activityDetail.getTotal() > 1 ? context.getString(R.string.minutes) :
                                    context.getString(R.string.minute)));
                    mTvNumber.setVisibility(View.GONE);
                    break;
                case ActivityType.WEIGHT:
                    mImageView.setImageResource(R.drawable.ic_scale);
                    mTvNumber.setVisibility(View.GONE);
                    break;
                case ActivityType.HEIGHT:
                    mImageView.setImageResource(R.drawable.ic_height);
                    mTvNumber.setVisibility(View.GONE);
                    break;
            }
        }

    }
}
