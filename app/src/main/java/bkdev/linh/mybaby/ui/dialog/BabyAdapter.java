package bkdev.linh.mybaby.ui.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.models.Baby;
import butterknife.BindView;
import butterknife.ButterKnife;


public class BabyAdapter extends BaseAdapter {

    private List<Baby> mBabies;

    public BabyAdapter(@NonNull Context context, List<Baby> babies) {
        super(context);
        mBabies = babies;
    }

    public void setbabies(List<Baby> mBabies) {
        this.mBabies = mBabies;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);

        ((ActivityHolder) viewHolder).bindData(mBabies.get(i));

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ActivityHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_baby, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return mBabies == null ? 0 : mBabies.size();
    }

    static class ActivityHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView mTvName;

        ActivityHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(Baby baby) {
            mTvName.setText(baby.getName());
        }

    }
}
