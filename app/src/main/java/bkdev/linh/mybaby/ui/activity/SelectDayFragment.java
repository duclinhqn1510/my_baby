package bkdev.linh.mybaby.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.AnimationListener;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.ActivityDetail;
import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.OnClick;
import lombok.Setter;
import lombok.experimental.Accessors;


@MakeActivityStarter
public class SelectDayFragment extends BaseFragment {

    @BindView(R.id.viewSpace)
    View mViewSpace;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.llInfo)
    LinearLayout mLlInfo;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindAnim(R.anim.slide_bottom_in)
    Animation mSlideBottomIn;
    @BindAnim(R.anim.slide_bottom_out)
    Animation mSlideBottomOut;

    @Arg
    String mTitle;

    @Setter
    @Accessors(prefix = "m")
    private List<ActivityDetail> mActivityDetails;

    @Setter
    @Accessors(prefix = "m")
    private ActivityActivity.ActivityListener mActivityListener = null;

    @Override
    public int getContentView() {
        return R.layout.fragment_select_day;
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mTvTitle.setText(mTitle);

        ActivityDetailAdapter mActivityCountAdapter = new ActivityDetailAdapter(getBaseActivity(), mActivityDetails);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        mRecyclerView.setAdapter(mActivityCountAdapter);
        mActivityCountAdapter.setActivityDetailListener(() -> {
            if (mActivityListener != null) {
                mActivityListener.onInfo();
            }
        });

        mLlInfo.startAnimation(mSlideBottomIn);

        mSlideBottomIn.setAnimationListener((AnimationListener) animation -> {
            mViewSpace.setVisibility(View.VISIBLE);
        });

        mSlideBottomOut.setAnimationListener((AnimationListener) animation -> {
            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
            }
        });
    }

    @OnClick
    void onClickViewSpace() {
        mViewSpace.setVisibility(View.GONE);
        mLlInfo.startAnimation(mSlideBottomOut);
    }


    public void onBackPress() {
        mViewSpace.setVisibility(View.GONE);
        mLlInfo.startAnimation(mSlideBottomOut);
    }

    public interface SelectDayListener {
        void onDateSelected(Calendar calendar);
    }
}
