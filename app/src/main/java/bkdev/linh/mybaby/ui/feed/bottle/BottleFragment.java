package bkdev.linh.mybaby.ui.feed.bottle;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Calendar;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseFragment;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Bottle;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.BottleInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import bkdev.linh.mybaby.views.EditTextCustom;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 4/8/2018.
 */

@MakeActivityStarter
public class BottleFragment extends BaseFragment<BottleView, BottlePresenter> implements BottleView {
    private static final int BOTTLE_VOLUME_STEP = 5;
    private static final int BOTTLE_VOLUME_MAX = 300;
    private static final int BOTTLE_VOLUME_MIN = 5;

    @BindView(R.id.edtDate)
    EditTextCustom mEdtDate;
    @BindView(R.id.edtTime)
    EditTextCustom mEdtTime;
    @BindView(R.id.numberPicker)
    NumberPicker mNumberPicker;
    @BindView(R.id.llTime)
    LinearLayout mLlTime;
    @BindView(R.id.flBottle)
    FrameLayout mFlBottle;
    @BindView(R.id.tvSave)
    TextView mTvSave;

    @Arg
    boolean mIsStarted;
    @Arg(optional = true)
    int mBottleId;
    @Arg(optional = true)
    int mActivityId;

    Calendar mDateOfFeeding;
    private int mVolume;
    private String[] mVolumeDisplays;

    @Override
    public int getContentView() {
        return R.layout.fragment_bottle;
    }

    @Override
    protected BottlePresenter createPresenter() {
        return new BottlePresenter();
    }

    @Override
    public void initValue(View view, Bundle savedInstanceState) {
        mDateOfFeeding = Calendar.getInstance();

        mEdtDate.setPickerListener(() -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getBaseActivity(), (datePicker, year, monthOfYear, dayOfMonth) -> {
                if (datePicker.isShown()) {
                    mDateOfFeeding.set(year, monthOfYear, dayOfMonth);
                    mEdtDate.setText(TimeUtil.getTimeString(mDateOfFeeding));
                }
            }, mDateOfFeeding.get(Calendar.YEAR), mDateOfFeeding.get(Calendar.MONTH), mDateOfFeeding.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() - 10000);
            datePickerDialog.show();
        });

        mEdtTime.setPickerListener(() -> {
            TimePickerDialog timePickerDialog = new TimePickerDialog(getBaseActivity(), (timePicker, hourOfDay, minute) -> {
                if (timePicker.isShown()) {
                    mDateOfFeeding.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    mDateOfFeeding.set(Calendar.MINUTE, minute);
                    mEdtTime.setText(TimeUtil.getTimeString(mDateOfFeeding, TimeUtil.FormatType.TYPE_6));
                }
            }, mDateOfFeeding.get(Calendar.HOUR_OF_DAY), mDateOfFeeding.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        });

        mVolumeDisplays = getBottleDisplayValues(BOTTLE_VOLUME_MIN, BOTTLE_VOLUME_MAX, BOTTLE_VOLUME_STEP);
        mNumberPicker.setDisplayedValues(mVolumeDisplays);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setMaxValue(mVolumeDisplays.length - 1);

        mNumberPicker.setOnValueChangedListener((picker, oldVal, newVal) -> {
            try {
                mVolume = Integer.parseInt(mVolumeDisplays[newVal]);
            } catch (Exception e) {
                getBaseActivity().showToast(R.string.error);
            }
        });
        if (mBottleId != 0 && mActivityId != 0) {
            loadToEdit();

        }

    }

    public void loadToEdit() {
        setUpView(false);
        if (mBottleId != 0 && mActivityId != 0) {
            getPresenter().getBottle(mBottleId);
        }
    }

    private void setUpView(boolean isHasData) {
        mTvSave.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
        mLlTime.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
        mFlBottle.setVisibility(isHasData ? View.VISIBLE : View.INVISIBLE);
    }


    @OnClick(R.id.tvSave)
    void onClickSave() {
        if (mBottleId != 0 && mActivityId != 0) {
            getPresenter().updateBottle(mBottleId, BottleInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .timeOfFeeding(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .volume(mVolume)
                    .build());
        } else {
            getPresenter().createBottle(BottleInput.builder()
                    .babyId(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId())
                    .timeOfFeeding(TimeUtil.getDateTimeUTC(mDateOfFeeding, TimeUtil.FormatType.TYPE_1))
                    .volume(mVolume)
                    .build());
        }
    }

    @Override
    public void createBottleSuccess(Bottle bottle) {

        getBaseActivity().showToast("Bottle" + bottle.getId() + "" + bottle.isUploaded());

        if (bottle.isUploaded()) {
            bkdev.linh.mybaby.models.Activity activity = getBabyActivity(bottle);

            moveBack(activity);
        } else {
            getBaseActivity().showToast(R.string.message_data_save_offline);
            getBaseActivity().finish();
        }
    }

    private void moveBack(bkdev.linh.mybaby.models.Activity activity) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY, activity);
        intent.putExtras(bundle);

        getBaseActivity().setResult(Activity.RESULT_OK, intent);
        getBaseActivity().finish();
    }

    private bkdev.linh.mybaby.models.Activity getBabyActivity(Bottle bottle) {
        Baby baby = Prefs.getInstance().get(PrefsKey.BABY, Baby.class);
        User user = Prefs.getInstance().get(PrefsKey.USER, User.class);
        bkdev.linh.mybaby.models.Activity activity = new bkdev.linh.mybaby.models.Activity();
        activity.setBabyId(bottle.getBabyId());
        activity.setAuthorId(user.getId());
        activity.setContent(String.format(getString(R.string.formula_activity_format), baby.getName(), bottle.getVolume()));
        activity.setType(ActivityType.BOTTLE);
        activity.setFamilyId(baby.getFamilyId());
        activity.setDate(bottle.getTimeOfFeeding());
        activity.setTypeId(bottle.getId());
        return activity;
    }

    @Override
    public void getBottleSuccess(Bottle bottle) {
        setUpView(true);
        try {
            mVolume = Integer.parseInt(mVolumeDisplays[bottle.getVolume() / BOTTLE_VOLUME_STEP - 1]);
        } catch (Exception e) {
            getBaseActivity().showToast(R.string.error);
        }
        mEdtTime.setText(TimeUtil.stringUTCToLocalString(bottle.getTimeOfFeeding(), DateFormat.HH_mm));
        mEdtDate.setText(TimeUtil.stringUTCToLocalString(bottle.getTimeOfFeeding(), DateFormat.yyyy_MM_dd));
        mDateOfFeeding.setTimeInMillis(TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd_T_HH_mm_ss_SSS_Z, bottle.getTimeOfFeeding()));
        mNumberPicker.setValue(bottle.getVolume() / BOTTLE_VOLUME_STEP - 1);
    }

    @Override
    public void updateBottleSuccess(Bottle bottle) {
        bkdev.linh.mybaby.models.Activity activity = getBabyActivity(bottle);
        ActivityInput activityInput = new ActivityInput(activity);
        getPresenter().updateActivity(mActivityId, activityInput);
    }

    @Override
    public void updateActivitySuccess(bkdev.linh.mybaby.models.Activity activity) {
        moveBack(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private String[] getBottleDisplayValues(int min, int max, int step) {
        int length = (max - min) / step;
        String[] displayValues = new String[length];
        for (int i = 0; i < length; i++) {
            displayValues[i] = String.valueOf(min + i * step);
        }
        return displayValues;
    }
}
