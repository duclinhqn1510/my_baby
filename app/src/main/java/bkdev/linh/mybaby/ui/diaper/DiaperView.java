package bkdev.linh.mybaby.ui.diaper;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Diaper;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface DiaperView extends MvpView {
    void createDiaperSuccess(Diaper diaper);

    void getDiaperSuccess(Diaper diaper);

    void updateDiaperSuccess(Diaper diaper);

    void updateActivitySuccess(Activity activity);
}
