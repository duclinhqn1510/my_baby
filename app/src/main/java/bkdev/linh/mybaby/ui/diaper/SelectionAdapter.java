package bkdev.linh.mybaby.ui.diaper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;


public class SelectionAdapter extends BaseAdapter {

    private int[] mColors;


    public SelectionAdapter(@NonNull Context context, int[] colors) {
        super(context);
        mColors = colors;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);

        ((ColorHolder) viewHolder).mImageView.setImageResource(mColors[i]);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ColorHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_selection, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mColors == null ? 0 : mColors.length;
    }

    static class ColorHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView mImageView;

        ColorHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
