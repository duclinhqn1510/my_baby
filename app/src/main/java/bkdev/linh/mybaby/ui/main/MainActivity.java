package bkdev.linh.mybaby.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.github.nkzawa.socketio.client.IO;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import activitystarter.Arg;
import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.BuildConfig;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.api.Params;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.DeviceToken;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.events.ActivityEvent;
import bkdev.linh.mybaby.models.input.DeviceTokenInput;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.ui.activity.ActivityActivityStarter;
import bkdev.linh.mybaby.ui.dialog.ChooseBabyDialog;
import bkdev.linh.mybaby.ui.dialog.ChooseBabyDialogStarter;
import bkdev.linh.mybaby.ui.dialog.DialogListener;
import bkdev.linh.mybaby.ui.moment.MomentFragmentStarter;
import bkdev.linh.mybaby.ui.more.MoreFragmentStarter;
import bkdev.linh.mybaby.ui.news.NewsFragmentStarter;
import bkdev.linh.mybaby.ui.track.TrackFragment;
import bkdev.linh.mybaby.ui.track.TrackFragmentStarter;
import bkdev.linh.mybaby.ui.welcome.SplashActivityStarter;
import bkdev.linh.mybaby.utils.CommonUtil;
import bkdev.linh.mybaby.views.CustomLinearLayoutManager;
import bkdev.linh.mybaby.views.HackyViewPager;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


@MakeActivityStarter
public class MainActivity extends BaseActivity<MainView, MainPresenter> implements OnTabSelectListener, MainView {
    public static final int FEEDING_REQUEST = 111;
    public static final int SLEEPING_REQUEST = 112;
    public static final int DIAPER_REQUEST = 113;

    private static final int TAB_NEWS = 0;
    private static final int TAB_MOMENTS = 1;
    private static final int TAB_TRACK = 2;
    private static final int TAB_MORE = 3;
    private static final int TAB_COUNT = 4;

    @BindView(R.id.viewPager)
    HackyViewPager mViewPager;
    @BindView(R.id.bottomBar)
    BottomBar mBottomBar;
    @BindView(R.id.recyclerViewActivity)
    RecyclerView mRecyclerViewActivity;
    @BindView(R.id.rlActivity)
    RelativeLayout mRlActivity;
    @BindView(R.id.imgNotify)
    ImageView mImgNotify;
    @BindView(R.id.fabActivity)
    FloatingActionButton mFabActivity;
    private int mEmitedTypeId;
    private String mEmitedType;

    @Arg(optional = true)
    Activity mAddActivity;
    private boolean mHadJoinRoom;


    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    private MainPagerAdapter mMainPagerAdapter;
    private RecentActivityAdapter mRecentActivityAdapter;
    private List<Activity> mActivities;
    private CompositeDisposable mCompositeDisposable;

    private com.github.nkzawa.socketio.client.Socket mSocket;

    {
        try {
            mSocket = IO.socket(BuildConfig.HOST_API);
        } catch (URISyntaxException e) {
            Log.d(MainActivity.class.getSimpleName(), e.getMessage());
        }
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mActivities = new ArrayList<>();
        mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mMainPagerAdapter);
        mViewPager.setOffscreenPageLimit(mMainPagerAdapter.getCount() - 1);
        mBottomBar.setOnTabSelectListener(this);

        mRecentActivityAdapter = new RecentActivityAdapter(this, mActivities);
        mRecyclerViewActivity.setLayoutManager(new CustomLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerViewActivity.setAdapter(mRecentActivityAdapter);

        mSocket.connect();
        mSocket.on(CommonUtil.ACTIVITY, args -> {
            if (args[0] instanceof Integer) {
                getPresenter().getActivities(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId());
            }
        });
        joinRoom();

        mCompositeDisposable = new CompositeDisposable();
        mCompositeDisposable.add(App.getInstance().bus()
                .toObservable()
                .subscribeOn(Schedulers.io())
                .subscribe(o -> {
                    if (o instanceof ActivityEvent) {
                        emitActivity(((ActivityEvent) o).getActivity());
                    }
                }));


        mFabActivity.setOnClickListener(view -> {
            mRlActivity.setVisibility(View.VISIBLE);
            mImgNotify.setVisibility(View.GONE);
        });

        getPresenter().getActivities(Prefs.getInstance().get(PrefsKey.BABY, Baby.class).getId());
        createDeviceToken();

        if (mAddActivity != null && mHadJoinRoom) {
            emitActivity(mAddActivity);
        }

    }

    private void createDeviceToken() {
        if (!Prefs.getInstance().get(PrefsKey.IS_REGISTER_DEVICE, Boolean.class)) {
            SharedPreferences pref = getSharedPreferences(Prefs.PREFS_FB, MODE_PRIVATE);
            String regId = pref.getString(PrefsKey.DEVICE_TOKEN, null);
            if (!TextUtils.isEmpty(regId)) {
                getPresenter().createDeviceToken(DeviceTokenInput.builder()
                        .token(regId)
                        .build());
            }
        }
    }

    public void getBabies() {
        getPresenter().getBabies(Prefs.getInstance().get(PrefsKey.USER, User.class).getFamilyId());

    }

    @OnClick(R.id.imgDropDown)
    void onDropDown() {
        if (mRlActivity.getVisibility() == View.VISIBLE) {
            mRlActivity.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.tvRecentActivity)
    void onRecentActivity() {
        ActivityActivityStarter.start(this);
    }

    public void joinRoom() {
        JSONObject obj = new JSONObject();
        try {
            obj.put(Params.FAMILY_ID, Prefs.getInstance().get(PrefsKey.USER, User.class).getFamilyId());
            mSocket.emit(CommonUtil.JOIN_ROOM, obj);
            mHadJoinRoom = true;
            Log.d(MainActivity.class.getSimpleName(), obj.toString());

        } catch (JSONException e) {
            showToast(R.string.error);
            e.printStackTrace();
        }
    }

    @Override
    public void onTabSelected(int tabId) {
        switch (tabId) {
            case R.id.tabNews:
                mViewPager.setCurrentItem(TAB_NEWS);
                mRlActivity.setVisibility(View.GONE);
                break;
            case R.id.tabMoment:
                mViewPager.setCurrentItem(TAB_MOMENTS);
                mRlActivity.setVisibility(View.GONE);
                break;
            case R.id.tabTrack:
                mViewPager.setCurrentItem(TAB_TRACK);
                mRlActivity.setVisibility(View.GONE);
                break;
            case R.id.tabMore:
                mViewPager.setCurrentItem(TAB_MORE);
                mRlActivity.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void getBabiesSuccess(List<Baby> babies) {
        getSafeTransaction().registerTransition(fragmentManager -> {
            ChooseBabyDialog babyDialog = ChooseBabyDialogStarter.newInstance("Babies")
                    .setDialogListener(new DialogListener() {
                        @Override
                        public void onChooseBaby(Baby baby) {
                            Prefs.getInstance().put(PrefsKey.BABY, baby);
                            SplashActivityStarter.startWithFlags(MainActivity.this, Intent.FLAG_ACTIVITY_NEW_TASK);
                            ActivityCompat.finishAffinity(MainActivity.this);
                        }
                    });
            babyDialog.setBabies(babies);
            babyDialog.show(fragmentManager);
        });

    }

    @Override
    public void getLastActivitySuccess(Activity activity) {
        showToast(activity.getContent());
        mActivities.add(0, activity);
        mRecentActivityAdapter.notifyDataSetChanged();
        mImgNotify.setVisibility(View.VISIBLE);
        mRlActivity.setVisibility(View.VISIBLE);
    }

    @Override
    public void getActivitiesSuccess(List<Activity> activities) {
        mActivities.clear();
        mActivities.addAll(activities);
        mRecentActivityAdapter.notifyDataSetChanged();
    }

    @Override
    public void createDeviceTokenSuccess(DeviceToken deviceToken) {
        Prefs.getInstance().put(PrefsKey.IS_REGISTER_DEVICE, true);
        Log.d(MainActivity.class.getSimpleName(), deviceToken.getToken());
    }

    private class MainPagerAdapter extends FragmentStatePagerAdapter {
        // Sparse array to keep track of registered fragments in memory
        private SparseArray<Fragment> mFragments = new SparseArray<>();

        MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            mFragments.put(position, fragment);
            return fragment;
        }

        // Unregister when the item is inactive
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        // Returns the fragment for the position (if instantiated)
        public Fragment getRegisteredFragment(int position) {
            return mFragments.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case TAB_NEWS:
                    fragment = NewsFragmentStarter.newInstance();
                    break;
                case TAB_MOMENTS:
                    fragment = MomentFragmentStarter.newInstance();
                    break;
                case TAB_TRACK:
                    fragment = TrackFragmentStarter.newInstance();
                    break;
                case TAB_MORE:
                    fragment = MoreFragmentStarter.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(MainActivity.class.getSimpleName(), requestCode + " " + resultCode);
        if (requestCode == DIAPER_REQUEST || requestCode == FEEDING_REQUEST || requestCode == SLEEPING_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.getExtras() != null) {
                    Activity activity = data.getExtras().getParcelable(CommonUtil.KEY_BUNDLE_ACTIVITY);
                    emitActivity(activity);
                }
            }
            reSetView();
        }
    }

    private void reSetView() {
        TrackFragment trackFragment = (TrackFragment) mMainPagerAdapter.getRegisteredFragment(TAB_TRACK);
        if (trackFragment != null) {
            trackFragment.setUpViews();
        }
    }

    private void emitActivity(Activity activity) {

        if (activity != null && !(activity.getType().equals(mEmitedType) && mEmitedTypeId == activity.getTypeId())) {
            JSONObject obj = new JSONObject();
            try {
                obj.put(CommonUtil.BABY_ID, activity.getBabyId());
                obj.put(CommonUtil.FAMILY_ID, activity.getFamilyId());
                obj.put(CommonUtil.AUTHOR_ID, activity.getAuthorId());
                obj.put(CommonUtil.CONTENT, activity.getContent());
                obj.put(CommonUtil.TYPE, activity.getType());
                obj.put(CommonUtil.DATE, activity.getDate());
                obj.put(CommonUtil.TYPE_ID, activity.getTypeId());

                mSocket.emit("send", obj);
                Log.d("TAGGGG", obj.toString());

            } catch (JSONException e) {
                Log.d(MainActivity.class.getSimpleName(), "ERROR");
                e.printStackTrace();
            }
            mEmitedTypeId = activity.getTypeId();
            mEmitedType = activity.getType();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mHadJoinRoom = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
