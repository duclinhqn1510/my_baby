package bkdev.linh.mybaby.ui.sleep;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Sleep;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public interface SleepView extends MvpView {
    void createSleepSuccess(Sleep sleep);

    void getSleepSuccess(Sleep sleep);

    void updateSleepSuccess(Sleep sleep);

    void updateActivitySuccess(Activity activity);
}
