package bkdev.linh.mybaby.ui.moment.detail;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.BuildConfig;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.utils.AndroidLifecycleUtils;
import bkdev.linh.mybaby.utils.GlideUtil;

public class MomentPagerAdapter extends PagerAdapter {

    private List<Moment> mMoments;

    public MomentPagerAdapter(List<Moment> moments) {
        mMoments = moments;
    }

    @Override
    public int getCount() {
        return null != mMoments ? mMoments.size() : 0;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        final Context context = container.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_image_slide, container, false);
        ImageView imageView = view.findViewById(R.id.imgSlideShow);
        ProgressBar progressBar = view.findViewById(R.id.progressBar);
        TextView tvCaption = view.findViewById(R.id.tvCaption);

        boolean canLoadImage = AndroidLifecycleUtils.canLoadImage(context);

        if (canLoadImage) {
            String url = BuildConfig.HOST_API + mMoments.get(position).getImageUrl();
            GlideUtil.load(context, url, imageView, progressBar);
        }

        imageView.setOnClickListener(v -> {
            if (context instanceof Activity) {
                if (!((Activity) context).isFinishing()) {
                    ((Activity) context).onBackPressed();
                }
            }
        });
        tvCaption.setText(mMoments.get(position).getCaption());

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}