package bkdev.linh.mybaby.ui.moment.add;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Image;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.models.input.CreateMomentInput;
import bkdev.linh.mybaby.models.response.CreateMomentResponse;
import bkdev.linh.mybaby.models.response.UploadImageResponse;
import io.reactivex.Observable;
import okhttp3.MultipartBody;

/**
 * Created by Linh NDD
 * on 5/18/2018.
 */

public class AddMomentPresenter extends BasePresenter<AddMomentView> {
    public void uploadImage(MultipartBody.Part photo) {
        getView().showLoading();
        getCompositeDisposable().add(uploadImageObservable(photo)
                .subscribeWith(new CallbackWrapper<Image>() {
                    @Override
                    public void next(Image image) {
                        getView().uploadImageSuccess(image);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void createMoment(CreateMomentInput createMomentInput) {
        getView().showLoading();
        getCompositeDisposable().add(createMomentObservable(createMomentInput)
                .subscribeWith(new CallbackWrapper<Moment>() {
                    @Override
                    public void next(Moment moment) {
                        getView().createMomentSuccess(moment);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Image> uploadImageObservable(MultipartBody.Part photo) {
        return Observable.defer(() -> ApiClient.call().uploadImage(photo)
                .filter(uploadImageResponse -> uploadImageResponse != null && uploadImageResponse.getImage() != null)
                .map(UploadImageResponse::getImage)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Moment> createMomentObservable(CreateMomentInput createMomentInput) {
        return Observable.defer(() -> ApiClient.call().createMoment(createMomentInput)
                .filter(createMomentResponse -> createMomentResponse != null && createMomentResponse.getMoment() != null)
                .map(CreateMomentResponse::getMoment)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }
}
