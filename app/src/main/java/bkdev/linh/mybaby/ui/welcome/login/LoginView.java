package bkdev.linh.mybaby.ui.welcome.login;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.User;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public interface LoginView extends MvpView{
    void loginSuccess(User user);
}
