package bkdev.linh.mybaby.ui.welcome.tutorial;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import bkdev.linh.mybaby.R;

/**
 * Created by Linh NDD
 * on 4/17/2018.
 */

public class TutorialPagerAdapter extends PagerAdapter {
    private final LayoutInflater mLayoutInflater;
    private final int[] mImages;
    private final Context mContext;

    TutorialPagerAdapter(Context context, int[] images) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImages = images;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mImages.length;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = mLayoutInflater.inflate(R.layout.item_slide_tutorial, container, false);
        ImageView imgContent = view.findViewById(R.id.imgContent);

        Glide.with(mContext)
                .load(mImages[position])
                .into(imgContent);

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
