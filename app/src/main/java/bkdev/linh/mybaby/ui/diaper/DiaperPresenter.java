package bkdev.linh.mybaby.ui.diaper;

import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.databases.AppDatabase;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Diaper;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.DiaperInput;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import bkdev.linh.mybaby.models.response.DiaperResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class DiaperPresenter extends BasePresenter<DiaperView> {

    public void createDiaper(DiaperInput diaperInput) {
        getView().showLoading();
        getCompositeDisposable().add(createDiaperObservable(diaperInput)
                .subscribeWith(new CallbackWrapper<Diaper>() {
                    @Override
                    public void next(Diaper diaper) {
                        getView().createDiaperSuccess(diaper);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void updateDiaper(int id, DiaperInput diaperInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateDiaperObservable(id, diaperInput)
                .subscribeWith(new CallbackWrapper<Diaper>() {
                    @Override
                    public void next(Diaper diaper) {
                        getView().updateDiaperSuccess(diaper);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void updateActivity(int id, ActivityInput activityInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateActivityObservable(id, activityInput)
                .subscribeWith(new CallbackWrapper<Activity>() {
                    @Override
                    public void next(Activity activity) {
                        getView().updateActivitySuccess(activity);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void getDiaper(int id) {
        getView().showLoading();
        getCompositeDisposable().add(getDiaperObservable(id)
                .subscribeWith(new CallbackWrapper<Diaper>() {
                    @Override
                    public void next(Diaper diaper) {
                        getView().getDiaperSuccess(diaper);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Diaper> updateDiaperObservable(int id, DiaperInput diaperInput) {
        return Observable.defer(() -> ApiClient.call().updateDiaper(id, diaperInput)
                .filter(diaperResponse -> diaperResponse != null && diaperResponse.getDiaper() != null)
                .map(DiaperResponse::getDiaper)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Diaper> getDiaperObservable(int id) {
        return Observable.defer(() -> ApiClient.call().getDiaper(id)
                .filter(diaperResponse -> diaperResponse != null && diaperResponse.getDiaper() != null)
                .map(DiaperResponse::getDiaper)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Activity> updateActivityObservable(int id, ActivityInput activityInput) {
        return Observable.defer(() -> ApiClient.call().updateActivity(id, activityInput)
                .filter(activityResponse -> activityResponse != null && activityResponse.getActivity() != null)
                .map(ActivityResponse::getActivity)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Diaper> createDiaperObservable(DiaperInput diaperInput) {
        return Observable.defer(() -> ApiClient.call().createDiaper(diaperInput)
                .filter(diaperResponse -> diaperResponse != null && diaperResponse.getDiaper() != null)
                .map(DiaperResponse::getDiaper)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }
}
