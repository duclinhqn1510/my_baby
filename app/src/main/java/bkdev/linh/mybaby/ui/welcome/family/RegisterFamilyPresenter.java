package bkdev.linh.mybaby.ui.welcome.family;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Family;
import bkdev.linh.mybaby.models.User;
import bkdev.linh.mybaby.models.input.JoinFamilyInput;
import bkdev.linh.mybaby.models.input.RegisterFamilyInput;
import bkdev.linh.mybaby.models.input.UpdateUserInput;
import bkdev.linh.mybaby.models.response.FamilyResponse;
import bkdev.linh.mybaby.models.response.UserAccountResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/26/2018.
 */

public class RegisterFamilyPresenter extends BasePresenter<RegisterFamilyView> {

    public void registerFamily(RegisterFamilyInput registerFamilyInput) {
        getView().showLoading();
        getCompositeDisposable().add(registerFamilyObservable(registerFamilyInput)
                .subscribeWith(new CallbackWrapper<Family>() {
                    @Override
                    public void next(Family family) {
                        getView().registerFamilySuccess(family);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Family> registerFamilyObservable(RegisterFamilyInput registerFamilyInput) {
        return Observable.defer(() -> ApiClient.call().registerFamily(registerFamilyInput)
                .filter(familyResponse -> familyResponse != null && familyResponse.getFamily() != null)
                .map(FamilyResponse::getFamily)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    public void updateUser(int id, UpdateUserInput updateUserInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateUserObservable(id, updateUserInput)
                .subscribeWith(new CallbackWrapper<User>() {
                    @Override
                    public void next(User user) {
                        getView().updateUserSuccess(user);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void joinFamily(JoinFamilyInput joinFamilyInput) {
        getView().showLoading();
        getCompositeDisposable().add(joinFamilyObservable(joinFamilyInput)
                .subscribeWith(new CallbackWrapper<User>() {
                    @Override
                    public void next(User user) {
                        getView().joinFamilySuccess(user);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<User> updateUserObservable(int id, UpdateUserInput updateUserInput) {
        return Observable.defer(() -> ApiClient.call().updateUser(id, updateUserInput)
                .filter(userAccountResponse -> userAccountResponse != null && userAccountResponse.getUser() != null)
                .map(UserAccountResponse::getUser)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<User> joinFamilyObservable(JoinFamilyInput joinFamilyInput) {
        return Observable.defer(() -> ApiClient.call().joinFamily(joinFamilyInput)
                .filter(userAccountResponse -> userAccountResponse != null && userAccountResponse.getUser() != null)
                .map(UserAccountResponse::getUser)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }
}
