package bkdev.linh.mybaby.ui.welcome.sign_up;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.User;

/**
 * Created by Linh NDD
 * on 5/26/2018.
 */

public interface SignUpView extends MvpView{

    void signUpSuccess(User user);
}
