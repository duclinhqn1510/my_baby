package bkdev.linh.mybaby.ui.main;

import java.util.List;

import bkdev.linh.mybaby.base.MvpView;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.DeviceToken;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public interface MainView extends MvpView {
    void getBabiesSuccess(List<Baby> babies);

    void getLastActivitySuccess(Activity activity);

    void getActivitiesSuccess(List<Activity> activities);

    void createDeviceTokenSuccess(DeviceToken deviceToken);
}
