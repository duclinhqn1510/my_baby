package bkdev.linh.mybaby.ui.moment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import bkdev.linh.mybaby.BuildConfig;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.utils.GlideUtil;
import butterknife.BindView;
import butterknife.ButterKnife;


public class MomentAdapter extends BaseAdapter {
    public static final int CREATE_ITEM_TYPE = 0;
    public static final int NORMAL_ITEM_TYPE = 1;

    private List<Moment> mMoments;


    public MomentAdapter(@NonNull Context context, List<Moment> items) {
        super(context);
        mMoments = items;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);
        if (getItemViewType(i) == NORMAL_ITEM_TYPE) {
            ((NormalHolder) viewHolder).bindData(getContext(), mMoments.get(i));
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CREATE_ITEM_TYPE) {
            return new CreateHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_moment_create, parent, false));
        } else {
            return new NormalHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_moment_normal, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1)
            return CREATE_ITEM_TYPE;
        return NORMAL_ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return mMoments == null ? 1 : mMoments.size() + 1;
    }

    static class NormalHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBar)
        ProgressBar mProgressBar;
        @BindView(R.id.imageView)
        ImageView mImageView;
        @BindView(R.id.tvCaption)
        TextView mTvCaption;

        NormalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(Context context, Moment moment) {
            String url = BuildConfig.HOST_API + moment.getImageUrl();
            GlideUtil.loadBlur(context, url, mImageView, mProgressBar);
            mTvCaption.setText(moment.getCaption());
        }
    }

    static class CreateHolder extends RecyclerView.ViewHolder {
        CreateHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
