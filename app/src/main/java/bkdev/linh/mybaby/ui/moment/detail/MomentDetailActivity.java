package bkdev.linh.mybaby.ui.moment.detail;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

import activitystarter.Arg;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.type.AnimationType;
import bkdev.linh.mybaby.utils.CubeOutTransformer;
import bkdev.linh.mybaby.views.HeaderBarCustom;
import butterknife.BindView;

/**
 * Created by Linh NDD
 * on 5/19/2018.
 */

public class MomentDetailActivity extends BaseActivity {
    @BindView(R.id.headerBar)
    HeaderBarCustom mHeaderBar;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    @Arg
    ArrayList<Moment> mMoments;
    @Arg
    int mPosition;

    @Override
    public int getContentView() {
        return R.layout.activity_moment_detail;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHeaderBar.setHeaderBarListener(new HeaderBarCustom.HeaderBarListener() {
            @Override
            public void onClickImgLeft() {
                onBackPressed();
            }

        });
        MomentPagerAdapter momentPagerAdapter = new MomentPagerAdapter(mMoments);
        mViewPager.setAdapter(momentPagerAdapter);
        mViewPager.setCurrentItem(mPosition);

        try {
            mViewPager.setPageTransformer(true, CubeOutTransformer.class.newInstance());
        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityTransition(AnimationType.FADE);
    }
}
