package bkdev.linh.mybaby.ui.grow_chart.weight;

import java.util.List;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.models.Weight;
import bkdev.linh.mybaby.models.input.CreateWeightInput;
import bkdev.linh.mybaby.models.response.CreateWeightResponse;
import bkdev.linh.mybaby.models.response.GetWeightsResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class WeightPresenter extends BasePresenter<WeightView> {
    public void createWeight(CreateWeightInput createWeightInput) {
        getView().showLoading();
        getCompositeDisposable().add(createWeightObservable(createWeightInput)
                .subscribeWith(new CallbackWrapper<Weight>() {
                    @Override
                    public void next(Weight weight) {
                        getView().createWeightSuccess(weight);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void getWeights(int babyId) {
        getView().showLoading();
        getCompositeDisposable().add(getWeightsObservable(babyId)
                .subscribeWith(new CallbackWrapper<List<Weight>>() {
                    @Override
                    public void next(List<Weight> weights) {
                        getView().getWeightsSuccess(weights);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Weight> createWeightObservable(CreateWeightInput createWeightInput) {
        return Observable.defer(() -> ApiClient.call().createWeight(createWeightInput)
                .filter(createWeightResponse -> createWeightResponse != null && createWeightResponse.getWeight() != null)
                .map(CreateWeightResponse::getWeight)
                /*.map(weight -> {
                    mAppDatabase.weightDao().insert(weight);
                    return weight;
                })*/
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<List<Weight>> getWeightsObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getWeights(babyId)
                .filter(getWeightsResponse -> getWeightsResponse != null && getWeightsResponse.getWeights() != null)
                .map(GetWeightsResponse::getWeights)
                /*.map(weights -> {
                            for (Weight weight : weights) {
                                mAppDatabase.weightDao().insert(weight);
                            }

                            return mAppDatabase.weightDao().loadAllWeight();
                        }
                )*/
                .filter(weights -> weights != null)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

}
