package bkdev.linh.mybaby.ui.sleep;

import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.databases.AppDatabase;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Sleep;
import bkdev.linh.mybaby.models.input.ActivityInput;
import bkdev.linh.mybaby.models.input.SleepInput;
import bkdev.linh.mybaby.models.response.ActivityResponse;
import bkdev.linh.mybaby.models.response.SleepResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

public class SleepPresenter extends BasePresenter<SleepView> {

    public void createSleep(SleepInput sleepInput) {
        getView().showLoading();
        getCompositeDisposable().add(createSleepObservable(sleepInput)
                .subscribeWith(new CallbackWrapper<Sleep>() {
                    @Override
                    public void next(Sleep sleep) {
                        getView().createSleepSuccess(sleep);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void updateSleep(int id, SleepInput sleepInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateDiaperObservable(id, sleepInput)
                .subscribeWith(new CallbackWrapper<Sleep>() {
                    @Override
                    public void next(Sleep sleep) {
                        getView().updateSleepSuccess(sleep);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void updateActivity(int id, ActivityInput activityInput) {
        getView().showLoading();
        getCompositeDisposable().add(updateActivityObservable(id, activityInput)
                .subscribeWith(new CallbackWrapper<Activity>() {
                    @Override
                    public void next(Activity activity) {
                        getView().updateActivitySuccess(activity);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);

                    }
                }));
    }

    public void getSleep(int id) {
        getView().showLoading();
        getCompositeDisposable().add(getSleepObservable(id)
                .subscribeWith(new CallbackWrapper<Sleep>() {
                    @Override
                    public void next(Sleep sleep) {
                        getView().getSleepSuccess(sleep);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Sleep> updateDiaperObservable(int id, SleepInput sleepInput) {
        return Observable.defer(() -> ApiClient.call().updateSleep(id, sleepInput)
                .filter(sleepResponse -> sleepResponse != null && sleepResponse.getSleep() != null)
                .map(SleepResponse::getSleep)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Sleep> getSleepObservable(int id) {
        return Observable.defer(() -> ApiClient.call().getSleep(id)
                .filter(sleepResponse -> sleepResponse != null && sleepResponse.getSleep() != null)
                .map(SleepResponse::getSleep)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    private Observable<Activity> updateActivityObservable(int id, ActivityInput activityInput) {
        return Observable.defer(() -> ApiClient.call().updateActivity(id, activityInput)
                .filter(activityResponse -> activityResponse != null && activityResponse.getActivity() != null)
                .map(ActivityResponse::getActivity)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Sleep> createSleepObservable(SleepInput sleepInput) {
        return Observable.defer(() -> ApiClient.call().createSleep(sleepInput)
                .filter(sleepResponse -> sleepResponse != null && sleepResponse.getSleep() != null)
                .map(SleepResponse::getSleep)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }
}
