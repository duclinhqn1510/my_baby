package bkdev.linh.mybaby.ui.welcome.tutorial;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.ui.welcome.login.LoginActivityStarter;
import bkdev.linh.mybaby.ui.welcome.sign_up.SignUpActivityStarter;
import bkdev.linh.mybaby.views.indicator.CirclePageIndicator;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Linh NDD
 * on 4/17/2018.
 */

@MakeActivityStarter
public class TutorialActivity extends BaseActivity{
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tvLogin)
    TextView mTvLogin;
    @BindView(R.id.tvSignUp)
    TextView mTvRegister;
    @BindView(R.id.indicator)
    CirclePageIndicator mCirclePageIndicator;

    private TutorialPagerAdapter mTutorialPagerAdapter;
    private int[] mTutorialImages = new int[]{R.drawable.imag_tutorial, R.drawable.imag_tutorial, R.drawable.imag_tutorial};

    @Override
    public int getContentView() {
        return R.layout.activity_tutorial;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mTutorialPagerAdapter = new TutorialPagerAdapter(this, mTutorialImages);
        mViewPager.setAdapter(mTutorialPagerAdapter);
        mCirclePageIndicator.setViewPager(mViewPager);
    }

    @OnClick(R.id.tvLogin)
    void onClickLogin() {
        LoginActivityStarter.start(this);
    }

    @OnClick(R.id.tvSignUp)
    void onClickRegister() {
        SignUpActivityStarter.start(this);
    }
}
