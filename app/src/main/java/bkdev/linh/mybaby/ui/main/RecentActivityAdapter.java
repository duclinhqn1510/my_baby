package bkdev.linh.mybaby.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseAdapter;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.utils.DateFormat;
import bkdev.linh.mybaby.utils.TimeUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Setter;
import lombok.experimental.Accessors;


public class RecentActivityAdapter extends BaseAdapter implements StickyRecyclerHeadersAdapter {

    private List<Activity> mActivities;

    @Setter
    @Accessors(prefix = "m")
    private ActivityListener mActivityListener = null;


    public RecentActivityAdapter(@NonNull Context context, List<Activity> activities) {
        super(context);
        mActivities = activities;
    }

    public void setActivities(List<Activity> mActivities) {
        this.mActivities = mActivities;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        super.onBindViewHolder(viewHolder, i);

        ((ActivityHolder) viewHolder).bindData(mActivities.get(i));

        RxView.longClicks(((ActivityHolder) viewHolder).itemView)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(o -> {
                    if (mActivityListener != null) {
                        mActivityListener.onAction(i);
                    }
                });
    }

    public interface ActivityListener {
        void onAction(int position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ActivityHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_activity, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getHeaderId(int position) {
        return TimeUtil.convertStringUTCDateTime2Milliseconds(DateFormat.yyyy_MM_dd, TimeUtil.stringUTCToLocalString(mActivities.get(position).getDate(), DateFormat.yyyy_MM_dd));
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new HeaderHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_activity_header, parent, false));

    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((HeaderHolder) holder).onBind(mActivities.get(position));
    }

    @Override
    public int getItemCount() {
        return mActivities == null ? 0 : mActivities.size();
    }

    class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView mTvTitle;

        HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void onBind(Activity activity) {
            mTvTitle.setText(getString(R.string.date_format, TimeUtil.stringUTCToLocalString(activity.getDate(), DateFormat.dd_MM_yyyy)));
        }
    }

    static class ActivityHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        ImageView mImageView;
        @BindView(R.id.tvContent)
        TextView mTvContent;
        @BindView(R.id.tvDateTime)
        TextView mTvDateTime;

        ActivityHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindData(Activity activity) {
            mTvContent.setText(activity.getContent());
            mTvDateTime.setText(TimeUtil.stringUTCToLocalString(activity.getDate(), DateFormat.yyyy_MM_dd_HH_mm_ss));
            switch (activity.getType()) {
                case ActivityType.BREAST:
                    mImageView.setImageResource(R.drawable.ic_breastfeeding);
                    break;
                case ActivityType.BOTTLE:
                    mImageView.setImageResource(R.drawable.ic_bottle);
                    break;
                case ActivityType.DIAPER:
                    mImageView.setImageResource(R.drawable.ic_diaper_2);
                    break;
                case ActivityType.SLEEP:
                    mImageView.setImageResource(R.drawable.ic_sleep_2);
                    break;
                case ActivityType.SOLID:
                    mImageView.setImageResource(R.drawable.ic_solid);
                    break;
                case ActivityType.WEIGHT:
                    mImageView.setImageResource(R.drawable.ic_scale);
                    break;
                case ActivityType.HEIGHT:
                    mImageView.setImageResource(R.drawable.ic_height);
                    break;
            }
        }

    }
}
