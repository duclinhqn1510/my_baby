package bkdev.linh.mybaby.ui.welcome.add_baby;

import java.util.List;

import bkdev.linh.mybaby.App;
import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.CallbackWrapper;
import bkdev.linh.mybaby.base.BasePresenter;
import bkdev.linh.mybaby.databases.AppDatabase;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.models.Bottle;
import bkdev.linh.mybaby.models.Family;
import bkdev.linh.mybaby.models.Weight;
import bkdev.linh.mybaby.models.input.BabyInput;
import bkdev.linh.mybaby.models.response.BabiesResponse;
import bkdev.linh.mybaby.models.response.BabyResponse;
import bkdev.linh.mybaby.models.response.FamilyResponse;
import bkdev.linh.mybaby.models.response.GetBottlesResponse;
import bkdev.linh.mybaby.models.response.GetWeightsResponse;
import io.reactivex.Observable;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

public class AddBabyPresenter extends BasePresenter<AddBabyView> {
    private AppDatabase mAppDatabase = AppDatabase.getDatabase(App.getInstance());

    public void getBabies(int familyId) {
        getView().showLoading();
        getCompositeDisposable().add(getBabiesObservable(familyId)
                .subscribeWith(new CallbackWrapper<List<Baby>>() {
                    @Override
                    public void next(List<Baby> babies) {
                        getView().getBabiesSuccess(babies);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void addBaby(BabyInput babyInput) {
        getView().showLoading();
        getCompositeDisposable().add(addBabyObservable(babyInput)
                .subscribeWith(new CallbackWrapper<Baby>() {
                    @Override
                    public void next(Baby baby) {
                        getView().addBabySuccess(baby);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    public void getFamily(int id) {
        getView().showLoading();
        getCompositeDisposable().add(getFamilyObservable(id)
                .subscribeWith(new CallbackWrapper<Family>() {
                    @Override
                    public void next(Family family) {
                        getView().getFamilySuccess(family);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().hideLoading();
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<List<Baby>> getBabiesObservable(int familyId) {
        return Observable.defer(() -> ApiClient.call().getBabies(familyId)
                .filter(babiesResponse -> babiesResponse != null && babiesResponse.getBabies() != null)
                .map(BabiesResponse::getBabies)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Baby> addBabyObservable(BabyInput babyInput) {
        return Observable.defer(() -> ApiClient.call().addBaby(babyInput)
                .filter(babyResponse -> babyResponse != null && babyResponse.getBaby() != null)
                .map(BabyResponse::getBaby)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    private Observable<Family> getFamilyObservable(int id) {
        return Observable.defer(() -> ApiClient.call().getFamily(id)
                .filter(familyResponse -> familyResponse != null && familyResponse.getFamily() != null)
                .map(FamilyResponse::getFamily)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui()
                )
        );
    }

    public void getBottles(int babyId) {
        getCompositeDisposable().add(getBottlesObservable(babyId)
                .subscribeWith(new CallbackWrapper<Boolean>() {
                    @Override
                    public void next(Boolean isSaved) {
                        getView().getBottlesSuccess(isSaved);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Boolean> getBottlesObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getBottles(babyId)
                .filter(getBottlesResponse -> getBottlesResponse != null && getBottlesResponse.getBottles() != null)
                .map(GetBottlesResponse::getBottles)
                .map(bottles -> {
                            for (Bottle bottle : bottles) {
                                bottle.setUploaded(true);
                                mAppDatabase.bottleDao().insert(bottle);
                            }

                            return mAppDatabase.bottleDao().loadAllBottle();
                        }
                )
                .map(bottles -> bottles != null)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }

    public void getWeights(int babyId) {
        getCompositeDisposable().add(getWeightsObservable(babyId)
                .subscribeWith(new CallbackWrapper<Boolean>() {
                    @Override
                    public void next(Boolean isSaved) {
                        getView().getWeightsSuccess(isSaved);
                    }

                    @Override
                    public void complete() {
                        getView().hideLoading();
                    }

                    @Override
                    public void error(int code, String message) {
                        getView().onError(code, message);
                    }
                }));
    }

    private Observable<Boolean> getWeightsObservable(int babyId) {
        return Observable.defer(() -> ApiClient.call().getWeights(babyId)
                .filter(getBottlesResponse -> getBottlesResponse != null && getBottlesResponse.getWeights() != null)
                .map(GetWeightsResponse::getWeights)
                .map(weights -> {
                            for (Weight weight : weights) {
                                weight.setUploaded(true);
                                mAppDatabase.weightDao().insert(weight);
                            }

                            return mAppDatabase.weightDao().loadAllWeight();
                        }
                )
                .map(weights -> weights != null)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
        );
    }
}
