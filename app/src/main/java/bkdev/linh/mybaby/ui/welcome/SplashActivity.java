package bkdev.linh.mybaby.ui.welcome;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import activitystarter.MakeActivityStarter;
import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.base.BaseActivity;
import bkdev.linh.mybaby.models.Baby;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;
import bkdev.linh.mybaby.type.AnimationType;
import bkdev.linh.mybaby.ui.main.MainActivityStarter;
import bkdev.linh.mybaby.ui.welcome.add_baby.AddBabyActivityStarter;
import bkdev.linh.mybaby.ui.welcome.tutorial.TutorialActivityStarter;

/**
 * Created by Linh NDD
 * on 4/16/2018.
 */

@MakeActivityStarter
public class SplashActivity extends BaseActivity {
    private static final int TIME_DELAYED = 1300;
    private WeakReference<Context> mWeakReference;
    private Handler mHandler;
    private Runnable mActivityStarter = () -> {
        if (!Prefs.getInstance().get(PrefsKey.IS_LOGIN, Boolean.class)) {
            TutorialActivityStarter.start(this);
        } else {
            if (Prefs.getInstance().get(PrefsKey.BABY, Baby.class) == null) {
                AddBabyActivityStarter.start(this);
            } else {
                MainActivityStarter.start(mWeakReference.get());
            }
        }
        ActivityCompat.finishAffinity(this);
        activityTransition(AnimationType.FADE);
    };

    @Override
    public int getContentView() {
        return R.layout.activity_splash;
    }

    @Override
    public void initValue(Bundle savedInstanceState) {
        mHandler = new Handler();
        mWeakReference = new WeakReference<>(this);
        mHandler.postDelayed(mActivityStarter, TIME_DELAYED);
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacks(mActivityStarter);
        mHandler = null;
        super.onDestroy();
    }
}
