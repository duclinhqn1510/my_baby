package bkdev.linh.mybaby.service.firebase;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Map;

import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private Gson mGson;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (!Prefs.getInstance().get(PrefsKey.IS_REGISTER_DEVICE, Boolean.class)) {
            return;
        }

        if (remoteMessage == null) {
            return;

        }

        if (mGson == null) {
            mGson = new Gson();
        }
        Log.d("TAGG", "notification:" + remoteMessage.toString());

        if (remoteMessage.getNotification() != null) {
            handleDataMessage(remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody(),
                    remoteMessage.getData());
        }
    }

    private void handleDataMessage(String title, String message, Map<String, String> data) {
        if (!Prefs.getInstance().get(PrefsKey.IS_RECEIVE_NOTIFICATION, Boolean.class)) {
            return;
        }

        int notificationId = 0;
        Intent intent = new Intent("bkdev.linh.mybaby.ActivityNotification");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        MyFirebaseNotificationUtil.showNotificationMessage(getApplicationContext(),
                notificationId, title, message, "", intent);

    }
}
