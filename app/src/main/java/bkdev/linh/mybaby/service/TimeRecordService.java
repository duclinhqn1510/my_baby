package bkdev.linh.mybaby.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import bkdev.linh.mybaby.R;
import bkdev.linh.mybaby.type.ActivityType;
import bkdev.linh.mybaby.type.BreastSideType;
import bkdev.linh.mybaby.ui.feed.FeedingActivity;
import bkdev.linh.mybaby.ui.sleep.SleepActivity;
import bkdev.linh.mybaby.utils.CommonUtil;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Linh NDD
 * on 5/7/2018.
 */

public class TimeRecordService extends Service {
    private CompositeDisposable mCompositeDisposable;
    private Intent mIntent;

    public static final String GROUP_KEY = "group_mybaby";
    public static final String CHANNEL_ID = "com.bkdev.mybaby";
    public static final String CHANNEL_NAME = "MY BABY";
    private String mActivityType;
    private int mBreastSide;
    private long mLeftTime;
    private long mRightTime;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mActivityType = intent.getStringExtra(CommonUtil.EXTRA_ACTIVITY_TYPE);
        showNotification();
        mIntent = new Intent(mActivityType);
        mCompositeDisposable = new CompositeDisposable();
        mCompositeDisposable.add(Observable.interval(1, TimeUnit.SECONDS)
                .subscribe(aLong -> {
                    switch (mActivityType) {
                        case ActivityType.SLEEP:
                            sendBroadcast(mIntent.putExtra(CommonUtil.EXTRA_KEY_TIME, aLong));
                            break;
                        case ActivityType.BREAST: {
                            registerReceiver(mBroadcastReceiver, new IntentFilter(CommonUtil.BREAST_TIMER));
                            switch (mBreastSide) {
                                case BreastSideType.LEFT:
                                    mLeftTime = aLong - mRightTime;
                                    sendBroadcast(mIntent.putExtra(CommonUtil.EXTRA_KEY_LEFT_TIME, mLeftTime));
                                    break;
                                case BreastSideType.RIGHT:
                                    mRightTime = aLong - mLeftTime;
                                    sendBroadcast(mIntent.putExtra(CommonUtil.EXTRA_KEY_RIGHT_TIME, mRightTime));
                                    break;
                            }

                            break;
                        }

                    }

                }));


        return START_STICKY;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mBreastSide = intent.getIntExtra(CommonUtil.EXTRA_KEY_BREAST_SIDE, 0);
        }
    };

    @Override
    public void onDestroy() {
        Log.d("TAGGG", "onDestroy");
        mCompositeDisposable.clear();
        if (!TextUtils.isEmpty(mActivityType) && mActivityType.equals(ActivityType.BREAST)) {
            unregisterReceiver(mBroadcastReceiver);
        }
        super.onDestroy();
    }

    private void showNotification() {
        long[] pattern = new long[]{500L, 500L, 500L, 500L, 500L};

        int notificationId = 1;
        Intent intent;
        switch (mActivityType) {
            case ActivityType.SLEEP:
                intent = new Intent(this, SleepActivity.class);
                break;
            default:
                intent = new Intent(this, FeedingActivity.class);
                break;
        }
        intent.putExtra(CommonUtil.EXTRA_SERVICE_STARTED, true);
        intent.putExtra(CommonUtil.IS_FROM_NOTIFICATION, true);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel androidChannel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            androidChannel.enableLights(true);
            androidChannel.enableVibration(true);
            androidChannel.setLightColor(Color.BLUE);
            androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(androidChannel);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher_round))
                .setContentTitle("MyBaby")
                .setContentText(mActivityType)
                .setColor(ContextCompat.getColor(this, R.color.primary))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(mActivityType))
                .setPriority(NotificationManagerCompat.IMPORTANCE_HIGH | NotificationManagerCompat.IMPORTANCE_MAX)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setGroup(GROUP_KEY)
                .setGroupSummary(true)
                .setShowWhen(true)
                .setOngoing(true)
                .setLights(Color.BLUE, 1000, 500)
                .setContentIntent(pendingIntent);

//        if (notificationManager != null) {
//            notificationManager.notify(notificationId, builder.build());
//        }
        startForeground(notificationId, builder.build());
    }
}
