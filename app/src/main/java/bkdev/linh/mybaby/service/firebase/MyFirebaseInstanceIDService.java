package bkdev.linh.mybaby.service.firebase;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.shareds.PrefsKey;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("TAGGG", "token" + token);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Prefs.PREFS_FB, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PrefsKey.DEVICE_TOKEN, token);
        editor.apply();
    }
}

