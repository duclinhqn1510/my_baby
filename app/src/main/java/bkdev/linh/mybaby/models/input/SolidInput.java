package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
public class SolidInput {
    @SerializedName("id")
    private int id;
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("timeOfFeeding")
    private String timeOfFeeding;
    @SerializedName("babyReaction")
    private String babyReaction;
    @SerializedName("amount")
    private int amount;
}
