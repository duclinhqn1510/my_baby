package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
public class SleepInput {
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("beginTime")
    private String beginTime;
    @SerializedName("endTime")
    private String endTime;
    @SerializedName("duration")
    private long duration;
}
