package bkdev.linh.mybaby.models;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Linh NDD
 * on 5/12/2018.
 */

@Data
@AllArgsConstructor
public class ActivityDetail {
    @SerializedName("type")
    private String type;
    @SerializedName("count")
    private int count;
    @SerializedName("total")
    private int total;
    @SerializedName("advice")
    private String advice;
    public ActivityDetail() {
    }
}
