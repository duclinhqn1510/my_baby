package bkdev.linh.mybaby.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Data
@EqualsAndHashCode
public class Baby {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("familyId")
    @Expose
    private int familyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private int gender;
    @SerializedName("birthday")
    @Expose
    private String birthday;
}
