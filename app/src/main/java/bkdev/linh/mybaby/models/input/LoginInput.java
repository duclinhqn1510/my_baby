package bkdev.linh.mybaby.models.input;

import lombok.Builder;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
public class LoginInput {
    private String username;
    private String password;
}
