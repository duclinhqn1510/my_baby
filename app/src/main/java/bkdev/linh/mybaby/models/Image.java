package bkdev.linh.mybaby.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;

/**
 * Created by Linh NDD
 * on 5/18/2018.
 */

@Getter
public class Image {
    @SerializedName("originalName")
    @Expose
    private String originalName;
    @SerializedName("generatedName")
    @Expose
    private String generatedName;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
}
