package bkdev.linh.mybaby.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

@Entity
public class Activity implements Parcelable {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("familyId")
    private int familyId;
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("authorId")
    private int authorId;
    @SerializedName("type")
    private String type;
    @SerializedName("typeId")
    private int typeId;
    @SerializedName("content")
    private String content;
    @SerializedName("date")
    private String date;

    public Activity() {
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFamilyId() {
        return familyId;
    }

    public void setFamilyId(int familyId) {
        this.familyId = familyId;
    }

    public int getBabyId() {
        return babyId;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.familyId);
        dest.writeInt(this.babyId);
        dest.writeInt(this.authorId);
        dest.writeString(this.type);
        dest.writeInt(this.typeId);
        dest.writeString(this.content);
        dest.writeString(this.date);
    }

    protected Activity(Parcel in) {
        this.id = in.readInt();
        this.familyId = in.readInt();
        this.babyId = in.readInt();
        this.authorId = in.readInt();
        this.type = in.readString();
        this.typeId = in.readInt();
        this.content = in.readString();
        this.date = in.readString();
    }

    public static final Creator<Activity> CREATOR = new Creator<Activity>() {
        @Override
        public Activity createFromParcel(Parcel source) {
            return new Activity(source);
        }

        @Override
        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };
}
