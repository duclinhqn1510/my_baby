package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
public class SignUpInput {
    private String username;
    private String password;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("familyCode")
    private String familyCode;
}
