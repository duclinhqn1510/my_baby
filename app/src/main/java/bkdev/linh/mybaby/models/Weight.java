package bkdev.linh.mybaby.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */

@Entity
public class Weight {
    @PrimaryKey
    private int id;
    private float number;
    @SerializedName("diffMonth")
    private float diffMonth;
    @SerializedName("babyId")
    private int babyId;
    private String date;

    private String advice;

    private boolean isUploaded;

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public float getDiffMonth() {
        return diffMonth;
    }

    public void setDiffMonth(float diffMonth) {
        this.diffMonth = diffMonth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getNumber() {
        return number;
    }

    public void setNumber(float number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public int getBabyId() {
        return babyId;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }
}
