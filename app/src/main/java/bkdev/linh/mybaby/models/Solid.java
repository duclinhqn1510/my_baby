package bkdev.linh.mybaby.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Entity
public class Solid {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("timeOfFeeding")
    private String timeOfFeeding;
    @SerializedName("babyReaction")
    private String babyReaction;
    @SerializedName("amount")
    private int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBabyId() {
        return babyId;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }

    public String getTimeOfFeeding() {
        return timeOfFeeding;
    }

    public void setTimeOfFeeding(String timeOfFeeding) {
        this.timeOfFeeding = timeOfFeeding;
    }

    public String getBabyReaction() {
        return babyReaction;
    }

    public void setBabyReaction(String babyReaction) {
        this.babyReaction = babyReaction;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
