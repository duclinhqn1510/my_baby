package bkdev.linh.mybaby.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import bkdev.linh.mybaby.models.ActivityDetail;
import lombok.Getter;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Getter
public class GetActivityDetailsResponse {
    @SerializedName("data")
    @Expose
    private List<ActivityDetail> activityDetails;
}
