package bkdev.linh.mybaby.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import bkdev.linh.mybaby.models.Sleep;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Getter
@EqualsAndHashCode
public class SleepResponse {
    @SerializedName("data")
    @Expose
    private Sleep sleep;
}
