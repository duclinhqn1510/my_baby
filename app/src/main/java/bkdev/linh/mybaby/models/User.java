package bkdev.linh.mybaby.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Data
@EqualsAndHashCode
public class User implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("familyId")
    @Expose
    private int familyId;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("token")
    @Expose
    private String token;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.familyId);
        dest.writeString(this.fullName);
        dest.writeString(this.username);
        dest.writeString(this.token);
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.familyId = in.readInt();
        this.fullName = in.readString();
        this.username = in.readString();
        this.token = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
