package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
public class DiaperInput {
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("timeOfChanging")
    private String timeOfChanging;
    @SerializedName("reasonOfChanging")
    private String reasonOfChanging;
    @SerializedName("color")
    private String color;
    @SerializedName("status")
    private String status;
}
