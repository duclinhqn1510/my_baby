package bkdev.linh.mybaby.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import bkdev.linh.mybaby.models.Bottle;
import lombok.Getter;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Getter
public class BottleResponse {
    @SerializedName("data")
    @Expose
    private Bottle bottle;
}
