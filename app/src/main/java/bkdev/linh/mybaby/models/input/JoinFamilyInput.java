package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
public class JoinFamilyInput {
    @SerializedName("familyCode")
    private String familyCode;
    @SerializedName("userId")
    private int  userId;
}
