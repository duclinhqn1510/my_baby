package bkdev.linh.mybaby.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Data
@EqualsAndHashCode
public class Family {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("authorId")
    @Expose
    private int authorId;
}
