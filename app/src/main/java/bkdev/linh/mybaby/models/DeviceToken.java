package bkdev.linh.mybaby.models;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Linh NDD
 * on 5/12/2018.
 */

@Data
@AllArgsConstructor
public class DeviceToken {
    @SerializedName("token")
    private String token;
    @SerializedName("userId")
    private int userId;

    public DeviceToken() {
    }
}
