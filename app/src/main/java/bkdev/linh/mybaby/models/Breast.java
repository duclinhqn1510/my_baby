package bkdev.linh.mybaby.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Entity
public class Breast {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("timeOfFeeding")
    private String timeOfFeeding;
    @SerializedName("leftDuration")
    private long leftDuration;
    @SerializedName("rightDuration")
    private long rightDuration;

    public Breast() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBabyId() {
        return babyId;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }

    public String getTimeOfFeeding() {
        return timeOfFeeding;
    }

    public void setTimeOfFeeding(String timeOfFeeding) {
        this.timeOfFeeding = timeOfFeeding;
    }

    public long getLeftDuration() {
        return leftDuration;
    }

    public void setLeftDuration(long leftDuration) {
        this.leftDuration = leftDuration;
    }

    public long getRightDuration() {
        return rightDuration;
    }

    public void setRightDuration(long rightDuration) {
        this.rightDuration = rightDuration;
    }
}
