package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Builder
@Data
public class BottleInput {
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("timeOfFeeding")
    private String timeOfFeeding;
    @SerializedName("volume")
    private int volume;
}
