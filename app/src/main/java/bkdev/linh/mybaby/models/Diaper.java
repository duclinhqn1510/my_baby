package bkdev.linh.mybaby.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Entity
public class Diaper {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("timeOfChanging")
    private String timeOfChanging;
    @SerializedName("color")
    private String color;
    @SerializedName("status")
    private String status;
    @SerializedName("reasonOfChanging")
    private String reasonOfChanging;

    public String getReasonOfChanging() {
        return reasonOfChanging;
    }

    public void setReasonOfChanging(String reasonOfChanging) {
        this.reasonOfChanging = reasonOfChanging;
    }

    public Diaper() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBabyId() {
        return babyId;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }

    public String getTimeOfChanging() {
        return timeOfChanging;
    }

    public void setTimeOfChanging(String timeOfChanging) {
        this.timeOfChanging = timeOfChanging;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
