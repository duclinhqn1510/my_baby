package bkdev.linh.mybaby.models.events;

import bkdev.linh.mybaby.models.Activity;

/**
 * Created by Linh NDD
 * on 5/6/2018.
 */

public class ActivityEvent {
    private Activity activity;

    public ActivityEvent(Activity activity) {
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
