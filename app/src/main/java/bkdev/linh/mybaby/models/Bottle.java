package bkdev.linh.mybaby.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Linh NDD
 * on 4/30/2018.
 */

@Entity
public class Bottle {
    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    private int id;
    @SerializedName("babyId")
    @ColumnInfo(name = "babyId")
    private int babyId;
    @SerializedName("timeOfFeeding")
    @ColumnInfo(name = "timeOfFeeding")
    private String timeOfFeeding;
    @SerializedName("volume")
    @ColumnInfo(name = "volume")
    private int volume;

    @ColumnInfo(name = "isUploaded")
    private boolean isUploaded = true;

    public Bottle() {
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBabyId() {
        return babyId;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }

    public String getTimeOfFeeding() {
        return timeOfFeeding;
    }

    public void setTimeOfFeeding(String timeOfFeeding) {
        this.timeOfFeeding = timeOfFeeding;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
