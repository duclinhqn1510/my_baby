package bkdev.linh.mybaby.models.input;

import com.google.gson.annotations.SerializedName;

import bkdev.linh.mybaby.models.Activity;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */


public class ActivityInput {
    @SerializedName("familyId")
    private int familyId;
    @SerializedName("babyId")
    private int babyId;
    @SerializedName("authorId")
    private int authorId;
    @SerializedName("type")
    private String type;
    @SerializedName("typeId")
    private int typeId;
    @SerializedName("content")
    private String content;
    @SerializedName("date")
    private String date;

    public ActivityInput(Activity activity) {
        this.familyId = activity.getFamilyId();
        this.babyId = activity.getBabyId();
        this.authorId = activity.getAuthorId();
        this.type = activity.getType();
        this.typeId = activity.getTypeId();
        this.content = activity.getContent();
        this.date = activity.getDate();
    }

    public ActivityInput() {
    }
}
