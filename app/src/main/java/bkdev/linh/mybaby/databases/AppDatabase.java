package bkdev.linh.mybaby.databases;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import bkdev.linh.mybaby.databases.dao.ActivityDao;
import bkdev.linh.mybaby.databases.dao.BottleDao;
import bkdev.linh.mybaby.databases.dao.BreastDao;
import bkdev.linh.mybaby.databases.dao.DiaperDao;
import bkdev.linh.mybaby.databases.dao.SleepDao;
import bkdev.linh.mybaby.databases.dao.WeightDao;
import bkdev.linh.mybaby.models.Activity;
import bkdev.linh.mybaby.models.Bottle;
import bkdev.linh.mybaby.models.Breast;
import bkdev.linh.mybaby.models.Diaper;
import bkdev.linh.mybaby.models.Moment;
import bkdev.linh.mybaby.models.Sleep;
import bkdev.linh.mybaby.models.Solid;
import bkdev.linh.mybaby.models.Weight;

/**
 * Created by Linh NDD
 * on 5/1/2018.
 */

@Database(entities = {Activity.class, Bottle.class, Weight.class,
        Sleep.class, Breast.class, Diaper.class, Solid.class, Moment.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase sInstance;

    public static AppDatabase getDatabase(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "MyBaby.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return sInstance;
    }

    public abstract BottleDao bottleDao();

    public abstract ActivityDao activityDao();

    public abstract WeightDao weightDao();

    public abstract SleepDao sleepDao();

    public abstract BreastDao breastDao();

    public abstract DiaperDao diaperDao();

}
