package bkdev.linh.mybaby.databases.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

import bkdev.linh.mybaby.models.Bottle;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */
@Dao
public interface BottleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Bottle bottle);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Bottle bottle);

    @Delete
    void delete(Bottle bottle);

    @Transaction
    @Query("SELECT * FROM Bottle ")
    List<Bottle> loadAllBottle();

    @Transaction
    @Query("SELECT sum(volume) FROM Bottle where timeOfFeeding between :startDateTime and :endDateTime ")
    int loadVolumeByTime(String startDateTime, String endDateTime);


    @Transaction
    @Query("DELETE FROM Bottle ")
    void deleteAll();
}
