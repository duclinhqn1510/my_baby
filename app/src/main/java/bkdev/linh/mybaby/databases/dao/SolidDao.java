package bkdev.linh.mybaby.databases.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

import bkdev.linh.mybaby.models.Solid;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */
@Dao
public interface SolidDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Solid solid);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Solid solid);

    @Delete
    void delete(Solid solid);

    @Transaction
    @Query("SELECT * FROM Solid ")
    List<Solid> loadAllBreast();

    @Transaction
    @Query("SELECT sum(amount) FROM Solid where timeOfFeeding between :startDateTime and :endDateTime ")
    int loadCupByTime(String startDateTime, String endDateTime);

    @Transaction
    @Query("DELETE FROM Solid ")
    void deleteAll();
}
