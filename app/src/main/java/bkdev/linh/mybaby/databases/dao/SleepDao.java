package bkdev.linh.mybaby.databases.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import bkdev.linh.mybaby.models.Sleep;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */
@Dao
public interface SleepDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Sleep sleep);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Sleep sleep);

    @Delete
    void delete(Sleep sleep);

    @Transaction
    @Query("DELETE FROM Sleep ")
    void deleteAll();
}
