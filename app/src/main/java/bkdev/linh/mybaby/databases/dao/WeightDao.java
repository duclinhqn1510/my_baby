package bkdev.linh.mybaby.databases.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

import bkdev.linh.mybaby.models.Weight;
import io.reactivex.Flowable;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */
@Dao
public interface WeightDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Weight weight);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Weight weight);

    @Delete
    void delete(Weight weight);

    @Transaction
    @Query("SELECT * FROM Weight order by Weight.date ")
    List<Weight> loadAllWeight();

    @Transaction
    @Query("SELECT * FROM Weight order by Weight.date ")
    Flowable<List<Weight>> loadAllWeightFlowable();

    @Transaction
    @Query("DELETE FROM Weight ")
    void deleteAll();
}
