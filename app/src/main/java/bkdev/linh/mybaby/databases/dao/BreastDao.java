package bkdev.linh.mybaby.databases.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

import bkdev.linh.mybaby.models.Breast;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */
@Dao
public interface BreastDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Breast breast);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Breast breast);

    @Delete
    void delete(Breast breast);

    @Transaction
    @Query("SELECT * FROM Breast ")
    List<Breast> loadAllBreast();

    @Transaction
    @Query("DELETE FROM Breast ")
    void deleteAll();
}
