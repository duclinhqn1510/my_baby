package bkdev.linh.mybaby.databases.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

import bkdev.linh.mybaby.models.Activity;

/**
 * Created by Linh NDD
 * on 5/2/2018.
 */
@Dao
public interface ActivityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Activity activity);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Activity activity);

    @Delete
    void delete(Activity activity);

    @Transaction
    @Query("SELECT * FROM Activity ")
    List<Activity> loadAllActivity();

    @Transaction
    @Query("DELETE FROM Bottle ")
    void deleteAll();
}
