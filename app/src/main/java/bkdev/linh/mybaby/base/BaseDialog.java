package bkdev.linh.mybaby.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import activitystarter.ActivityStarter;
import bkdev.linh.mybaby.R;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseDialog extends DialogFragment {

    private BaseActivity mBaseActivity;
    private Unbinder mUnBinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            mBaseActivity = (BaseActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(getContentView(), container, false);
        setUnBinder(ButterKnife.bind(this, v));
        ActivityStarter.fill(this);
        return v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getBaseActivity());
        if (dialog.getWindow() != null) {
            dialog.setCanceledOnTouchOutside(true);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }
        return dialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initValue(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
          Set width & height of dialog on onResume method follow this
          http://stackoverflow.com/questions/14946887/setting-the-size-of-a-dialogfragment
         */
        if (getDialog().getWindow() != null) {
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            params.width = getWidthDialog();
            params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            getDialog().getWindow().setAttributes(params);
        }
    }

    public void show(FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            super.show(fragmentManager, null);
        }
    }

    public abstract void initValue(Bundle savedInstanceState);

    public abstract int getContentView();

    protected int getWidthDialog() {
        return getResources().getDimensionPixelSize(R.dimen.dialog_width);
    }

    public BaseActivity getBaseActivity() {
        return mBaseActivity;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
    }


}
