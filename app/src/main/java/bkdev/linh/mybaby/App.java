package bkdev.linh.mybaby;

import android.app.Application;
import android.content.Context;

import bkdev.linh.mybaby.api.ApiClient;
import bkdev.linh.mybaby.api.ApiConfig;
import bkdev.linh.mybaby.shareds.Prefs;
import bkdev.linh.mybaby.utils.rx.RxBus;

/**
 * Created by linhdd
 * on 1/17/18.
 */

public class App extends Application {

    private static App sInstance;

    private RxBus mBus;


    public static synchronized App getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return getInstance().getApplicationContext();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        ApiConfig apiConfig = ApiConfig.builder()
                .context(getApplicationContext())
                .baseUrl(BuildConfig.HOST_API)
                .build();
        ApiClient.getInstance().init(apiConfig);

        Prefs.getInstance().init(getApplicationContext());

        mBus = new RxBus();

    }

    public RxBus bus() {
        return mBus;
    }
}
